# ITk at BNL

<h1 style="font-size: 36px;">Website: https://itk-at-bnl.docs.cern.ch/</h1>

The purpose of this repository is to store documentation, code, and procedures for performing ITk related tasks at BNL. The website it built [HERE](https://itk-at-bnl.docs.cern.ch/).

Contact: Abraham Tishelman-Charny

* Email: abraham.tishelman.charny@cern.ch
* Skype: "abe_tc"
* Mattermost: @atishelm

Nice example `mkdocs` config: https://github.com/squidfunk/mkdocs-material/tree/master
