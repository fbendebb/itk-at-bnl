# Stave testing

The local support structure of the ITk strip modules is called a Stave. One stave hosts 28 total modules, 14 on each side. BNL is going to assemble and test half of the ITk strips barrel, summing to 196 staves. The other half of the ITk strips barrel will be assembled and tested at RAL. This page contains instructions and procedures for how to test staves at BNL.

In order to remotely connect to the stave testing PC, following the instructions in the [Remote connections instruction page](https://itk-at-bnl.docs.cern.ch/Instructions/Remote_Connection/), following the case of `cleanroom_gateway_tunnel_ForwardStaveTestingPC`. 
