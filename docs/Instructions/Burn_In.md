# Burn in

Instructions for setting up the Burn-In Crate found here: [HybridBurnInCrate](https://twiki.cern.ch/twiki/bin/viewauth/Atlas/HybridBurnInCrate#Software_links)


**Useful Information:**

ATLAS database:
https://itkpd-test.unicorncollege.cz/

Hybrid positions on panel:
<img width="500" src="/Images/Barrel-Panel-Positions.jpg" alt="Barrel Panel Position">

Image of hybrid burn in crate:
<img width="500" src="/Images/HybridBurnInCrate.jpeg" alt="Burn In Crate">

Image of testing panel with hybrids and powerboards: 

Full testing panel has a total of 6 hybrids and three powerboards.
<img width="500" src="/Images/BurnInTestingPanel_1.jpg" alt="Burn In Panel">


**Starting Hybrid Burn In Crate**

1. Place panels (with hybrids and pbs) into slots of the crate 
    - Slot position 0-5 from top (starting at 0) to bottom (ending in 5)

2. Plug wire connections from DAQ board (Genesys G2 FPGA) to Arduino (which provides control over fans)

3. Plug wire connections from Burn In Crate to the power supply

4. Turn DAQ board on (switch near the power supply wire splitter to the fans)

5. On PC, find firmware program (Vivado Lab)
    - right click on xilinx firmware and select Program Device

With these steps, we finished setting up the crate and genesys 2 board for testing. 

**Initializing AMACs**

See instructions in Thermal Cycling also.

1) Open file LTT_AMAC_Monitor.cpp with:
```
emacs LTT_AMAC_Monitor.cpp &
```
- Directory: /home/hybrid_station/Desktop/NEW_burnin/hybrid-burnin-gui/itsdaq-sw-burnin_rebase/macros/

In file LTT_AMAC_Monitor.cpp, modify third to last line in:
  
```   
    Stavelet();

    for(int panel=0; panel<6; panel++) gROOT->ProcessLine(Form("h->switchOF(%d, 0);", panel));
    
    //    AMACv2_final_initialiseAMACs({4,1,0,4,1,0,1,0,4,1,0,1,0},{1,1,1,2,2,2,3,3,4,4,4,5,5}); 
    AMACv2_final_initialiseAMACs({powerboard id},{slot position of pbs}); 
    Influx_AMAC_Startup();
    Influx_AMAC_READ();

```
Want to change:

```  
    AMACv2_final_initialiseAMACs({pb id},{slot position of pbs});
```
- pb id can be read from pbs on panel (read from right to left)
- number slot position of pbs in crate

For example: 
```
    AMACv2_final_initialiseAMACs({4,1,0,4,1,0,1,0,4,1,0,1,0},{1,1,1,2,2,2,3,3,4,4,4,5,5});
```
- first parameter in curly brackets defines the pb id numbers (right to left) for each slot positon. 
        -from example: pb id #'s 4,1,0 in slot 1; 1 4,1,0 in slot 2; 1, 0 in slot 3; 4,1,0 in slot 4; 1,0 in slot 5

- second parameter in curly brackets initializes AMACs on pbs in the panel slot positons 1-5:
        - from example: {1,1,1, ...} initalizes three AMACs in slot position 1, {...,3,3,...} initializes two AMACs in slot position 3


2) Save file

**Configuration Set Up:**

**For New Panels (Please Read)** Skip to below for existing panels. 

For new panels, we will need to set up a fake run which will provide us the hybrid's fuse id on the panel. From the fuse ids, we can assemble all pbs and hybrids on the panel in the CERN database. Afterwards, we copy the config file into the local folder where st_system_config.dat pulls files from.

1)  Open config file "st_system_config.dat" with:

```
emacs st_system_config.dat &
```
Directory: 
/home/hybrid_station/Desktop/NEW_burnin/hybrid-burnin-gui/itsdaq-sw-burnin_rebase/DATA/config

- Change existing panel serial number to "star_barrel_Hxx Star_Test" where xx is a dummy number (0, 1, 2, ..., 30) to specify individual modules

Example of module script in st_system_config file:
```
Module   1  1  1  0  0  0  0  0  0  0  103  0  0  0  0x32  0x33  50  50  star_barrel_H1 Star_Test 
```
(Order is important! If one position of hybrid is missing, you must skip one module line in the config file that is designated for that missing hybrid position (see hybrid position at the top))

Listed infos about position in the config files are on the top of st_system_config.dat

2) Save file
    
**For Existing Panels**

1) Open the module configurations in the config file "st_system_config.dat" with:

```
emacs st_system_config.dat &
```

Directory: 
/home/hybrid_station/Desktop/NEW_burnin/hybrid-burnin-gui/itsdaq-sw-burnin_rebase/DATA/config

2) Modify config file for panel setup: 

- See information about module config files here: [ConfigFiles](https://twiki.cern.ch/twiki/bin/viewauth/Atlas/ABCStarHybridModuleTests#Config_Files)

Example of line from config file: 
```    
Module   1  1  1  0  0  0  0  0  0  0  103  0  0  0  0x32  0x33  50 50 panel_SN20USBTG0000087/SN20USBHX2001197 Star_Test
```
- Initialize the first module (hybrid) in position 0 and all modules you want to test in ascending order (1, 2,...)

Example in st_system_config file:
```
Module   0  1  1  0  0  0  0  0  0  0  103  0  0  0  0x32  0x33  50 50 panel_SN20USBTG0000087/SN20USBHX2001194 Star_Test
#Module   1  1  1  0  0  0  0  0  0  0  103  0  0  0  0x32  0x33  50 50 panel_SN20USBTG0000087/SN20USBHX2001195 Star_Test
Module   1  1  1  0  0  0  0  0  0  0  103  0  0  0  0x32  0x33  50 50 panel_SN20USBTG0000087/SN20USBHX2001196 Star_Test
#Module   3  1  1  0  0  0  0  0  0  0  103  0  0  0  0x32  0x33  50 50 panel_SN20USBTG0000087/SN20USBHX2001197 Star_Test
#Module   4  1  1  0  0  0  0  0  0  0  103  0  0  0  0x32  0x33  50 50 panel_SN20USBTG0000087/SN20USBHX2001197 Star_Test
Module   2  1  1  0  0  0  0  0  0  0  103  0  0  0  0x32  0x33  50 50 panel_SN20USBTG0000087/SN20USBHX2001198 Star_Test
```
All six hybrids (X hybrids) on panel, but only initialize three hybrids for testing. Hybrids not running are commented out.

**Running ITSDAQ**

1) Run ITSDAQ

Move to the directory: 

/home/hybrid_station/Desktop/NEW_burnin/hybrid-burnin-gui/itsdaq-sw-burnin_rebase

- Three pages from ITSDAQ should pop up: NTC temp reading (near hybrids), testing procedures (StrobeDelay, ThreePointGain, etc.), and AMAC NTC temp readings

Run command
```
source Run_ITSDAQ.sh 
```
**Check:** Initial current reading around ~0.5-0.7 A before running ITSDAQ. Once you run ITSDAQ, the amps increases at the start of the NTC temp readings (first pop-up page) and again when tests are performed (third pop-up page). The amount of current increase is proportional to how many hybrids/pbs you are running. 

2) Get the fuse id

Move to the directory:
/home/hybrid_station/Desktop/NEW_burnin/hybrid-burnin-gui/itsdaq-sw-burnin_rebase/DATA/data

List all recent files made in the directory using 
```
ls -lstr
```
example of fuse id file: strun1407_1.root

Choose any of the recent ROOT files to get the fuse id.

3) Move to the directory: "/home/hybrid_station/Desktop/database/production_database_scripts/strips/hybrids" 

Run command after changes:

```
python3 assembleHybridByFuseIDs.py --panel (panel serial number) --root /home/hybrid_station/Desktop/NEW_burnin/hybrid-burnin-gui/itsdaq-sw-burnin_rebase/DATA/data/strun(strun number).root --dryRun 
```
- Copy/paste fuse id root file (looks like strun####_#.root) and replace strun number (ex: 1407_1) in the code
- Copy/paste matched panel serial number from google sheet (panel id located on the right bottom of panel)
        - google sheet [here](https://docs.google.com/spreadsheets/d/1PgVL5i0sD9h57ORrsTRfMN7Rfhz91kjiC8jE2fEcbeQ/edit#gid=66396988)
- Run command after changes
- This assembles all components of the panel (hybrids, pbs, etc.) in the ATLAS database
- **Note:** need to have special permission from database to assemble hybrids

Image of output from assembleHybridByFuseIDs.py:

<img width="500" src="/Images/assemble_chips.jpg" alt="assemble ABCStar chips">

4) Move to the modules directory:
/home/hybrid_station/Desktop/database/production_database_scripts/strips/modules

Run command:
```
python3 getPanelConfig.py --serial (panel serial number)
```
- Copy/paste the same panel serial number, starting with 20
- This produces the configuration file for the panel

5) Change to the directory:
```
/home/hybrid_station/Desktop/database/production_database_scripts/strips/modules/hybrid_configs/panel_SN(panel serial number)/config
```
If you ls into that directory, the hybrid serial numbers should appear as det files. The hybrid serial number should match the ones from database.

6) Copy the entire folder with the .det files to the directory:

/home/hybrid_station/Desktop/NEW_burnin/hybrid-burnin-gui/itsdaq-sw-burnin_rebase/DATA/config

by doing the following:
```
cp *.det /home/hybrid_station/Desktop/database/production_database_scripts/strips/modules/hybrid_configs/panel_SN(panel serial number)/config
```
7) Modify st_system_config.dat file to list panel serial number and hybrid serial number
    - ex. 
```
Module   1  1  1  0  0  0  0  0  0  0  103  0  0  0  0x32  0x33  50 50 panel_SN20USBTG0000087/SN20USBHX2001197 Star_Test
```
- Match the ordering of the hybrid serial numbers by the ordering of the numbers from the database (starting from top to bottom).

8)  Rerun source Run_ITSDAQ.sh

9) Update BNL_ModuleStatus google sheet for any problems (wirebonding, faulty chips, etc.) [here](https://docs.google.com/spreadsheets/d/1PgVL5i0sD9h57ORrsTRfMN7Rfhz91kjiC8jE2fEcbeQ/edit#gid=2146007184)

*Side note: This is where you can get the RC plots:
/home/hybrid_station/Desktop/NEW_burnin/hybrid-burnin-gui/itsdaq-sw-burnin_rebase/DATA/ps

**Ending Run**

For electric testing, to end your run: close out of all three pages produced from ITSDAQ and turn off the current from PS. Restart run for the 100 hr burn-in. 

After the 100 hr burn-in, end your run in this order: (1) close out of all three pages produced from ITSDAQ, (2) turn off the current from PS, and (3) turn off the DAQ (FPGA) board. 

**Known Error Messages**

This error is produced due to wrong hex number in stream (S0) position in hybrid config file:

<img width="500" src="/Images/stream_error.jpg" alt="stream error image">

- Check stream position (S0) from error messages and modify config files on specific hybrid in that stream positon. 
- Go to directory: /home/hybrid_station/Desktop/database/production_database_scripts/strips/modules/hybrid_configs/panel_SN(panel serial number)/config
- Choose matched hybrid serial number. Open config file and change the hex number to 0x1111111 or 0x2222222 or ... (all the way up to 4444444). both stream position S0 and S1 must be the same hex number so you must change both
- Check each time after modification to see if error disappears

HCC crossed registry

power supply issues:

automatic setting to CC (constant current) mode --> make sure electrical connections are robust




**----NEW BURN IN II CRATE----**

- For new burn in crate in the testing clean room
- Similar setup to the first burn in crate, but with newest version of itsdaq and influxDB v2.7.1

Key differences between burn in crate I and II:

- No need to configure AMACs. New itsdaq auto-configures AMACs using AutoConfig.cpp
- Restart itsdaq 2-3 times to run test --> itsdaq should enable all CCRs and disable all OFs by itself



For any questions (or corrections), please ask:

Alysea Kim --> alysea.kim@cern.ch



