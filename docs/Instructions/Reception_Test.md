# Reception test

When a module is received from another institute, we need to ensure it survived the shipping process properly. We do this by performing a reception test, defined as an electrical test a room temperature.


## Procedure 

1. Make sure coldjig GUI is running - requires one terminal on the raspberry pi running. In production mode, should already have this open by default. If not, follow these instructions to turn on the GUI from scratch:


<details>


# Starting the coldjig s/w web gui 

The purpose of these instructions are to explain how to start the coldbox s/w GUI from scratch. This is used to monitor and control the various hardware components of the setup. 

1. If not already done, open a firefox window and a konsole terminal window by (double) clicking the icons on the desktop.
2. On the konsole terminal, type the command `ssh_pi` and press enter. You will then be asked for the password. If you do not know the password, ask Abe or Stefania. 
3. If the ssh into the pi was successful, you should see the terminal prompt start with `pi@raspberrypi:~ $`. If this is the case, you can switch to the software working directory by typing and entering the command `gui`. After this, type and enter these commands in order:

```
pipenv shell
source setenv.sh
./run_BNL.sh
```

If this was successful, you should begin to see some information printout in the window and you can move to step 4.

If there was already an attempted instance to start the coldjig s/w running before you began, at this stage you may receive the error:

```
OSError: [Errno 98] Address already in use
```

This is because there is a job already using the IP address provided in the configuration file to start an instance of the GUI. You can kill that already existing process in one way by running `ps -a` to list all running processes, and killing the python3 process which is likely the culprit by identifying its "PID" (process ID) from the `ps -a` printout, and running `kill -9 <PID>` to kill the process. After this, try `./run.sh` again to see if you can start the web gui. 

A possibly related problem occurs when the web gui starts but the "OK" message does not print in the terminal and commands in the gui are not executed. To fix this, try clearing the jobs. Run 'jobs' and then 'kill %1' in the pi.

4. Open a firefox tab and click on the bookmark in the bookmarks bar of your desired webgui: `Coldjig-GUI-QC` or `Coldjig-GUI-Reception`. If this worked properly, you should see a window load the GUI and a Grafana dashboard. 
5. Start the GUI by clicking "Start" in the Control Panel tab. If this worked, you should see additional printout in the konsole where you ran `./run.sh`. If all is working well, the grafana panel should start to update. One can always set the time of the grafana values to be more recent to confirm - e.g. switching absolute time range from "Last 1 hour" to "Last 5 minutes" with the dropdown menu.

</details>

2. Open box, connect up to 4 modules to chuck connections 
3. Close box, set chiller to +20C via the coldjig GUI.
4. Edit BNL Module spreadsheet to say the electrical test for the chosen modules is in progress.
5. In ITSDAQ DAT/config/st_system_config.dat, define a `Module` line for each hybrid. Notably, the 5th numerical column (s0) corresponds to the module's chuck, where the first chuck is 0 and subsequent chuck's values increase in increments of 8. 
6. Ensure all power supplies are defined in DAT/config/power_supplies.json - this will be important for tracking LV/HV current/voltage in Influx. 
7. Make sure we have a .det file for each module in DAT/config. If there isn't, see instructions for "Download hybrid configuration files".
7. Press `OUTPUT` on the two INSTEK power supplies in order to supply 11V to the modules. For long strip modules, you should see current values of around 50 mA per channel. If nothing happens when you press the button, press 'LOCAL' first.
8. Run ITSDAQ using the alias `rid` (for 'source RUNITSDAQ.sh').
9. Configure the AMAC by 'AutoConfig()'. After this, the LV should read about 280 mA, meaning the ASICS are configured.

10. Ramp the necessary channels to -385V. Do not ramp the channels if the hybrid is not configured. Make sure the current limit is set to 50 uA on the HV power supply before you ramp.

11. Run the electrical test in the ITSDAQ GUI by doing

    'Test>FullTest'

12. After the test, ramp down the HV and LV and turn off the chiller. Also enter the the HCC fuse ID on the BNL Modules spreadhseet. You can do this easily by doing something like `grep "HCC_FUSE" DAT/results/SCIPP-PPB_LS-038*` to quickly search text files depending on which module you have.




PROBLEMS:


1. Trying to run an electrical test, you get printouts like "Not found good even in many (10) packets" and the test will run forever if you don't stop it. Also, the LV current only reads about 240 mA after configuration.


    Solution: The module's .det file may not be configured to account for cross bonding on the HCC. Try opening the .det file and changing register 32 to read 0x02900020.

2. When running the test, you get a messasge in red: "hsio_read_event: read_bit_data error".

    Solution: Look at what stream/module the error corresponds to (0,8,16, or 24 depending on location of the module). if it's only 1 or 2 streams, you can comment out that module in DAT/config/st_system_config.dat and redo the test without it. 

3. After staring ITSDAQ, you get a 

    "Failed to read from DAQ board to get initial status, closing connection"

error, preceded by a message about the address already being in use (something like "udp_open:bind failed: Address already in use").

    Solution: You have a process in the background keeping the board busy. Run "ps -a" and look for the process. It might be a root instance. Then do "kill -9 [number1] [number 2] ..." where the numbers are associated with the processes you want to kill. 

4. You have trouble pulling the hybrid config files, getting an error on the line where you try to pull the hybrid SN in GetDBInfo.py. You then discover that the hybrid is not assembled in the database and has no config file yet.

    Solution: First, find someone to yell at for not assembling the hybrid! Then, run an electrical test (for example, strobe delay) using a default config file. On box 2, there is a file called "default.det" that should automatically be used when another config file isn't found. The test should produce a root file in DAT/data/ that you need for the next step.

    Now you can use the root file produced from this test to run an assembly script. There is no maintained script for assembling a single hybrid (that I am aware of) but Emily has a hastily edited version of 'assembleHybridByFuseIDs.py' on box 2 that should work. This script is in production_database_scripts/strips/hybrids on the hybrid burn in setup but I am unsure what git repository it comes from since it is not on master. The script you want to run on box 2 is Desktop/production_database_scripts/strips/hybrids/assembleHybridSingle.py. Using the root file outputted by your electrical test, run (for example):
        python3 assembleHybridSingle.py --root ~/Desktop/itsdaq-sw/DAT/data/strun26_1.root --hybridSN 20USBHX2001311

    Now you can try to pull the config file from the database like you normally do.

