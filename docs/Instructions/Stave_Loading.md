# Stave Loading

BNL and RAL are the two stave loading sites for the ITk strips barrel. 

Documentation on Stave Loading can be found here: 

[Stave Loading at BNL](/Instructions/Stave_Loading_At_BNL.pdf)

Files used to rebuild these instruction using LaTeX can be found here:

[Stave loading instruction files](https://gitlab.cern.ch/atishelm/itk-at-bnl/-/tree/7ad4941e0039a887743081ff7ba69fae94ef66f4/Stave-testing/Stave_Loading_Documents)
