# Thermal cycling

In order to ensure an assembled module is robust and can survive temperature changes, each assembled module must be thermal cycled 10 times, with an electrical test performed at each low and high temperature point. 

Note - more TC instructions, used at Birmingham, can be found [here](https://bilpa.docs.cern.ch/itkstrips/coldjig/thermalcycling/).

## Procedure 

Before starting, ensure you're using the operational branches of the software by running the following scripts (currently only contained in box 3).

In the Pi:

```
gui
python3 SetupOperationsMode.py
```

On the PC (running ITSDAQ):

```
itsdaq
python3 SetupOperationsMode.py
```

1. Make sure coldjig GUI is running - requires one terminal on the raspberry pi running. In production mode, should already have this open by default. If not, follow these instructions to turn on the GUI from scratch:


<details>


# Starting the coldjig s/w web gui 

The purpose of these instructions are to explain how to start the coldbox s/w GUI from scratch. This is used to monitor and control the various hardware components of the setup. 

1. If not already done, open a firefox window and a konsole terminal window by (double) clicking the icons on the desktop.
2. On the konsole terminal, type the command `ssh_pi` and press enter. You will then be asked for the password. If you do not know the password, ask Abe or Stefania. 
3. If the ssh into the pi was successful, you should see the terminal prompt start with `pi@raspberrypi:~ $`. If this is the case, you can switch to the software working directory by typing and entering the command `gui`. After this, type and enter these commands in order:

```
pipenv shell
source setenv.sh
./run_BNL.sh
```

If this was successful, you should begin to see some information printout in the window and you can move to step 4.

If there was already an attempted instance to start the coldjig s/w running before you began, at this stage you may receive the error:

```
OSError: [Errno 98] Address already in use
```

This is because there is a job already using the IP address provided in the configuration file to start an instance of the GUI. You can kill that already existing process in one way by running `jobs` to list all killed processes, and killing the python3 process which is likely the culprit. E.g. kill %1, kill %2, ... After this, try `./run_BNL.sh` again to see if you can start the web gui. 

4. Open a firefox tab and click on the bookmark in the bookmarks bar of your desired webgui: `Coldjig-GUI-QC` or `Coldjig-GUI-Reception`. If this worked properly, you should see a window load the GUI and a Grafana dashboard. 
5. Start the GUI by clicking "Start" in the Control Panel tab. If this worked, you should see additional printout in the konsole where you ran `./run.sh`. If all is working well, the grafana panel should start to update. One can always set the time of the grafana values to be more recent to confirm - e.g. switching absolute time range from "Last 1 hour" to "Last 5 minutes" with the dropdown menu.

</details>

2. Open box, connect up to 4 modules to chuck connections 
3. Close box, set chiller to +20C via the coldjig GUI.
4. In ITSDAQ DAT/config/st_system_config.dat, define a `Module` line for each hybrid. Notably, the 5th numerical column (s0) corresponds to the module's chuck, where the first chuck is 0 and subsequent chuck's values increase in increments of 8. 
5. Ensure all power supplies are defined in DAT/config/power_supplies.json - this will be important for tracking LV/HV current/voltage in Influx. 
6. Press `OUTPUT` on the two INSTEK power supplies in order to supply 11V to the modules. For long strip modules, you should see current values of around 50 mA per channel.
7. In one terminal which is in in `itsdaq-sw` directory, run the command `ram` (this is an alias for source INFLUX_AMAC.sh). This is the AMAC monitoring loop. It also callibrates the AMAC. You should now see the current values jump to around 200 mA.
8. Ensure the HV power supplies set voltages are 0V (the orange value under the current read value of the channels). Only if this is the case, manually press the `ON` buttons on the right sidecan(6,15,100,100);");
will be switched on and therefore monitorable.
9. In a second terminal in the `itsdaq-sw` directory, run the command `source INFLUX_DAQ.sh`. This is the DAQ loop. You will now see the current jump to 260 mA in each channel. The terminal should show something like:

    TIME_LAST_COMMAND_RECEIVED 20230524144598

    TIME_LAST_COMMAND_SENT     20230524144601

    Waiting For new command to execute (11 since received command)


10. Manually set the HV for each channel to -385V, and maximum current to 50 microAmps.
11. In the `Advanced` tab of the coldjig GUI, set `Cold temperature` to -40C, `Hot temperature` to +20C, and `Warm-up temperature` to +20C. NOTE: Hot temp was changed from 40C to 20C in July 2023. 
12. In the `Control Panel` tab of the coldjig GUI, check off each of the chucks which has a module in it, on the left-hand side of the GUI.br tests` or `Standard tests with cold shunting`. The current procedure is to select `Standard tests with cold shunting` in order to stress True Blue modules and see if cold noise appears. 
15. Press the green `Start TC` button. If this works properly, you should see the ITSDAQ terminal running the DAQ loop print out a statement that it received the `INIT_MODULES` command from the coldjig. Then, you should see the chiller begin to cool to -40C. (Until it is fixed, you may get an "unexpected text in data field" error that you can ignore)
16. Tape the "Thermal Cycling" sign on the coldbox handle :D

17. To stop the test early, kill the INFLUX_DAQ.sh instance in the second terminal, and then click the `Stop TC` button on the Coldjig GUI. If you do not kill INFLUX_DAQ first, `Stop TC` will trigger another FullTest.

PROBLEMS: 

1. After pressing "Start TC", second terminal prints:

    TTi::Mon (V? viRead) for PST-3202 at resource /dev/serial/by-id/usb-FTDI_Chipi-X_FT2VC3J3-if00-port0 failed with code 0xffffffff (TSerialProxy: IO Error)

    Solution: Fully turn off HV with switch in the back and  turn it back. You may have to turn off LV as well to make sure calibration is all done correctly, bringing you back to step 6.


2. After trying to run 'ram', itsdaq eventually prints:
    Create TTi from json:
    /dev/serial/by-id/usb-FTDI_Chipi-X_FT2VC3J3-if00-port0 for BNL Module 1 LV
    return existing port for /dev/serial/by-id/usb-FTDI_Chipi-X_FT2VC3J3-if00-port0

    *** Break *** segmentation violation

 Either for one of the LV or HV supplies.

    Solution: If the power supply is not the first one, you may need to comment out the extraneous supplies in powersupplies.json.

3. After trying to run 'ram', itsdaq has a problem with loading module configuration:

    st_configure_modules: load configuration for MODULE 0
    st_read_module: failed to open file /home/qcbox/Desktop/itsdaq-sw/DAT/config/SCIPP-PBB_LS-027.det

and so on. 

    Solution: If the configuration file is indeed there, try literally copying and pasting the name into st_system_config.dat, there may be a small character difference that's hard to see.

4. After trying to run 'ram', itsdaq has a problem loading a HV (or LV) supply:

    TSerialProxy: Error 13 from open: Permission denied
    TkHV::Init Could not open resource /dev/serial/by-id/yaddyaddayadda

    Solution: You may need to reset the ports to your power supplies using sudo chmod a+rwx. You can look in history for the correct command to run, or try running the OpenPS.sh file.

5. After staring ITSDAQ, you get a 

    "Failed to read from DAQ board to get initial status, closing connection"

error, preceded by a message about the address already being in use (something like "udp_open:bind failed: Address already in use").

    Solution: You have a process in the background keeping the board busy. Run "ps -a" and look for the process. It might be a root instance. Then do "kill -9 [number1] [number 2] ..." where the numbers are associated with the processes you want to kill.  

6. When trying to start the coldbox GUI by doing "ssh_pi", you get the message:

    "ssh: connect to host 169.254.163.91 port 22: No route to host:

    Solution: It could be that the pi IP address changed, and this will probably not work. However, the problem may also be that the pi is somehow not reading. To check this, change the monitor to show the pi by pressing the leftmost button at the bottom right of the monitor twice. Then press the button next to this until you get to "input Control". Select via the leftmost button. Change the input control from DisplayPort to HDMI (the PC to the pi). Then remove the keyboard/mouse USB from the PC and plug it into the pi. If you can't see anything on the monitor when you click around with the mouse, the pi is not responding. You can turn off the pi and turn it back on by unplugging and replugging the white USBC cord on the leftmost port of the pi. You should now be able to click around and see the pi respond.

    Remember to plug the mouse/keyboard back into the PC and change the input control back!

If you have setup an ssh into the cleanroom gateway machine, you should be able to monitor the grafana panels from your office. You should then monitor the grafana dashboards - an example of the main one is shown below (for a case of 7 thermal cycles, with cold shunting, where a module went into breakdown after about 4 cycles):

<img src="/Images/22May2023_7_TCs_withShunting.png " alt="Coldjig dashboard">

And the `DCS` dashboard, where one can see the current increasing for the module in Chuck 2, both from the HV PS unit and from the AMAC (this is why it's crucial to set up power supply monitoring before starting to thermal cycle):

<img src="/Images/22May2023_7_TCs_withShunting_VoltageAndCurrent.png" alt="DCS dashboard">

Have fun thermal cycling!

Please send feedback on experience and how to improve these instructions to `abraham.tishelman.charny@cern.ch`
