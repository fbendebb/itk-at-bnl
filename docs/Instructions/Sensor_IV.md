# Sensor IVs

Before gluing electronics onto a sensor and declaring it ready for module testing, we must ensure that the sensor does not go into [breakdown](https://en.wikipedia.org/wiki/Avalanche_breakdown), a phenomenon in which a sensor's current shoots up very high. If this happens below the maximum operating voltage, it will not be usable as a piece of our particle detector (Fails Quality Control) because it will not have a low, stable noise. 

To check sensors at different stages for breakdown, we apply a series of voltage values and note down the measured current values - this is called an IV. Below are the instructions for taking an IV at the BNL setup:

## Step-by-step Instructions

1. Navigate to the GUI in Firefox and turn on the chiller (it's in the **Advanced** tab).
2. Check that you're in `~/Desktop/IV/itsdaq-sw` with the linux command `pwd`. 
3. Note the wafer numbers and their chuck locations in the coldbox.
4. Note the chucks used so you know what channels to use in the next step.
5. Navigate to `DAT/config/power_supplies.json` and edit the 'hv_supplies' field to have the desired number of channels. You can "comment out" a channel by closing the 'hv_supplies' bracket and starting a "comment" field. For example, to do an IV on only the first two chucks:
```
"hv_supplies": [
                {
            "psName": "169.254.227.68:10001",
            "modName": "Module 0",
            "output": 1, 
            "deviceType" : "ISEG",
            "channel" : 0
            },
            {
            "psName": "169.254.227.68:10001",
            "modName": "Module 1",
            "output": 1, 
            "deviceType" : "ISEG",
            "channel" : 1
            }],
"comment": [
                {
            "psName": "169.254.227.68:10001",
            "modName": "Module 2",
            "output": 1, 
            "deviceType" : "ISEG",
            "channel" : 2
        },
                {
            "psName": "169.254.227.68:10001",
            "modName": "Module 3",
            "output": 1, 
            "deviceType" : "ISEG",
            "channel" : 3
        }

        ]
```

6. Navigate to the main folder `~/Desktop/IV/itsdaq-sw/`.
7. Run the program by typing the alias `rid`, or do it the long way by typing `source RUNITSDAQ.sh`. 
8. After the popups come up, go back to the terimnal, press `Enter` and then press the `up arrow` button a few times until you get to the command `HVSupplies[n]->SetLimit(700)` where "n" is the channel of the HV supply that you are using. Run the command by pressing `Enter`. For example, if you only had modules in the first two chucks, you would run `HVSupplies[0]->SetLimit(700)` and `HVSupplies[1]->SetLimit(700)`. This sets that maximum voltage that the HV power supply is allowed to go to. We set this to 700 because when taking an IV, we test our sensors up to 700 volts. (NOTE: previously we set the limit to -700, but the software now takes a positive voltage here)
9. Click on the DCS tab in the "Burst+Data" window and click on either the `Sensor IV (700V)` or `Module (bare) IV scan (700V)` button, depending on the type of test you are doing:
  * Sensor (i.e. minimally populated PCB): `Sensor IV`
  * Sensor with HV Tab attached: `Module (bare) IV scan (700V)`

10. In the resulting popup, enter the appropriate details of the sensor, among others:
  * Sensor setting:
    * Short strip sensor / module (i.e. four columns of strips): `ATLAS18SS`
    * Long strip sensor / module (i.e. two columns of strips): `ATLAS18LS`
  * Temperature 20 degrees C (double check the chuck temperatures on the coldjig GUI), Humidity 0 (double check relative humidity reading on the coldjig GUI).
  * Enter the serial numbers for each of the sensors tested, from left to right in the coldbox (If you leave them as "missing", the output files will NOT be saved!)
  * Enter the wafer numbers for each of the sensors tested, from left to right in the coldbox.
11. Wait for test test to complete, but note the currents in the HV supply, checking whether they are of the order of 10^-7A, around 0.1 microAmperes. 
12. After the test is complete, you should find the `.dat` results at `~/Desktop/IV/itsdaq-sw/DAT/results`.

## Additional notes

When you are entering the serial number into the `wafer_chuck_locations.json`, make sure that:

1. For bare sensors, you use the sensor SN (there is no other option, as there is not yet an associated module SN)
2. For HV-tabbed sensors with IV NOT taken through the HV tab, you use the sensor SN.
3. For HV-tabbed assemblies with an IV taken through the HV tab, there are now referred to as modules, so you should use the module SN.
4. A picture of the sensor IV GUI is below. The current limit is automatically set. iSlope, iLimitAMAC, and Drain Resistors don't do anything for this test.

<img src="/Images/SensorIVGUI.png" alt="IV Scan GUI">

## Potential problems

1. If the pi runs out of space, a temporary solution is clearing the /var/log/ folder to create more space. A more robust solution is described on [this page](https://itk-at-bnl.docs.cern.ch/Known_Issues/No_Space_Left/).
