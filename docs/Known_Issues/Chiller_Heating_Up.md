# Chiller heating up 

If you find that the chiller (FTS Systems SP Scientific) is constantly heating up and cannot be controlled remotely via coldjig S/W or even manually, make sure that the `REFR` button is hit and its light is green. If it is not, this means the refrigerant will not properly work. To set this green, first ensure the remote option is deselected, then select the `REFR` option. 
