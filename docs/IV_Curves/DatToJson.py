"""
15 May 2023
Abraham Tishelman-Charny

The purpose of this python module is to convert a DAT file, with output information from an ITSDAQ IV, to a json file to be in a compatible format to other IV outputs.
"""

import re
import os

Custom_Label = "bypass_AMAC"
inDir = "data_dat"
outDir = "data_bypassAMAC"
files = ["%s/%s"%(inDir,f) for f in os.listdir(inDir)]
print(files)

for dat_file in files:
    print("On file:",dat_file)

    # get json template, dat file
    #dat_file = "BNL-PPB2-MLS-216_-35C_IV_bypassAMAC.dat"
    template_name = "Dat_to_json_template.txt"
    out_file = "%s/%s"%(outDir, dat_file.split('/')[-1].replace(".dat", ".json"))

    with open(template_name, 'r') as template: template = template.read()

    # Get from .dat file:
    # component, CURRENT, CURRENT_RMS, PS_CURRENT, VOLTAGE, TEMPERATURE 
    # replace in template, save.

    with open(dat_file, 'r') as f:
        dat_content = f.read()

    # Extract the voltage, current values.
    pattern = r'(-?\d+\.\d+)\t(-?\d+\.\d+)\t(-?\d+\.\d+)' 

    # assuming a certain pattern in the dat file:
    # Voltage[V]      Current[nA]     Shunt_voltage[mV]
    # 0000000.00      0000000.00      0000000.00
    # -000009.79      -000320.53      0000000.00
    # -000019.79      -000320.85      0000000.00
    # -000029.78      -000322.32      0000000.00

    matches = re.findall(pattern, dat_content)

    SET_VOLTAGE = []

    # from AMAC, or bypass of AMAC
    CURRENT = []
    CURRENT_RMS = []

    # from PS
    VOLTAGE = []
    PS_CURRENT = []

    for match in matches:
        voltage, current, shunt_voltage = match
        CURRENT.append(float(current))
        VOLTAGE.append(float(voltage))
        CURRENT_RMS.append(0)
        PS_CURRENT.append(0)
        SET_VOLTAGE.append(0)

    # get component, temperature
    with open(dat_file, 'r') as f:
        data = f.read().splitlines()

    # initialize variables
    component = ''
    temperature = ''
    identifiers = ["Module_SN", "Temperature"]
    values = []

    for identifier in identifiers:
        # loop through the data to find the matching identifier
        for line in data:
            if identifier in line:
                # get the link component and temperature values
                value = line.split()[1]
                values.append(value)
                break  # exit the loop once the values are found

    component = values[0]
    temperature = values[1]

    temperature_ = "\"%s_%s\""%(Custom_Label,temperature) # add custom label to temperature string

    template = template.replace("{SET_VOLTAGE}", str(SET_VOLTAGE))
    template = template.replace("{CURRENT}", str(CURRENT))
    template = template.replace("{CURRENT_RMS}", str(CURRENT_RMS))
    template = template.replace("{VOLTAGE}", str(VOLTAGE))
    template = template.replace("{PS_CURRENT}", str(PS_CURRENT))
    template = template.replace("{component}", str(component))
    template = template.replace("{TEMPERATURE}", str(temperature_))

    with open(out_file, 'w') as out_f:
        out_f.write(template)
    out_f.close()

    print("Saved:",out_file)