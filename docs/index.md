# ITk at BNL

Welcome to the ITk at BNL website. The purpose of this website is to store documentation, code, and procedures for performing ITk related tasks at BNL. For a basic introduction to ITk at BNL, click below:

[ITk at BNL - Introduction](/Images/6_March_2023_ITk-at-BNL.pdf)

## Calendar

The calendar below contains important dates for ITk and BNL events, and dates during which personnel will be away from BNL:

<iframe src="https://calendar.google.com/calendar/embed?src=de81d2b6ee56e05532e3c771adbc762c0fe8d66a79e648f7eb5a90a0a4d66c33%40group.calendar.google.com&ctz=America%2FNew_York" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>

## Documentation and links

You may find the following links useful:

- [ITk Strips ColdJig Wiki](https://gitlab.cern.ch/groups/ColdJigDCS/-/wikis/home)
- [ColdBox CERNBox](https://cernbox.cern.ch/index.php/apps/files/?dir=/__myshares/ITK_ColdBox%20(id%3A240536)&)
- [ColdBox SharePoint (Currently being migrated to above GitLab Wiki)](https://espace.cern.ch/ITkColdBox/_layouts/15/start.aspx#/SitePages/Home.aspx)
- [ITSDAQ documentation](https://atlas-strips-itsdaq.web.cern.ch/)
- [Endcap documentation (has nice README for setting up)](https://gitlab.cern.ch/pekman/endcap_setup/-/blob/2058720a47ad274b575a30fbd35a2a45df945839/README.md)
- [ABCStarHybridModuleTests Twiki](https://twiki.cern.ch/twiki/bin/viewauth/Atlas/ABCStarHybridModuleTests)
- [ITk strips Twiki](https://twiki.cern.ch/twiki/bin/view/Atlas/ITkStrips)
- [BNL electrical results](https://gitlab.cern.ch/fcapocas/bnl-electrical-results)

## Software repositories

The following software repositories are commonly used for ITk at BNL tasks:

* [atlas-itk-strips-daq](https://gitlab.cern.ch/atlas-itk-strips-daq)
     * [itsdaq-sw](https://gitlab.cern.ch/atlas-itk-strips-daq/itsdaq-sw) &rarr; Software for data acquisition of ITk strips modules
          * NOTE that in order to access the documentation, you need to subscribe to the following egroups:
          * `atlas-upgrade-itk-strip-triggerDAQ`
          * `atlas-itk-production-database`
     * [itsdaq-fw](https://gitlab.cern.ch/atlas-itk-strips-daq/itsdaq-fw) &rarr; ITSDAQ firmware
* [ColdJigDCS](https://gitlab.cern.ch/ColdJigDCS)
     * [ColdJigLib2](https://gitlab.cern.ch/ColdJigDCS/coldjiglib2) &rarr; Software used to read/set/monitor hardware
     * [Coldbox_controller_WebGUI](https://gitlab.cern.ch/ColdJigDCS/coldbox_controller_webgui) &rarr; Software for producing web GUI to monitor and control hardware
* [atlas-itk](https://gitlab.cern.ch/atlas-itk)
* [ITk production database scripts](https://gitlab.cern.ch/atlas-itk/sw/db/production_database_scripts)
* [ITSDAQ-merger](https://gitlab.cern.ch/mghani/itsdaq-merger/) &rarr; Scripts used to merge ITSDAQ and coldjig jsons, and upload to DB. To eventually be integrated into ITSDAQ.
     * [Related slides](https://indico.cern.ch/event/1191858/contributions/5078438/attachments/2521798/4336652/ITSDAQ_ColdJig_Merger.pdf)
* [YARR](https://gitlab.cern.ch/YARR/YARR) &rarr; The software used for system tests (multiple staves) at SR1 at CERN
* [Possibly useful SHT software](https://github.com/jothanna/sht85/blob/master/sht85/__init__.py)

## Required trainings to enter the clean room

In order to gain access to the testing clean room, one must complete the following trainings on the [BNL training website](https://www.bnl.gov/training/):

* Computer Use Agreement GE-COMPUSE-AGREE
* Guest Site Orientation TQ-GSO
* Cyber Security GE-CYBERSEC
* Advanced Electrical Safety Awareness TQ-ESA 
* Hazard Communication HP-IND-200
* General Employee Radiological Training TQ-GERT 

## Website contact 

Abraham Tishelman-Charny

* Email: abraham.tishelman.charny@cern.ch
* Skype: "abe_tc"
* Mattermost: @atishelm

## Misc

* [ITk lessons (password = 123)](https://indico.cern.ch/event/1082956/)
* [Fun game for learning VIM](https://vim-adventures.com/)
