\documentclass[12pt]{article}
\usepackage{graphicx}
\usepackage{indentfirst}
\usepackage[margin = 1in]{geometry}
\usepackage{setspace}
\usepackage{hyperref}

\setstretch{1.25}

 \begin{document}
 
 \title{Stave Loading at BNL}
 \author{Author: Maya Mancini}
 \date{Last Updated: \today}
 \maketitle
 
 \pagenumbering {roman}
 \tableofcontents
 \pagebreak
 \pagenumbering{arabic}
 
 
 \section{Stave Orientation}
 
Because staves are 2-sided objects, we need a way to differentiate the 2 possible orientations. As you will see, there are a number of naming schemes for this very purpose (Left/Right hand, Primary/Secondary, etc). For this document, I will use Side J and Side L. Below, you can see the 2 orientations, their corresponding names, and useful components to be aware of.

\begin{figure}[h!]
\centering
\includegraphics[width=1\textwidth, height=0.2\textwidth]{Screenshots/Screenshot 2023-07-06 at 11.06.59 AM}
\caption{Stave: Side J}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphics[width=1\textwidth, height=0.2\textwidth]{Screenshots/Screenshot 2023-07-13 at 9.53.29 AM}
\caption{Stave: Side L}
\end{figure}
 
\section{Operating the Aerotech} 
\label{Operating the Aerotech}

There are a number of LabView programs (.vi's) that you can use to operate the Aerotech (as you'll see later on in the document). You will have the most control (and best response time), however, when using the A3200 CNC Operator Interface which is the native Aerotech software. It is an app found in Windows Explorer, which you can access from the home screen of the computer at the Aerotech station.

\pagebreak

\subsection{CNC App}

\begin{figure}[h!]
\centering
\includegraphics[width=0.55\textwidth, height=0.5\textwidth]{Screenshots/Capture1.PNG}
\caption{CNC Front Panel}
\end{figure}

On the front panel of the CNC app, you'll see there are a number of panels with useful information about the Aerotech stage. The first thing you'll want to check is the connection status down at the bottom lefthand corner. It should say "connected"\footnotemark.
\footnotetext{Reference the Trouble Shooting section at the end of this document to see what to do in the case that the CNC is NOT connected.}

\begin{figure}[h!]
\centering
\includegraphics[width=0.2\textwidth]{Screenshots/Screenshot 2023-05-25 at 9.50.31 AM}
\end{figure}

Once you check the connection status, the next thing you'll want to do is look to the upper lefthand corner where you'll see the "Control" and "Status" panels. The "Status" panel tells you whether each axis of the Aerotech is enabled or disabled. The stage will not move in the direction of a given axis if it is not enabled, so it is important to make sure you enable each axis before attempting to operate the Aerotech. To do so, you'll click the button with a green circle and grey circle under the "Control" panel (pictured below) to enable each axis. The X, Y, and Z axes are enabled separately, so you must enable all 3 of them.


\begin{figure}[h!]
\centering
\includegraphics[width=0.6\textwidth, height=0.15\textwidth]{Screenshots/Screenshot 2023-05-25 at 9.50.43 AM copy}
\end{figure}

Once enabled, the status of each axis will change to "enabled" under the "Status" panel and the enable buttons will each light up green.

Now that the axes are enabled, you can freely operate the Aerotech. To do so, you'll use the jog pad in the center of the panel.
As pictured, the X-axis corresponds to left and right movements and the Y-axis corresponds to up and down movements horizontal to the granite table on which the Aerotech stage sits. The Z-axis corresponds to up and down vertical movements, however, positive values are DOWN towards the table and negative movements are UP away from the table.

\begin{figure}[h!]
\centering
\includegraphics[width=0.4\textwidth]{Screenshots/Screenshot 2023-05-25 at 9.51.07 AM}
\end{figure}

To adjust the distance that the Aerotech moves, you'll look below the jog pad where you can manually input (in mm) the step size and jogging speed \footnotemark.\footnotetext{The jogging speed of 25mm/s is default and shouldn't be changed during any module loading or gluing unless told otherwise.} 

\begin{figure}[h!]
\centering
\includegraphics[width=0.4\textwidth]{Screenshots/Screenshot 2023-05-25 at 9.51.15 AM}
\end{figure}


You can move the stage any where within the limits \footnotemark:
X: -750,+750
Y: -150,+150
Z: -50, +50
\footnotetext{If you try to send the Aerotech out of bounds, it will throw an error.}

\pagebreak

\begin{figure}[h!]
\centering
\includegraphics[width=0.4\textwidth]{Screenshots/Screenshot 2023-05-25 at 9.50.49 AM}
\includegraphics[width=0.4\textwidth]{Screenshots/Screenshot 2023-05-25 at 9.50.56 AM}
\end{figure}

Above the jog pad, you'll find the "Program Position Feedback" and "Program Velocity Feedback" panels which tell you the current position and velocity of the Aerotech by axis.


\section{Reception}
Before assembly, BNL must first receive (or make) the necessary components. For the stave, you'll need to receive a stave core\footnotemark. \footnotetext{There are other components (i.e. EOS cards, DCDCs, termination boards, VTRX, etc), but those are usually received by other people.}The first step in reception is the visual inspection. Then, you'll conduct a bus tape facing survey and confocal scan\footnotemark. \footnotetext{The visual inspection is the only step you'll have to do during production.}
\subsection{Visual Inspection}
The visual inspection is exactly how it sounds. You want to take a look at both sides of a stave core noting any impurities or damage caused by shipping. If you do find damage or something odd, take a picture and report to a supervisor.

\subsection{Bus Dot Survey}
In order to do a bus tape facing/dot survey, you'll want to first set up the coordinate system of the stave as it is on the table\footnotemark.\footnotetext{You'll find instructions on how to do this in section 3.} With the \textbf{ContinuousCameraStageV2.vi} used to set up the coordinate system, you'll also want to find the location of the first bus tape dot and record it for the next step. 


\begin{figure}[h!]
\centering
\includegraphics[width=0.2\textwidth, height = 0.1\textwidth]{Screenshots/IMG_6949.jpeg}
\end{figure}

The bus tape "dots" are small, orange fiducial marks used to measure the bus tape facing after co-curing. They're near the locking points and, for this survey, you'll want to find the "00" dot. After recording the position of the first dot, you'll navigate to the folder: 

\begingroup
\footnotesize \noindent \textbf{Local Disk (C:)/Users/Aerotech/My Documents/BNL\textunderscore ThermomechanicalStave/Todd/BusTapeDots}
\endgroup

    \noindent Here, you'll open 2 .vi's from this folder (\textbf{BusTapeDotEdge.vi} and \textbf{FindAndMoveToDotManual.vi}) and operate them side by side. Before running the program, you'll first want to set all the inputs on the setup panels.

\begin{figure}[h!]
\centering
\includegraphics[width=0.7\textwidth, height=0.55\textwidth]{Screenshots/Capture48.PNG}
\end{figure}

\noindent \textbf{On the front panel of \textbf{BusTapeDotEdge.vi}:}

\begin{itemize}
\item Set Module Pitch: -98 for side J \textbf{or} 98 for side L
\end{itemize}

\begin{figure}[h!]
\centering
\includegraphics[width=0.3\textwidth, height=0.1\textwidth]{Screenshots/Screenshot 2023-05-25 at 4.48.30 PM}
\end{figure}



\noindent \textbf{"Camera Setup" Panel:}

\begin{itemize}
\item Set stave side: J \textbf{or} L
\item Select "Meas Bustape": button should turn green
\item \textbf{OPTIONAL:} Save mages (must choose save image path)
\end{itemize}

\pagebreak

\begin{figure}[h!]
\centering
\includegraphics[width=0.5\textwidth, height=0.42\textwidth]{Screenshots/Screenshot 2023-05-25 at 4.47.14 PM}
\end{figure}

\noindent \textbf{"Locking Point Setup" Panel:}

\begin{itemize}
\item First Locking Point: input X,Y,Z coord you recorded with \textbf{ContinuousCameraStageV2.vi} 
\item \textbf{OPTIONAL:}  select number of photos to take of the LPs
\end{itemize}

\begin{figure}[h!]
\centering
\includegraphics[width=0.5\textwidth, height=0.42\textwidth]{Screenshots/Capture49.PNG}
\end{figure}



\noindent \textbf{"Bus Tape Setup" Panel:}

\begin{itemize}
\item Number per Dot: set number of photos/measurements per fiducial dot (typically 3)
\item Number of Dots: should always be 14 as there are 14 modules per stave side
\item Pixel to Micron: get this value from the calibration file generated by \textbf{SurveyStave.vi} (instructions in section 3.2)
\item Dots Height: working distance (also from calibration file)
\item X LP to first dot: X-distance between first LP and fiducial dot
\item Y LP to first dot: Y-distance between first LP and fiducial dot
\end{itemize}

\begin{figure}[h!]
\centering
\includegraphics[width=0.5\textwidth, height=0.45\textwidth]{Screenshots/Capture50.PNG}
\end{figure}

\noindent Once the setup is done, you can run the \textbf{BusTapeDotEdge.vi}. \textbf{FindAndMoveToDotManual.vi} should be open to the side because you'll need the "Image Out 2" for live feedback.


\begin{figure}[h!]
\centering
\includegraphics[width=0.5\textwidth, height=0.45\textwidth]{Screenshots/Capture51.PNG}
\caption{FindAndMoveToDotManual.vi }
\end{figure}

Once the survey starts, the first step will be a survey of the locking points (these output values can later be used to determine their co-linearity). There is no input needed from the operator (you) while this is happening. After the locking points have been surveyed, the bus tape dot survey will begin with the "00" dot first. The Aerotech will position the camera over where it believes the first dot should be and the pattern recognition program will try to find the dot's actual position from there. Once the pattern recognition finds the dot, you will see the result on the under "Image Out 2". There will also be a pop-up window prompting you to adjust the X, Y, or Z position of the camera to center the dot or accept the position as is. If you choose to adjust the Aerotech position, do \textbf{NOT} check the box next to "Accept" before clicking "ok". This will record the current position and send the camera to survey the next dot. Only do this when you feel the circle has been centered as accurately as possible.

\begin{figure}[h!]
\centering
\includegraphics[width=0.5\textwidth, height=0.45\textwidth]{Screenshots/Screenshot 2023-05-30 at 10.59.22 AM}
\caption{"Image Out 2" feedback}
\end{figure}

\noindent \textbf{Green Circle}: search area


\noindent \textbf{Blue Circle}: found circle

The results of both surveys will be output to .csv files titled "dotsmatched.csv" and "lpmatched.csv". Typically, this process takes about half an hour.

\subsection{Confocal Scan of Bust Tape Facing}

The first step in conducting a confocal scan of a stave core is to ensure that the Keyence confocal laser head is attached to the Aerotech arm (pictured below).

\begin{figure}[h!]
\centering
\includegraphics[width=0.6\textwidth, height=0.67\textwidth]{Screenshots/IMG_6947.jpeg}
\end{figure}

Now, you'll position the confocal laser above the stave core surface and adjust the Z height until the Keyence feedback (small screen sitting atop the left corner of the computer monitor) reads $\sim$ 0. Make sure to record this height from the "Program Position Feedback" panel on the CNC as you'll need it in when setting up the confocal program.

Then, using \textbf{ContinuousCameraStageV2.vi}, you'll position the stave such that its bottom right corner is at an X-position of $\sim$615mm (this does not have to be super precise). The cross hairs of the camera should line up with the corner of the stave at this point. This step makes sure that the entire stave core is captured by the confocal scan. \textbf{Make sure you do NOT move the stave from this position before starting the program!}

Now that the stave core and Aerotech are setup properly, you'll navigate to the following folder on the computer:

\begingroup
\noindent  \footnotesize \textbf{Local Disk (C:)/Users/Aerotech/My Documents/BNL\textunderscore ThermomechanicalStave/BuildStave2}
\endgroup

\noindent Here, you'll open the program \textbf{ConfocalScan.vi}.

\begin{figure}[h!]
\centering
\includegraphics[width=0.5\textwidth, height=0.45\textwidth]{Screenshots/Capture26.PNG}
\end{figure}

\noindent Fill in the following setup inputs:

\begin{itemize}
\item Directory Path: select a folder for the resulting output files
\item Z Height: this is the z-position you recorded a few steps back
\item Y Height: height of the stave (116mm)
\item Y Resolution: distance between confocal scans (typically 2.5mm)
\end{itemize}

\noindent Once you start the program, there is no further user input needed. If using a Y resolution of 2.5mm, the program should take 45 minutes to complete. Once finished, you'll find one .csv file for each time the Aerotech passed over the stave numbered in sequential order : confocal\_scan\_\{num\}.csv

\section{ Setting Up Locking Point Coordinate System }
All of the .vi programs used to mount a stave require that you set up a coordinate system as described in the following section. Because the stave is essentially free floating on the granite table below the Aerotech, the position of the two relative to each other change with every new stave that's mounted. In order to achieve such accurate placement of the modules, we need to predetermine where the stave is before mounting. This process involves using the Aerotech to find specific points used to determine the "Locking Point Coordinate System" which is defined by the engineering drawings/schematics used for the stave. The .vi programs then use this information to transform these coordinates into Aerotech coordinates used to physically mount the modules and adjust their placement. 

The first step in the process, is setting up the stave on the table. Make sure the stave is flush with the blocks placed against the rail near the Aerotech stage (pictured below). It is also important to make sure that the locking points are above the white tape placed along the granite table (pictured below). This contrasts them from the background making the pattern recognition work efficiently. It is important that you align the stave this way \textbf{BEFORE} setting up the coordinate system. Once the stave is aligned, navigate to the following directory on the computer:

\begin{figure}[h!]
\centering
\includegraphics[width=1\textwidth, height=0.4\textwidth]{Screenshots/IMG_6933.jpeg}
\end{figure}


\begingroup
\noindent \footnotesize \textbf{Local Disk (C:)/Users/Aerotech/My Documents/BNL\textunderscore ThermomechanicalStave/Utils/StandAlone}
\endgroup

\begin{figure}[h!]
\centering
\includegraphics[width=0.5\textwidth, height=0.45\textwidth]{Screenshots/Capture7.PNG}
\end{figure}

\noindent Here, you'll open \textbf{ContinuousCameraStageV2.vi}

\subsection{ContinuousCameraStageV2.vi}
This .vi gives you a live view of what the Aerotech camera sees. It allows you to record the coordinates on the stave locations needed to generate the Locking Point Coordinate System. As you can see on the front panel, this .vi has a number of functionalities. For this procedure, you'll first want to navigate to the "Camera Controls" tab.

\begin{figure}[h!]
\centering
\includegraphics[width=0.6\textwidth, height=0.55\textwidth]{Screenshots/Capture8.PNG}
\end{figure}

Here, you'll do the following:

\begin{itemize}

\item Make sure the "Camera Analysis Function" drop down menu is set to "[NONE]"
\item Turn on the "Primary Light" and "Spotlights"
\item One the "Camera Setup" sub-tab:
\begin{itemize}
\item Turn "Gain of the Camera" to $\sim$16
\end{itemize}
\end{itemize}

On the "Stage Controls" Panel, you have the option to move the Aerotech around using the "Jogging Controls". You can also use the CNC program to do this, but they both achieve the same goal so the choice is up to user preference. Simply clicking the left and right arrows will move the Aerotech in the longitudinal direction along the stave at whatever step size is pre-selected. Similarly, clicking the up and down arrow will move the Aerotech stages in the transverse direction. The up and down buttons (in purple) move the Aerotech in the Z-direction towards or away from the stave\footnotemark. \footnotetext{Unlike the CNC program, the up button actually moves the stage up and away instead of towards the stage (and vice versa)}

\begin{figure}[h!]
\centering
\includegraphics[width=0.5\textwidth, height=0.45\textwidth]{Screenshots/Screenshot 2023-05-30 at 2.42.20 PM}
\end{figure}

\pagebreak

If there is a specific position that you want to go to, you can do that by typing in the coordinates under "Move to Absolute Position [mm]" then clicking the "Move stage (abs)" button.

\noindent \textbf{Step 1:}

Using the preferred method of motion control, navigate to the corner of the stave making sure to align it with the cross-bars\footnotemark. \footnotetext{To ensure that the stave corner is in focus, the default Z-coordinate should be 16mm for side J and 22mm for side L} on the "current field of view" live feed back panel.

\begin{figure}[h!]
\centering
\includegraphics[width=0.7\textwidth, height=0.45\textwidth]{Screenshots/Capture11.PNG}
\end{figure}

\noindent \textbf{Step 2:}

Record the current position of the Aerotech \footnotemark \footnotetext{You can right this in a clean room notebook or in a text file} from either the "Current Stage Position" panel on \textbf{ContinuousCameraStageV2.vi} or the position feedback panel on the CNC program.

\noindent \textbf{Step 3:}

Repeat this step by finding the first cone on the first locking point and recording its coordinate. \footnotemark \footnotetext{The default Z-height should be 22mm for Side J and 16mm for Side L} 

\begin{figure}[h!]
\centering
\includegraphics[width=0.7\textwidth, height=0.45\textwidth]{Screenshots/Capture10.PNG}
\end{figure}


\noindent \textbf{Step 4:}

Find the dummy sensor box, and place it (using the vacuum pack) onto the module 0 location. \footnotemark \footnotetext{This does not have to be precise, just ensure that the sensor is in the general location of module 0} 

\begin{figure}[h!]
\centering
\includegraphics[width=0.35\textwidth, height = 0.45\textwidth]{Screenshots/IMG_6930.jpeg}
\includegraphics[width=0.5\textwidth, height = 0.45\textwidth]{Screenshots/IMG_6932.jpeg}
\end{figure}

\noindent \textbf{Step 5:}

Turn the gain of the camera down to $\sim$11 and navigate to the sensor. Here, you'll want find an area free of dust or damage and record the coordinates like with the stave corner and locking point.

\begin{figure}[h!]
\centering
\includegraphics[width=0.7\textwidth, height=0.45\textwidth]{Screenshots/Capture9.PNG}
\end{figure}

\noindent \textbf{Step 6:}

Close this program by clicking the "Terminate Program" button in the the top right corner of the front panel. It will turn green when you click it once. Make sure to click it twice ensuring that it turns red again.\footnotemark \footnotetext{This ensures all the ports are properly closed} 

\begin{figure}[h!]
\centering
\includegraphics[width=0.45\textwidth, height = 0.15\textwidth]{Screenshots/Screenshot 2023-07-13 at 3.48.45 PM}
\includegraphics[width=0.45\textwidth, height = 0.15\textwidth]{Screenshots/Screenshot 2023-07-13 at 3.53.13 PM}
\end{figure}

\subsection{SurveyStave.vi}

Once you have recorded the coordinates of the first locking point, stave corner, and dummy sensor, you'll navigate to:  


\begingroup
\noindent  \footnotesize \textbf{Local Disk (C:)/Users/Aerotech/My Documents/BNL\textunderscore ThermomechanicalStave/BuildStave2}
\endgroup

\pagebreak

\begin{figure}[h!]
\centering
\includegraphics[width=0.7\textwidth, height=0.45\textwidth]{Screenshots/Capture13.PNG}
\end{figure}

\noindent Here, you'll open \textbf{SurveyStave.vi}, this .VI surveys the 3 key points needed to establish the locking point coordinate system as well as set the pix-um conversion and ideal working height of the Aerotech.

To start the program, set up the following pre-sets:

\noindent \textbf{Configuration}

\begin{itemize}
\item Choose the correct stave orientation by click the "Stave Orientation" button until it reflects the stave side you're working on
\item Leave all the other default pre-sets as is
\end{itemize}

\begin{figure}[h!]
\centering
\includegraphics[width=0.5\textwidth, height=0.42\textwidth]{Screenshots/Screenshot 2023-07-13 at 4.11.31 PM}
\end{figure}

\pagebreak

\noindent \textbf{Calibration}

\begin{itemize}
\item input the stave corner coordinate into the \textbf{Stave Origin} box
\item input the dummy sensor coordinate into the \textbf{Cam Calib} box
\item input the locking coordinate into the \textbf{Calib Pattern} box
\end{itemize}


\begin{figure}[h!]
\centering
\includegraphics[width=0.5\textwidth, height=0.4\textwidth]{Screenshots/Screenshot 2023-07-13 at 4.11.54 PM}
\end{figure}


Now, you can run the VI and follow these steps:


\noindent \textbf{Step 1:}

It will first prompt you to select an output folder or create one. This should be the name of the stave AND should be located on the "D" drive as this folders take up too much space on the computer. 

\noindent \textbf{Steps 2-4:}

The Aerotech will move to each of the 3 coordinates you gave it which gives you to double check that everything is where you expect it to be. Make sure that the stave corner, locking point, and sensor show up in the current field of view before clicking accept.

\begin{figure}[h!]
\centering
\includegraphics[width=0.3\textwidth, height = 0.3\textwidth]{Screenshots/Capture15.PNG}
\includegraphics[width=0.3\textwidth, height = 0.3\textwidth]{Screenshots/Capture16.PNG}
\includegraphics[width=0.3\textwidth, height = 0.3\textwidth]{Screenshots/Capture17.PNG}
\end{figure}

\noindent \textbf{Step 5:}

Next, you will be prompted to run the camera calibration. If this is a brand new coordinate system, select yes. Otherwise, you can select a previous calibration file.

\begin{figure}[h!]
\centering
\includegraphics[width=0.6\textwidth, height = 0.2\textwidth]{Screenshots/Capture18.PNG}
\end{figure}

\noindent \textbf{Step 6:}

Click "OK" to begin the height calibration and pix-um conversion.

\begin{figure}[h!]
\centering
\includegraphics[width=0.4\textwidth, height = 0.2\textwidth]{Screenshots/Capture19.PNG}
\end{figure}

\noindent \textbf{Step 7:}

The "Calibrate Camera" window will pop up while the calibration is happening. Make sure to that the "Height" tab is selected immediately after it does.

\begin{figure}[h!]
\centering
\includegraphics[width=0.7\textwidth, height = 0.42\textwidth]{Screenshots/Capture20.PNG}
\end{figure}

\pagebreak

\noindent \textbf{Step 8:}

After the calibration is complete, a height plot will appear in addition to a window that will prompt you to accept or adjust the height (working distance). As long as it is $\sim$approximately Gaussian (unlike the picture), accept the calibration. 

\begin{figure}[h!]
\centering
\includegraphics[width=0.7\textwidth, height = 0.45\textwidth]{Screenshots/Capture21.PNG}
\end{figure}

\noindent \textbf{Step 9:}

The next window will ask you to name the stave and add any notes if needed. Click "OK" when done.

\begin{figure}[h!]
\centering
\includegraphics[width=0.7\textwidth, height = 0.42\textwidth]{Screenshots/Capture22.PNG}
\end{figure}

\pagebreak

\noindent \textbf{Step 10:}

Close this window and terminate the .VI the same way you terminated the previous one (clicking "Terminate Program" twice)

\begin{figure}[h!]
\centering
\includegraphics[width=0.45\textwidth, height = 0.35\textwidth]{Screenshots/Capture22 copy.PNG}
\includegraphics[width=0.45\textwidth, height = 0.35\textwidth]{Screenshots/Capture13 copy.PNG}
\end{figure}

Now, there should be a file in the output folder you created called \textbf{"CalibrationResults.ini"}. This file contains information about those 3 points you found on the stave manually as well as the calibrated working distance and the pix-um conversion.

\subsection{VirtualStave.vi}

With this calibration file generated, you can finish generating the coordinate system needed to mount modules by first navigating to:

\begingroup
\noindent  \footnotesize \textbf{Local Disk (C:)/Users/Aerotech/My Documents/BNL\textunderscore ThermomechanicalStave/BuildStave2}
\endgroup

and opening up \textbf{VirtualStave.vi}

\begin{figure}[h!]
\centering
\includegraphics[width=0.7\textwidth, height = 0.42\textwidth]{Screenshots/Capture23.PNG}
\end{figure}

\pagebreak

Before running the .VI, select the following file:

\begin{itemize}

\item \textbf{"configuration file path"} should always be\footnotemark: \begingroup \footnotesize \textbf{Local Disk (C:)/Users/Aerotech/My Documents/BNL\textunderscore ThermomechanicalStave/BuildStave2/StaveConfig\textunderscore File.ini} \endgroup

\footnotetext{This is a configuration file used for stave mounting and it should be the default value}

\item \textbf{"calibration file path"} should be the path to the calibration file you just created with \textbf{SurveyStave.vi}

\item \textbf{"Path to Python Script Directory"} should always be\footnotemark: \begingroup \footnotesize \textbf{Local Disk (C:)/Users/Aerotech/My Documents/BNL\textunderscore ThermomechanicalStave/BuildStave2/subvi} \endgroup

\footnotetext{This is the path to a directory containing a python script that is used to generate the coordinates used by the Aerotech}

\item \textbf{OPTIONAL:} You can choose to save the images that are taken of the locking points during the survey. If you do, make sure to select the output directory that you want the images to end up in.

\end{itemize}

\begin{figure}[h!]
\centering
\includegraphics[width=0.5\textwidth, height=0.4\textwidth]{Screenshots/Capture23 copy.PNG}
\end{figure}

Once all the necessary file paths are chosen, you can run the program. The first part of the .VI will do a survey of the locking points, you will see this through the "Image" panel. Make sure to watch the feedback images to ensure that there is no lighting issue that will disrupt the pattern recognition. When the locking point survey is complete, the program will return to the corner of the stave and find the edges using pattern recognition. All of this information will be sent to the python script which will then output a file 

\textbf{"virtualstave-Mark E (Slim 17).csv"}

\noindent with the nominal coordinates of each of the modules in the Aerotech system. You will also get immediate feedback from the script on the Debug Panel at the top of this .VI. This should be monitored to ensure that the script ran as expected\footnotemark.

\footnotetext{Refer to the Trouble Shooting section for instructions on what to do if an error is encountered}

\begin{figure}[h!]
\centering
\includegraphics[width=0.7\textwidth, height=0.3\textwidth]{Screenshots/Capture23 copy 2.PNG}
\end{figure}

\section{Dry Mounting Modules}

With the nominal module coordinates generated, the next step in the mounting process is to "Dry" mount the modules without glue to pre-align them. To start this process, first navigate to:

\begingroup
\noindent  \footnotesize \textbf{Local Disk (C:)/Users/Aerotech/My Documents/BNL\textunderscore ThermomechanicalStave}
\endgroup

\noindent and open \textbf{Run\textunderscore VMP2.vi}

\subsection{Run\textunderscore VMP2.vi}

\begin{figure}[h!]
\centering
\includegraphics[width=0.7\textwidth, height = 0.42\textwidth]{Screenshots/Capture27.PNG}
\end{figure}

This is the program that interfaces with the camera to get a live view of the modules on the stave core. It uses the coordinates made in the previous step to place the camera directly above the nominal module locations. Using pattern recognition, the program finds the fiducial marks on each of the four corners of the modules in real time while simultaneously displaying their correct location to aid in the placement accuracy. 


To start the program, first input the path to the directory in which the \textbf{CalibrationResults.ini} and \textbf{virtualstave-Mark E (Slim 17).csv} files are located into the "Calibration Directory" box. Then, run the .VI which should bring up the window pictured below.


\begin{figure}[h!]
\centering
\includegraphics[width=0.7\textwidth, height = 0.42\textwidth]{Screenshots/Capture28.PNG}
\end{figure}

\noindent \textbf{Camera Placement Control:}

\begin{figure}[h!]
\centering
\includegraphics[width=0.7\textwidth, height = 0.22\textwidth]{Screenshots/Capture29 copy 2.PNG}
\end{figure}

This section of the front panel is used to send the Aerotech to a given corner on an individual module.

\noindent \textbf{Step 1:}

Select which module you want to work on (between 0 and 13) from the drop-down menu next to "Work on Module"

\noindent \textbf{Step 2:}

Select which corner you want to look at (A,B,C, or D). *For reference, pictured below is the convention we use for naming the module corners, it is true for both Side J and L.

\begin{figure}[h!]
\centering
\includegraphics[width=1\textwidth, height = 0.15\textwidth]{Screenshots/Screenshot 2023-03-27 at 3.28.35 PM}
\end{figure}


\noindent \textbf{Step 3:}

Click "Done with Module \textunderscore \textunderscore, Corner \textunderscore \textunderscore" and the camera will position itself over the nominal location of the fiducial mark for the module and corner that you selected.

\begin{figure}[h!]
\centering
\includegraphics[width=0.7\textwidth, height = 0.15\textwidth]{Screenshots/Capture29 copy 4.PNG}
\end{figure}

\noindent \textbf{Placement Monitor:}

\begin{figure}[h!]
\centering
\includegraphics[width=0.5\textwidth, height = 0.42\textwidth]{Screenshots/Capture29 copy.PNG}
\end{figure}


This is where you'll find the live feedback from the camera. The cross-hairs are positioned at the center of where the fiducial mark should be. *To zoom in, simply click anywhere on the image with the magnify tool\footnotemark.

\footnotetext{During the dry-mount and final mount, you'll want to zoom in on the cross-hair by click directly on the cross section}



\noindent \textbf{Pattern Match Parameters:}

\begin{figure}[h!]
\centering
\includegraphics[width=0.5\textwidth, height = 0.42\textwidth]{Screenshots/Capture29 copy 3.PNG}
\end{figure}

This tab should generally be untouched. However, in the case that the pattern recognition does not find a fiducial mark, click the "Restrict Search" button so it turns from dark to light green. This restricts the area of the feedback image where the pattern recognition is looking for the fiducial. You can change the width of the box in which it scans for the fiducial as well as where the center of the box is drawn\footnotemark.

\footnotetext{The default draws a box of 250 pixels wide at the center of the screen where the cross-hairs are.}


To dry-mount or pre-align the modules, a technician will use the blue support bridges to place the modules in the general area of their nominal location. Then, using the .VI feedback, they will make fine adjustments to get all 4 fiducial marks on each module as accurate as possible.


\section{Gluing Modules to the Stave}

Once dry mounting is complete, the modules/bridges will be removed from the stave core for the first batch of glue\footnotemark. To start the gluing program, navigate to: 

\footnotetext{In production, you will: put down 5 glues patterns $\rightarrow$ load 4 modules $\rightarrow$ put down 5 glue patterns $\rightarrow$ load 5 modules $\rightarrow$ put down 4 glue patterns $\rightarrow$ load 5 modules}

\begingroup
\noindent  \footnotesize \textbf{Local Disk (C:)/Users/Aerotech/My Documents/BNL\textunderscore ThermomechanicalStave/BuildStave2}
\endgroup

\noindent and open \textbf{RunGlue\textunderscore V2.vi}

\subsection{RunGlue.vi}

\begin{figure}[h!]
\centering
\includegraphics[width=0.7\textwidth, height = 0.42\textwidth]{Screenshots/Capture36.PNG}
\end{figure}

On the front panel of \textbf{RunGlue\textunderscore V2.vi}, you'll

\subsection{Techcon Glue Dispenser}
\subsection{Preparing the Glue}
\subsection{RunGlue.vi}


\section{Module Placement (with Glue)}


\section{Metrology}
\subsection{XY Fiducial Survey}
\subsection{Loaded Stave Confocal Survey}
This is the exact same process as the reception confocal scan. This is often repeated after loading to check for any modules that might have lifted up thus affecting the clearance of the stave in the barrel \footnotemark. 
\footnotetext{This may not be done during production.}


\section{Maintenance}
\subsection{Vignetting for Camera Alignment}
\subsection{Homing the Aerotech Axes}

\section{Trouble Shooting}
\subsection{Sending the Aerotech Out of Bounds}
\subsection{IMAQ Camera Error}
\subsection{Techcon Malfunction}
\subsection{VirtualStave.vi Not Finding Edge of Stave}
If any of the outputs on this panel are "NaN", the pattern recognition of the stave edge has failed and 

\subsection{VirtualStave.vi Not Finding Locking Points}


\end{document}