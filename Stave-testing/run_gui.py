import tkinter as tk
from tkinter import ttk
#from tkinter.messagebox import showinfo
import time
import sys


def update_gui(f, root, labelVars):
    returnedDictionary = f()
    ports = returnedDictionary['ports']
    RH = returnedDictionary['RH']
    temps = returnedDictionary['SHTtemps']
    temps2 = returnedDictionary['TC08temps']
    dewpoints = returnedDictionary['dewpoints']
    for i in range(len(labelVars)):
        if labelVars[i] != None:
            labelVars[i].set(f"          Port:   {i}       \n\n       RH: {RH[ports.index(i)]:.3}% \n  SHTtemp: {temps[ports.index(i)]:.3}  \n  TC08temp: {float(temps2[ports.index(i)]):.4} \n      DP: {dewpoints[ports.index(i)]:.5}\n")
    root.after (5000, update_gui, f, root, labelVars)
    return

def run_gui(f):
    returnedDictionary = f()
    ports = returnedDictionary['ports']
    RH = returnedDictionary['RH']
    temps = returnedDictionary['SHTtemps']
    temps2 = returnedDictionary['TC08temps']
    dewpoints = returnedDictionary['dewpoints']
   
   
    root = tk.Tk()
    root.title('Stave Testing GUI')

    rootWidth = 500
    rootHeight = 600
    root.geometry(f'{rootWidth}x{rootHeight}')

   
    xLoc = 50
    yLoc = 50
    displayPorts = [0,1,2,3,4,5,6,7]
    usedPorts = []
    labelVars = []
    
    for i in range(8):
    
        if displayPorts[i] in ports:
                        
            lv = tk.StringVar()
            labelVars.append (lv)
            lv.set(f"          Port:   {i}       \n\n       RH: {RH[ports.index(i)]:.3}% \n  SHTtemp: {temps[ports.index(i)]:.3}  \n  TC08temp: {float(temps2[ports.index(i)]):.4} \n      DP: {dewpoints[ports.index(i)]:.5}\n")
            
            displayPorts[i] = ttk.Label(root, background='lightgreen', textvariable=lv)
            if xLoc%2 == 0:
                displayPorts[i].place(x=xLoc, y=yLoc)
                xLoc += 129
            else:
                displayPorts[i].place(x=xLoc, y=yLoc)
                xLoc -= 129
                yLoc += 125

        else:
            labelVars.append (None)
            displayPorts[i] = ttk.Label(root, background='crimson', text= f"\n\n\n                             -\n\n\n", foreground= 'crimson')
            displayPorts[i].place(x=xLoc, y=yLoc)
            if xLoc%2 == 0:
                displayPorts[i].place(x=xLoc, y=yLoc)
                xLoc += 129
            else:
                displayPorts[i].place(x=xLoc, y=yLoc)
                xLoc -= 129
                yLoc += 125

   
    root.configure(background='black')
   
    update_gui(f, root, labelVars)
   
    root.mainloop()
    

#run_gui(returnValues)
     
