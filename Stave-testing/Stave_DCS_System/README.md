# Stave DCS system

The purpose of this directory is to store files used to run the Stave DCS system at BNL. To run:

1. Connect to stave testing PC 
2. SSH into rapsberry pi
3. `cd <workingDirectory>`
4. `python3 cps.py`
