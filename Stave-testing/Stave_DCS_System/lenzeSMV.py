'''
  Program ChillerDevices.py
  
Description: ------------------------------------------------------------------
   This file contains the class construct to perform I/O with the devices used
in the UK ATLAS ITk stave testing equipment.  The current equipment/devices 
connected are:

   Flir A655sc IR camera.
   LaudaPro 290 recirculating cooler,
   Lenze ESV751N02YXC NEMA 4x inverter drive to control booster pump,
   Pico TC-08 Thermocouple digitiser
   Raspberry pi model 4B connected to miscelanneous hats to read 
     NTC and SHT85 devices through

History: ----------------------------------------------------------------------
   V1.0 - Jul-2020  First public release.

Environment: ------------------------------------------------------------------
   This program is written in Python 3.7 running on rpi model 4B

Author List: -------------------------------------------------------------------
  R. McKay    Iowa State University, USA  mckay@iastate.edu
  J. Yu       Iowa State University, USA  jieyu@iastate.edu
  W. Heidorn  Iowa State University, USA  wheidorn@iastate.edu
  
Notes: -------------------------------------------------------------------------

Dictionary of abbreviations: ---------------------------------------------------
  bol - boolean
  cls - class
  flt - float
  int - integer
  str - string
'''

# function __init__: 
#   initialize the class device
#   should include Configure Instance as input???
# function read( strCmdName ):
#   provide the command name
#   return information from the device
# function write( strCmdName ):
#   provide the command name
#   write to device if necessary
#   e.g. change Chiller setup temperature
# function getdevice( strDevName ):
#   provide device name
#   return the device instance
#
#


# Import section --------------------------------------------------------------

import serial  # https://github.com/pyserial/pyserial, install: pip3.6 install pyserial
import time
import logging
import libscrc

# ------------------------------------------------------------------------------
# --------------------------- Booster pump class -------------------------------
# ------------------------------------------------------------------------------

class lenzeSMV():
  def __init__(self, strPort, intBaud=9600, bytesize=serial.EIGHTBITS, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_TWO, timeout=1):
    """
      Initialize booster pump inverter (i.e. controller) serial protocol and define the 
      boundries of the frequency (i.e. RPS) we will allow the booster pump to run.
    """

#  *** Configuration for ESV751N02YXC booster pump inverter. ***
#    Computer USB port settings:  9600 baud, 8bit, 2 stop, no parity.
#PORT      =  ttyUSB0  #USB port connected to inverter.
#BAUD      =  9600     #Set the baud rate to match the inverter.
#BITS      =  8        #Set the number of data bits. 7 or 8
#PARITY    =  None     #Set the parity bit to match the inverter.
#STOPBITS  =  2        #Choices 1 or 2
#TIMEOUT   =  0        #0: read immediately, None: wait forever, X: wait X s

    self._pdev = serial.Serial(strPort, intBaud,bytesize,parity,stopbits,timeout) 


# manual: https://download.lenze.com/TD/ESV__SMV%20Modbus%20module__v3-0__EN.pdf
#
# Write:
# Request:   SA  06  RH  RL  DH  DL  CRH CRL
# Response:  SA  06  RH  RL  DH  DL  CRH CRL
#
# Read 16 bits:
# Request:   SA  03  RH  RL  00  01  CRH CRL
# Response:  SA  03  04  D1H D1L D2H D2L CRH CRL


  def Lock(self):
    # Lock all writable registers
    self.SendCommand(1, 1, 2) # Write Reg 1 Bit 1

  def Start(self):
    # Start the drive (controls must be unlocked) - WORKING
    print('PUMP:Start')
    self.SendCommand(1, 1, 8) # Write Reg 1 Bit 3

  def Stop(self):
    # Stop the drive (controls need not be unlocked) - WORKING
    print('PUMP:Stop')
    self.SendCommand(1, 1, 4) # Write Reg 1 Bit 2 - P111 STOP


  def GetStatus(self):
    print('PUMP:GetStatus')
    iStatus = self.SendCommand(0, 23, 1) # Read Reg 23, Drive Status
    return iStatus

  def GetCmdFreq(self):
    print('PUMP:GetCmdFreq')
    iCmdFreq = self.SendCommand(0, 44, 1) # Read Reg 24, Command(ed) Frequency
    return 0.1 * float(iCmdFreq)

  def GetActFreq(self):
    print('PUMP:GetActFreq')
    iActFreq = self.SendCommand(0, 25, 1) # Read Reg 25, Actual Frequency
    return 0.1 * float(iActFreq)

  def GetCtrlMode(self):
    print('PUMP:GetCtrlMode')
    iStatus = self.SendCommand(0, 27, 1) # Read Reg 27, Drive Status
    return iStatus


  def SetCmdFreq(self, rps):
    # Set the drive frequency (parameters must be unlocked)

    fCmdFreq = float(rps)
    if fCmdFreq < 10.0 :
       fCmdFreq = 10.0
    if fCmdFreq > 50.0 :
       fCmdFreq = 50.0

    iCmdFreq = int(10 * fCmdFreq)

    self.SendCommand(1, 44, iCmdFreq) # Write Reg 44: Network Speed Command

  # Next two commands rely upon drive password P194 to be set to 0
  def UnlockControl(self):
    # allow write access to Drive Control (Register 1) only - WORKING
    print('PUMP:UnlockControl')
    self.SendCommand(1, 48, 0) # Write Reg 48, Value 1

  def UnlockParameters(self):
    # allow write access to all registers EXCEPT Drive Control (Register 1) - WORKING
    print('PUMP:UnlockParameters')
    self.SendCommand(1, 49, 0) # Write Reg 49, Value 0 


  def SendCommand(self, doWrite, reg, val):

    debug = 0

    intReg = int(reg) 
    strReg = '{:04x}'.format(intReg) 
    strReg = strReg.upper()

    intVal = int(val)
    strVal = '{:04x}'.format(intVal) 
    strVal = strVal.upper()

    if doWrite == 0 :
       strCmd = "0103" + strReg + strVal
    else :
       strCmd = "0106" + strReg + strVal


    strCmdByteArr = bytearray.fromhex( strCmd )
    crc16 = libscrc.modbus(strCmdByteArr)

    strHexCRC = format(crc16, '04x')
    strHexCRC = strHexCRC[2:] + strHexCRC[0:2]  # Must swap bytes as CRC calculator result is little endian.
    strFinal = strCmd + strHexCRC.upper()

    if debug:
      print(' PUMP: Sending  ' + strFinal)
    self._pdev.write( bytes.fromhex(strFinal))


    rcode = 0
    if doWrite == 0 :
      # Read 16 bits:
      # Request:   SA  03  RH  RL  00  01  CRH CRL
      # Response:  SA  03  02  DH  DL  CRH CRL

      byteline = self._pdev.read(7)
      strResponse = byteline.hex()
      strResponse = strResponse.upper()
      strLen = len(strResponse)
      if debug:
        print(' PUMP: Received ' + strResponse)

      # Check length is as expected
      if strLen != 14 :
        logging.error(' PUMP: SendCommand (W) expected 14 chars but got ' + str(strLen))

        if strLen == 10 :
          logging.error('PUMP: Error code ' + strResponse[4:6])
          return -1
        return -2

      # Check first four bytes are the same
      if strResponse[0:4] != strFinal[0:4]:
        logging.error(' PUMP: SendCommand (R) expected ' + strFinal[0:4] + " but got " + strResponse[0:4])
        return -3

      # Reply is NOT byte swapped
      strReply = strResponse[6:10]

      if debug:
        print(' PUMP: will return ' + strReply)
      return int(strReply, 16)


    else:
      # Write:
      # Request:   SA  06  RH  RL  DH  DL  CRH CRL
      # Response:  SA  06  RH  RL  DH  DL  CRH CRL

      byteline = self._pdev.read(8)
      strResponse = byteline.hex()
      strResponse = strResponse.upper()
      strLen = len(strResponse)
      if debug:
        print(' PUMP: Received ' + strResponse)

      # Check length is as expected
      if strLen != 16 :
        logging.error(' PUMP: SendCommand (W) expected 16 chars but got ' + str(strLen))
        if strLen == 10 :
          logging.error('PUMP: Error code ' + strResponse[4:6])
          return -1
        return -2

      # Check all bytes are the same
      if strResponse[0:16] != strFinal[0:16]:
        logging.error(' PUMP: SendCommand (W) expected ' + strFinal + " but got " + strResponse)
        return -2

    return 0

