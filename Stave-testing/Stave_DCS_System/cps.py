#
# Copyright (C) 2019 Pico Technology Ltd. See LICENSE file for terms.
#
import time
from datetime import datetime
from influxdb import InfluxDBClient
import ctypes
import numpy as np
from picosdk.usbtc08 import usbtc08 as tc08
from picosdk.functions import assert_pico2000_ok
import socket
from lenzeSMV import lenzeSMV
from sht85 import sht85
from hyt221 import hyt221
from ADCPi import ADCPi
import tkinter

import threading
import math

lock=threading.Lock()

haveChiller = 1
haveNTCs = 1
havePump = 1
haveSHTs = 2
haveHYTs = 2
haveTC08 = 1


class clsCPS():
# GUI callbacks

   def cbChillPS(self):
      value = int(self.entChillPS.get())
      if value > 8:
          self.entChillPS.delete(0,10)
          self.entChillPS.insert(10, "too high")
          return
      if value < 1:
          self.entChillSP.delete(0,10)
          self.entChillPS.insert(10, "too low")
          return
      with lock:
         strCmd = ("OUT_SP_01_%.2f\r\n" % value)
         self.chiller.sendall(str.encode(strCmd))
         self.entChillPS.delete(0,10)
         self.entChillPS.delete(0,10)
         self.entChillPS.insert(10, self.chiller.recv(32)[0:2]) 

   def cbChillSP(self):
      value = float(self.entChillSP.get())
      dT = value - self.fltchillSP
      print("new", value,"old",self.fltchillSP,"dT",dT,"margin",self.margin)
      if value > 30.0:
          self.entChillSP.delete(0,10)
          self.entChillSP.insert(10, "too high")
          return
      if value < -50.0:
          self.entChillSP.delete(0,10)
          self.entChillSP.insert(10, "too low")
          return
      if (self.margin + dT) < 5:
          self.entChillSP.delete(0,10)
          self.entChillSP.insert(10, "too close")
          return
      with lock:
         strCmd = ("OUT_SP_00_%.2f\r\n" % value)
         self.chiller.sendall(str.encode(strCmd))
         self.entChillSP.delete(0,10)
         self.entChillSP.insert(10, self.chiller.recv(32)[0:2]) 

   def cbChillStart(self):
      print("Cool man")
      self.butChillStart.configure(state = "disabled")
      self.butChillStop.configure(state = "normal")
      self.butPumpStart.configure(state = "normal")
      self.butPumpStop.configure(state = "disabled")
      with lock:
         strCmd = ("START\r\n")
         self.chiller.sendall(str.encode(strCmd))
         strResponse = self.chiller.recv(32) 

   def cbChillStop(self):
      print("Heat man")
      self.butChillStart.configure(state = "normal")
      self.butChillStop.configure(state = "disabled")
      self.butPumpStart.configure(state = "disabled")
      self.butPumpStop.configure(state = "disabled")
      with lock:
         strCmd = ("STOP\r\n")
         self.chiller.sendall(str.encode(strCmd))
         strResponse = self.chiller.recv(32) 

   def cbPumpCF(self):
      value = float(self.entPumpCF.get())
      if value > 50.0:
          self.entPumpCF.delete(0,10)
          self.entPumpCF.insert(10, "too high")
          return
      if value < 10.0:
          self.entPumpCF.delete(0,10)
          self.entPumpCF.insert(10, "too low")
          return
      with lock:
         self.pump.SetCmdFreq(value)

   def cbPumpStart(self):
      #self.butChillStart.configure(state = "disabled")
      #self.butChillStop.configure(state = "disabled")
      self.butPumpStart.configure(state = "disabled")
      self.butPumpStop.configure(state = "normal")
      with lock:
         self.pump.Start()

   def cbPumpStop(self):
      #self.butChillStart.configure(state = "disabled")
      #self.butChillStop.configure(state = "normal")
      self.butPumpStart.configure(state = "normal")
      self.butPumpStop.configure(state = "disabled")
      with lock:
         self.pump.Stop()


   def cbMonitor(self):

      start_time = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
      start_ms = (time.time_ns() / 1000000)

      intchillSTB = 0
      if haveChiller:
         # Lauda chiller queries
         # IN_PV_00 bath temperature
         # IN_PV_05 level query
         # IN_SP_00 temperature setpoint
         # IN_SP_01 pump power level
         # IN_SP_02 query cooling mode
         # IN_SP_03 overtemperature switch-off point
         # IN_SP_04 temperature limit TiH (upper value)
         # IN_SP_05 temperature limit TiL (lower value)
         # IN_SP_07 safety mode temperature set point
         # IN_SP_08 interface communication timeout

         # Read Chiller Set Point
         self.chiller.sendall(b'IN_SP_00\r\n')
         self.fltchillSP = float(self.chiller.recv(32)) 
         self.strChillSP.set(str(self.fltchillSP))

         # Read Chiller Pump Speed
         self.chiller.sendall(b'IN_SP_01\r\n')
         intchillPS = int(self.chiller.recv(32)) 
         self.strChillPS.set(str(intchillPS))

         # Read Chiller Bath Temp
         self.chiller.sendall(b'IN_PV_00\r\n')
         fltchillBT = float(self.chiller.recv(32)) 
         self.strChillBT.set(str(fltchillBT) + " C")

         # Read Chiller Bath Level
         self.chiller.sendall(b'IN_PV_05\r\n')
         intchillBL = int(self.chiller.recv(32)) 
         self.strChillBL.set(str(intchillBL))

         # Read Chiller Standby Status
         # - returns 1 if chiller is in STANDBY eg OFF
         self.chiller.sendall(b'IN_MODE_02\r\n')
         intchillSTB = int(self.chiller.recv(32)) 

      if havePump:
         # turn off pump if chiller is stopped:
         # chiller may stop due to low level, this will minimise losses if due to leakage
         # However one must also turn off power
         if intchillSTB > 0:
            print('Chiller is in STANDBY so I am turning off the pump')
            self.cbPumpStop()

         # Read Pump Status
         intpumpStat =  self.pump.GetStatus()

         # Read Pump Ctrl mode
         intpumpCMOD =  self.pump.GetCtrlMode()

         # Read Pump Command Frequency
         fltpumpCF = self.pump.GetCmdFreq()
         self.strPumpCF.set(str(fltpumpCF))

         # Read Pump Actual Frequency
         fltpumpAF = self.pump.GetActFreq()
         self.strPumpAF.set(str(fltpumpAF))

      self.maxDP = -999
      # Read SHT85 humidity sensors
      if haveSHTs > 0:
         trh_1 = self.sht85_1.single_shot()
         self.strSHT85_1.set("T=%.2f C, " % trh_1[0] +"RH=%.2f %%, " % trh_1[1] + "DP=%0.2f C" % trh_1[2])
         if trh_1[2] > self.maxDP:
            self.maxDP = trh_1[2]
      if haveSHTs > 1:
         trh_2 = self.sht85_2.single_shot()
         self.strSHT85_2.set("T=%.2f C, " % trh_2[0] +"RH=%.2f %%, " % trh_2[1] + "DP=%0.2f C" % trh_2[2])
         if trh_2[2] > self.maxDP:
            self.maxDP = trh_2[2]
      if haveSHTs > 2:
         trh_3 = self.sht85_3.single_shot()
         self.strSHT85_3.set("T=%.2f C, " % trh_3[0] +"RH=%.2f %%, " % trh_3[1] + "DP=%0.2f C" % trh_3[2])
         if trh_3[2] > self.maxDP:
            self.maxDP = trh_3[2]
      if haveSHTs > 4:
         trh_4 = self.sht85_4.single_shot()
         self.strSHT85_4.set("T=%.2f C, " % trh_4[0] +"RH=%.2f %%, " % trh_4[1] + "DP=%0.2f C" % trh_4[2])
         if trh_4[2] > self.maxDP:
            self.maxDP = trh_4[2]

      # Read HYT221 humidity sensors
      if haveHYTs > 0:
         trh_5 = self.hyt221_1.single_shot()
         self.strHYT221_1.set("T=%.2f C, " % trh_5[0] +"RH=%.2f %%, " % trh_5[1] + "DP=%0.2f C" % trh_5[2])
         if trh_5[2] > self.maxDP:
            self.maxDP = trh_5[2]
      if haveHYTs > 1:
         trh_6 = self.hyt221_2.single_shot()
         self.strHYT221_2.set("T=%.2f C, " % trh_6[0] +"RH=%.2f %%, " % trh_6[1] + "DP=%0.2f C" % trh_6[2])
         if trh_6[2] > self.maxDP:
            self.maxDP = trh_6[2]
      if haveHYTs > 2:
         trh_7 = self.hyt221_3.single_shot()
         self.strHYT221_3.set("T=%.2f C, " % trh_7[0] +"RH=%.2f %%, " % trh_7[1] + "DP=%0.2f C" % trh_7[2])
         if trh_7[2] > self.maxDP:
            self.maxDP = trh_7[2]
      if haveHYTs > 3:
         trh_8 = self.hyt221_4.single_shot()
         self.strHYT221_4.set("T=%.2f C, " % trh_8[0] +"RH=%.2f %%, " % trh_8[1] + "DP=%0.2f C" % trh_8[2])
         if trh_8[2] > self.maxDP:
            self.maxDP = trh_8[2]


      self.minT = 999
      if haveNTCs:
         # Read NTC thermistors
         # As supplied, ADCPi uses a divider comprising 10k and 6.8k
         # hence we multiply by 6.8 / 16.8 to recover the voltage across the 6k8 bias resistor
         adcV = np.zeros((8),dtype=float)
         adcR = np.zeros((8),dtype=float)
         adcT = np.zeros((8),dtype=float)
  
         # piADC
         # channels 1,2,3, 5,6,7 set up for 10k NTC biassed from 5V with 6k8 to ground
         # channels 4 and 8 set up with 10k / 6k8 potential divider
         for x in range(4):
            adcV[x] = self.adc.read_voltage(x+1)

            # Assuming the NTC is connected between 3V and the 6k8 bias resistor
            # here we calculate the NTC resistance
            adcR[x] =  (2.993 * 6780 / adcV[x]) - 6780

            # EoS NTC are Semitec 103KT1608-1P
            # B is 3435K
            if adcR[x] > 0:
               steinhart = math.log(adcR[x] / 10000.0) / 3435
               steinhart += 1.0 / (25.0 + 273.15)         # log(R/Ro) / beta + 1/To
               steinhart = (1.0 / steinhart) - 273.15   # Invert, convert to C
            else:
               steinhart = -1
            adcT[x] = steinhart
             
         self.strADC_0.set("%.2f C" % adcT[0])
         self.strADC_1.set("%.2f C" % adcT[1])
         self.strADC_2.set("%.2f C" % adcT[2])
         self.strADC_3.set("%.3f V" % adcV[3])

         # consider both stave NTCs in minT calculation
         if adcT[0] < self.minT:
            self.minT = adcT[0]
         if adcT[1] < self.minT:
            self.minT = adcT[1]

      # Read TC-08 (K type thermocouples etc)
      temp = (ctypes.c_float * 9)()
      overflow = ctypes.c_int16(0)
      units = tc08.USBTC08_UNITS["USBTC08_UNITS_CENTIGRADE"]
      self.status["get_single"] = tc08.usb_tc08_get_single(self.chandle,ctypes.byref(temp), ctypes.byref(overflow), units)
      assert_pico2000_ok(self.status["get_single"])

      # scale flow and pressure meters
      # Channel 1: Proteus 08004BN1 Flowmeter
      #   output is 0 to 5V (check configuration!)
      #   corresponding to 0 to 5.3 lpm
      #   resistive divider is 140 / 10140
      #   so 5.3 lpm -> 5V -> (5*140)/10140 V
      #   => 013.02519444754568121766960663913 mV/lpm

      # Channel 2: RS 797-5030 Industrial Pressure Sensor
      #   output is 0 to 5V
      #   corresponding to 0 to 25 bar
      #   resistive divider is 140 / 10140
      #   so 25 bar -> 5V -> (5*140)/10140 V
      #   => 2.7613412228796844181459566075 mV/bar

      # Channel 3: RS 797-4970 Industrial Pressure Sensor
      #   output is 0 to 5V
      #   corresponding to -1 to 9 bar
      #   resistive divider is 140 / 10140
      #   so 9 bar -> 5V -> (5*140)/10140 V
      #   => 2.5 * 2.7613412228796844181459566075 mV/bar

      temp[1] = temp[1] / 13.0251944475456812176696066391
      temp[2] = (temp[2] /  2.7613412228796844181459566075) - 0.015 
      temp[3] = (temp[3] /  (2.5* 2.7613412228796844181459566075) ) - 1.034


      # print data
      #print("Cold Junction ", temp[0])
      #print(" Channel 1 ", temp[1], "lpm")
      #print(" Channel 2 ", temp[2], "bar")
      #print(" Channel 3 ", temp[3], "C (K type, coolant from plant)")
      #print(" Channel 4 ", temp[4], "C (K type, coolant to plant)")
      #print(" Channel 5 ", temp[5], "C (K type, pump body)")
      #print(" Channel 6 ", temp[6], "C (K type, motor body)")
      #print(" Channel 6 ", temp[6], "C (T type, air temp)")
      #print(" Channel 7 ", temp[7], "C (K type, coolant to stave)")
      #print(" Channel 8 ", temp[8], "C (T type, coolant from stave)")

      self.strTC08_0.set("%.2f C" % temp[0])
      self.strTC08_1.set("%.2f lpm" % temp[1])
      self.strTC08_2.set("%.2f bar" % temp[2])
      self.strTC08_3.set("%.2f bar" % temp[3])
      # self.strTC08_3.set("%.2f C" % temp[3])
      self.strTC08_4.set("%.2f C" % temp[4])
      self.strTC08_5.set("%.2f C" % temp[5])
      self.strTC08_6.set("%.2f C" % temp[6])
      self.strTC08_7.set("%.2f C" % temp[7])
      self.strTC08_8.set("%.2f C" % temp[8])

      # consider both stave coolant sesnors in minT calculation
      if temp[7] < self.minT:
         self.minT = temp[7]
      if temp[8] < self.minT:
         self.minT = temp[8]

      self.margin = self.minT - self.maxDP
      self.strMARGIN.set("minT=%.2f C, " % self.minT +"max DP=%.2f C, " % self.maxDP + "diff=%.2f C" % self.margin)

      # write to influxDB
      if haveChiller:
         self.client.write_points([{"measurement":'Chiller.SP', "tags":{}, "time": start_time, "fields":{'value': self.fltchillSP}}])
         self.client.write_points([{"measurement":'Chiller.BT', "tags":{}, "time": start_time, "fields":{'value': fltchillBT}}])
         self.client.write_points([{"measurement":'Chiller.PS', "tags":{}, "time": start_time, "fields":{'value': intchillPS}}])
         self.client.write_points([{"measurement":'Chiller.BL', "tags":{}, "time": start_time, "fields":{'value': intchillBL}}])

      if haveSHTs > 0:
         self.client.write_points([{"measurement":'SHT85.1',    "tags":{}, "time": start_time, 
          "fields":{
              'temperature':trh_1[0],
              'humidity':trh_1[1],
              'dewpoint':trh_1[2],
           }}])
      if haveSHTs > 1:
         self.client.write_points([{"measurement":'SHT85.2',    "tags":{}, "time": start_time, 
          "fields":{
              'temperature':trh_2[0],
              'humidity':trh_2[1],
              'dewpoint':trh_2[2],
           }}])
      if haveSHTs > 2:
         self.client.write_points([{"measurement":'SHT85.3',    "tags":{}, "time": start_time, 
          "fields":{
              'temperature':trh_3[0],
              'humidity':trh_3[1],
              'dewpoint':trh_3[2],
           }}])
      if haveSHTs > 3:
         self.client.write_points([{"measurement":'SHT85.4',    "tags":{}, "time": start_time, 
          "fields":{
              'temperature':trh_4[0],
              'humidity':trh_4[1],
              'dewpoint':trh_4[2],
           }}])

      if haveHYTs > 0:
         self.client.write_points([{"measurement":'HYT221.1',    "tags":{}, "time": start_time, 
          "fields":{
              'temperature':trh_5[0],
              'humidity':trh_5[1],
              'dewpoint':trh_5[2],
           }}])
      if haveHYTs > 1:
         self.client.write_points([{"measurement":'HYT221.2',    "tags":{}, "time": start_time, 
          "fields":{
              'temperature':trh_6[0],
              'humidity':trh_6[1],
              'dewpoint':trh_6[2],
           }}])
      if haveHYTs > 2:
         self.client.write_points([{"measurement":'HYT221.3',    "tags":{}, "time": start_time, 
          "fields":{
              'temperature':trh_7[0],
              'humidity':trh_7[1],
              'dewpoint':trh_7[2],
           }}])
      if haveHYTs > 3:
         self.client.write_points([{"measurement":'HYT221.4',    "tags":{}, "time": start_time, 
          "fields":{
              'temperature':trh_8[0],
              'humidity':trh_8[1],
              'dewpoint':trh_8[2],
           }}])

      if haveTC08:
         self.client.write_points([{"measurement":'Ambient',    "tags":{}, "time": start_time, "fields":{'value': temp[0]}}])
         self.client.write_points([{"measurement":'Flow',       "tags":{}, "time": start_time, "fields":{'value': temp[1]}}])
         self.client.write_points([{"measurement":'Pressure',   "tags":{}, "time": start_time, "fields":{'value': temp[2]}}])
         self.client.write_points([{"measurement":'Input Pressure',   "tags":{}, "time": start_time, "fields":{'value': temp[3]}}])
         # self.client.write_points([{"measurement":'PumpIn_T',   "tags":{}, "time": start_time, "fields":{'value': temp[3]}}])
         self.client.write_points([{"measurement":'PumpOut_T',  "tags":{}, "time": start_time, "fields":{'value': temp[4]}}])
         self.client.write_points([{"measurement":'PumpBody_T', "tags":{}, "time": start_time, "fields":{'value': temp[5]}}])
         # self.client.write_points([{"measurement":'MotorBody_T',"tags":{}, "time": start_time, "fields":{'value': temp[6]}}])
         self.client.write_points([{"measurement":'AirIn_T',"tags":{}, "time": start_time, "fields":{'value': temp[6]}}])
         self.client.write_points([{"measurement":'StaveIn_T',  "tags":{}, "time": start_time, "fields":{'value': temp[7]}}])
         self.client.write_points([{"measurement":'StaveOut_T', "tags":{}, "time": start_time, "fields":{'value': temp[8]}}])

      if havePump:
         self.client.write_points([{"measurement":'Pump.CF',    "tags":{}, "time": start_time, "fields":{'value': fltpumpCF}}])
         self.client.write_points([{"measurement":'Pump.AF',    "tags":{}, "time": start_time, "fields":{'value': fltpumpAF}}])

      # Run this every 30 seconds
      stop_ms = (time.time_ns() / 1000000)
      wait_ms = 10000 - (stop_ms - start_ms)
      self.gui.after(int(wait_ms),self.cbMonitor)

   def __init__(self):

      # Initialise margin calculations
      self.margin = 0
      self.maxDP = 999
      self.minT = -999
      self.fltchillSP = 15

      # Connect to influxDB
      self.client = InfluxDBClient(host='127.0.0.1', port=8086, username='CPSadmin', password='ProFlow2020', database='CPS')
      self.client.create_database('CPS')
      self.client.create_database('Stave')

      # Connect to, and configure, TC-08
      # Create chandle and status ready for use
      self.chandle = ctypes.c_int16()
      self.status = {}

      # open unit
      self.status["open_unit"] = tc08.usb_tc08_open_unit()
      assert_pico2000_ok(self.status["open_unit"])
      self.chandle = self.status["open_unit"]

      # set mains rejection to 50 Hz
      self.status["set_mains"] = tc08.usb_tc08_set_mains(self.chandle,0)
      assert_pico2000_ok(self.status["set_mains"])

      # set up channel
      # therocouples types and int8 equivalent
      # B=66 , E=69 , J=74 , K=75 , N=78 , R=82 , S=83 , T=84 , ' '=32 , X=88 
      typeX = ctypes.c_int8(88)
      typeK = ctypes.c_int8(75)
      typeT = ctypes.c_int8(84)
      self.status["set_channel"] = tc08.usb_tc08_set_channel(self.chandle, 1, typeX)
      assert_pico2000_ok(self.status["set_channel"])
      self.status["set_channel"] = tc08.usb_tc08_set_channel(self.chandle, 2, typeX)
      assert_pico2000_ok(self.status["set_channel"])
      self.status["set_channel"] = tc08.usb_tc08_set_channel(self.chandle, 3, typeX)
      #self.status["set_channel"] = tc08.usb_tc08_set_channel(self.chandle, 3, typeK)
      assert_pico2000_ok(self.status["set_channel"])
      self.status["set_channel"] = tc08.usb_tc08_set_channel(self.chandle, 4, typeK)
      assert_pico2000_ok(self.status["set_channel"])
      self.status["set_channel"] = tc08.usb_tc08_set_channel(self.chandle, 5, typeK)
      assert_pico2000_ok(self.status["set_channel"])
      self.status["set_channel"] = tc08.usb_tc08_set_channel(self.chandle, 6, typeT)
      assert_pico2000_ok(self.status["set_channel"])
      self.status["set_channel"] = tc08.usb_tc08_set_channel(self.chandle, 7, typeK)
      assert_pico2000_ok(self.status["set_channel"])
      self.status["set_channel"] = tc08.usb_tc08_set_channel(self.chandle, 8, typeT)
      assert_pico2000_ok(self.status["set_channel"])

      intchillSTB = 0
      if haveChiller:
         # Initialise connection to LaudaPro chiller (TCPIP)
         self.chiller = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
         try:
           self.chiller.connect(('192.168.223.130', 54321))
         except:
           print('Unable to connect to chiller. Is the DHCP server running?')
           print('sudo service isc-dhcp-server start')
           raise OSError('No route to host') 

         # Read Chiller Standby Status
         # - returns 1 if chiller is in STANDBY eg OFF
         self.chiller.sendall(b'IN_MODE_02\r\n')
         intchillSTB = int(self.chiller.recv(32)) 
         print('Chiller Standby', intchillSTB)

      if havePump:
         # Initialise connection to Lenze SMV Motor Control (RS485)
         self.pump = lenzeSMV("/dev/ttyUSB0")
         self.pump.UnlockControl()
         self.pump.UnlockParameters()

      # Initialise connection to SHT85 humidity sensors (piBurn)
      if haveSHTs > 0:
         self.sht85_1 = sht85(0x73,1)
      if haveSHTs > 1:
         self.sht85_2 = sht85(0x73,2)
      if haveSHTs > 2:
         self.sht85_3 = sht85(0x73,3)
      if haveSHTs > 3:
         self.sht85_4 = sht85(0x73,0)

      # Initialise connection to HYT221 humidity sensors (piBurn)
      if haveHYTs > 0:
         self.hyt221_1 = hyt221(0x73,1)
      if haveHYTs > 1:
         self.hyt221_2 = hyt221(0x73,3)
      if haveHYTs > 2:
         self.hyt221_3 = hyt221(0x73,2)
      if haveHYTs > 3:
         self.hyt221_4 = hyt221(0x73,0)

      if haveNTCs:
         # Initialise connection to NTCs (ADCpi)
         self.adc = ADCPi(0x68, 0x69, 16)


      # Initialise GUI
      self.gui = tkinter.Tk()
      self.gui.title("Chiller Pump System")

      tkinter.Label(self.gui, text="Chiller Control").grid(row=0, column=0)

      tkinter.Label(self.gui, text="Set Temperature").grid(row=1, column=0)
      self.strChillSP = tkinter.StringVar(value="???")
      self.entChillSP = tkinter.Entry(self.gui, textvariable = self.strChillSP)
      self.entChillSP.grid(row=1, column=1)
      butChillSP = tkinter.Button(self.gui, text = "Set", command = self.cbChillSP)
      butChillSP.grid(row=1, column=2)

      tkinter.Label(self.gui, text="Pump Speed").grid(row=2, column=0)
      self.strChillPS = tkinter.StringVar(value="???")
      self.entChillPS = tkinter.Entry(self.gui, textvariable = self.strChillPS)
      self.entChillPS.grid(row=2, column=1)
      butChillPS = tkinter.Button(self.gui, text = "Set", command = self.cbChillPS)
      butChillPS.grid(row=2, column=2)

      tkinter.Label(self.gui, text="Bath Temp.").grid(row=3, column=0)
      self.strChillBT = tkinter.StringVar(value="???")
      labChillBT = tkinter.Label(self.gui, textvariable = self.strChillBT).grid(row=3, column=1)

      if intchillSTB > 0:
         self.butChillStart = tkinter.Button(self.gui, text = "Start", command = self.cbChillStart, state = "normal")
      else:
         self.butChillStart = tkinter.Button(self.gui, text = "Start", command = self.cbChillStart, state = "disabled")
      self.butChillStart.grid(row=3, column=2)

      tkinter.Label(self.gui, text="Bath Level").grid(row=4, column=0)
      self.strChillBL = tkinter.StringVar(value="???")
      labChillBL = tkinter.Label(self.gui, textvariable = self.strChillBL).grid(row=4, column=1)

      if intchillSTB > 0:
         self.butChillStop = tkinter.Button(self.gui, text = "Stop", command = self.cbChillStop, state = "disabled")
      else:
         self.butChillStop = tkinter.Button(self.gui, text = "Stop", command = self.cbChillStop, state = "normal")
      self.butChillStop.grid(row=4, column=2)

      tkinter.Label(self.gui, text="Pump Control").grid(row=6, column=0)

      tkinter.Label(self.gui, text="Set Frequency").grid(row=7, column=0)
      self.strPumpCF = tkinter.StringVar(value="???")
      self.entPumpCF = tkinter.Entry(self.gui, textvariable = self.strPumpCF)
      self.entPumpCF.grid(row=7, column=1)
      butPumpCF = tkinter.Button(self.gui, text = "Set", command = self.cbPumpCF)
      butPumpCF.grid(row=7, column=2)

      tkinter.Label(self.gui, text="Mon Frequency").grid(row=8, column=0)
      self.strPumpAF = tkinter.StringVar(value="???")
      labPumpMF = tkinter.Label(self.gui, textvariable = self.strPumpAF).grid(row=8, column=1)

      if intchillSTB > 0:
         self.butPumpStart = tkinter.Button(self.gui, text = "Start", command = self.cbPumpStart, state = "disabled")
      else:
         self.butPumpStart = tkinter.Button(self.gui, text = "Start", command = self.cbPumpStart, state = "normal")
      self.butPumpStart.grid(row=8, column=2)
      self.butPumpStop = tkinter.Button(self.gui, text = "Stop", command = self.cbPumpStop, state = "disabled")
      self.butPumpStop.grid(row=9, column=2)

      rn = int(10)

      if haveTC08:
         tkinter.Label(self.gui, text="TC-08").grid(row=rn, column=0)
         rn+=1

         self.strTC08_0 = tkinter.StringVar(value="???")
         tkinter.Label(self.gui, text="0").grid(row=rn, column=0)
         tkinter.Label(self.gui, text="Ambient").grid(row=rn, column=1)
         labTC08_0 = tkinter.Label(self.gui, textvariable = self.strTC08_0).grid(row=rn, column=2)
         rn+=1
 
         tkinter.Label(self.gui, text="1").grid(row=rn, column=0)
         tkinter.Label(self.gui, text="Flow").grid(row=rn, column=1)
         self.strTC08_1 = tkinter.StringVar(value="???")
         labTC08_1 = tkinter.Label(self.gui, textvariable = self.strTC08_1).grid(row=rn, column=2)
         rn+=1
 
         tkinter.Label(self.gui, text="2").grid(row=rn, column=0)
         tkinter.Label(self.gui, text="Output Pressure").grid(row=rn, column=1)
         self.strTC08_2 = tkinter.StringVar(value="???")
         labTC08_2 = tkinter.Label(self.gui, textvariable = self.strTC08_2).grid(row=rn, column=2)
         rn+=1
 
         # tkinter.Label(self.gui, text="3").grid(row=rn, column=0)
         # tkinter.Label(self.gui, text="Fluid from chiller").grid(row=rn, column=1)
         # self.strTC08_3 = tkinter.StringVar(value="???")
         # labTC08_3 = tkinter.Label(self.gui, textvariable = self.strTC08_3).grid(row=rn, column=2)
         # rn+=1
 
         tkinter.Label(self.gui, text="3").grid(row=rn, column=0)
         tkinter.Label(self.gui, text="Inlet Pressure").grid(row=rn, column=1)
         self.strTC08_3 = tkinter.StringVar(value="???")
         labTC08_3 = tkinter.Label(self.gui, textvariable = self.strTC08_3).grid(row=rn, column=2)
         rn+=1
 
         tkinter.Label(self.gui, text="4").grid(row=rn, column=0)
         tkinter.Label(self.gui, text="Fluid to chiller").grid(row=rn, column=1)
         self.strTC08_4 = tkinter.StringVar(value="???")
         labTC08_4 = tkinter.Label(self.gui, textvariable = self.strTC08_4).grid(row=rn, column=2)
         rn+=1
 
         tkinter.Label(self.gui, text="5").grid(row=rn, column=0)
         tkinter.Label(self.gui, text="Pump body").grid(row=rn, column=1)
         self.strTC08_5 = tkinter.StringVar(value="???")
         labTC08_5 = tkinter.Label(self.gui, textvariable = self.strTC08_5).grid(row=rn, column=2)
         rn+=1
 
         tkinter.Label(self.gui, text="6").grid(row=rn, column=0)
         tkinter.Label(self.gui, text="Motor body").grid(row=rn, column=1)
         self.strTC08_6 = tkinter.StringVar(value="???")
         labTC08_6 = tkinter.Label(self.gui, textvariable = self.strTC08_6).grid(row=rn, column=2)
         rn+=1
 
         tkinter.Label(self.gui, text="7").grid(row=rn, column=0)
         tkinter.Label(self.gui, text="Fluid to stave").grid(row=rn, column=1)
         self.strTC08_7 = tkinter.StringVar(value="???")
         labTC08_7 = tkinter.Label(self.gui, textvariable = self.strTC08_7).grid(row=rn, column=2)
         rn+=1
 
         tkinter.Label(self.gui, text="8").grid(row=rn, column=0)
         tkinter.Label(self.gui, text="Fluid from stave").grid(row=rn, column=1)
         self.strTC08_8 = tkinter.StringVar(value="???")
         labTC08_8 = tkinter.Label(self.gui, textvariable = self.strTC08_8).grid(row=rn, column=2)
         rn+=1


      if haveNTCs:
         tkinter.Label(self.gui, text="piADC").grid(row=rn, column=0)
         rn+=1
 
         tkinter.Label(self.gui, text="0").grid(row=rn, column=0)
         tkinter.Label(self.gui, text="Stave NTC0").grid(row=rn, column=1)
         self.strADC_0 = tkinter.StringVar(value="???")
         piADC_0 = tkinter.Label(self.gui, textvariable = self.strADC_0).grid(row=rn, column=2)
         rn+=1

         tkinter.Label(self.gui, text="1").grid(row=rn, column=0)
         tkinter.Label(self.gui, text="Stave NTC1").grid(row=rn, column=1)
         self.strADC_1 = tkinter.StringVar(value="???")
         piADC_1 = tkinter.Label(self.gui, textvariable = self.strADC_1).grid(row=rn, column=2)
         rn+=1

         tkinter.Label(self.gui, text="2").grid(row=rn, column=0)
         tkinter.Label(self.gui, text="Testbox Void").grid(row=rn, column=1)
         self.strADC_2 = tkinter.StringVar(value="???")
         piADC_2 = tkinter.Label(self.gui, textvariable = self.strADC_2).grid(row=rn, column=2)
         rn+=1

         tkinter.Label(self.gui, text="3").grid(row=rn, column=0)
         tkinter.Label(self.gui, text="Testbox Lid").grid(row=rn, column=1)
         self.strADC_3 = tkinter.StringVar(value="???")
         piADC_3 = tkinter.Label(self.gui, textvariable = self.strADC_3).grid(row=rn, column=2)
         rn+=1

      if haveSHTs > 0:
         tkinter.Label(self.gui, text="SHT85").grid(row=rn, column=0)
         rn+=1

         tkinter.Label(self.gui, text="Sensor 1").grid(row=rn, column=0)
         self.strSHT85_1 = tkinter.StringVar(value="???")
         labSHT85_1 = tkinter.Label(self.gui, textvariable = self.strSHT85_1).grid(row=rn, column=1)
         rn+=1

      if haveSHTs > 1:
         tkinter.Label(self.gui, text="Sensor 2").grid(row=rn, column=0)
         self.strSHT85_2 = tkinter.StringVar(value="???")
         labSHT85_2 = tkinter.Label(self.gui, textvariable = self.strSHT85_2).grid(row=rn, column=1)
         rn+=1

      if haveSHTs > 2:
         tkinter.Label(self.gui, text="Sensor 3").grid(row=rn, column=0)
         self.strSHT85_3 = tkinter.StringVar(value="???")
         labSHT85_3 = tkinter.Label(self.gui, textvariable = self.strSHT85_3).grid(row=rn, column=1)
         rn+=1

      if haveSHTs > 3:
         tkinter.Label(self.gui, text="Sensor 4").grid(row=rn, column=0)
         self.strSHT85_4 = tkinter.StringVar(value="???")
         labSHT85_4 = tkinter.Label(self.gui, textvariable = self.strSHT85_4).grid(row=rn, column=1)
         rn+=1

      if haveHYTs > 0:
         tkinter.Label(self.gui, text="HYT221").grid(row=rn, column=0)
         rn+=1

         tkinter.Label(self.gui, text="Sensor 1").grid(row=rn, column=0)
         self.strHYT221_1 = tkinter.StringVar(value="???")
         labHYT221_1 = tkinter.Label(self.gui, textvariable = self.strHYT221_1).grid(row=rn, column=1)
         rn+=1

      if haveHYTs > 1:
         tkinter.Label(self.gui, text="Sensor 2").grid(row=rn, column=0)
         self.strHYT221_2 = tkinter.StringVar(value="???")
         labHYT221_2 = tkinter.Label(self.gui, textvariable = self.strHYT221_2).grid(row=rn, column=1)
         rn+=1

      if haveHYTs > 2:
         tkinter.Label(self.gui, text="Sensor 3").grid(row=rn, column=0)
         self.strHYT221_3 = tkinter.StringVar(value="???")
         labHYT221_3 = tkinter.Label(self.gui, textvariable = self.strHYT221_3).grid(row=rn, column=1)
         rn+=1

      if haveHYTs > 3:
         tkinter.Label(self.gui, text="Sensor 4").grid(row=rn, column=0)
         self.strHYT221_4 = tkinter.StringVar(value="???")
         labHYT221_4 = tkinter.Label(self.gui, textvariable = self.strHYT221_4).grid(row=rn, column=1)
         rn+=1

      tkinter.Label(self.gui, text="Stave Summary").grid(row=rn, column=0)
      rn+=1

      tkinter.Label(self.gui, text="Margin").grid(row=rn, column=0)
      self.strMARGIN = tkinter.StringVar(value="???")
      labMARGIN = tkinter.Label(self.gui, textvariable = self.strMARGIN).grid(row=rn, column=1)
      rn+=1


      self.cbMonitor()
      self.gui.mainloop()

   def exit(self):
      # close unit
      self.status["close_unit"] = tc08.usb_tc08_close_unit(self.chandle)
      assert_pico2000_ok(self.status["close_unit"])

      self.chiller.close()



cps = clsCPS()
