from lenzeSMV import lenzeSMV
import serial

pump = lenzeSMV("/dev/ttyUSB0", intBaud=9600, bytesize=serial.EIGHTBITS, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_TWO, timeout=15)
#pump.Start()
pump.UnlockControl()
pump.UnlockParameters()
intpumpStat =  pump.GetStatus()
pump.Start()
ntpumpStat =  pump.GetStatus()
