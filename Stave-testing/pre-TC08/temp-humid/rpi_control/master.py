import time
from sht85 import sht85
import serial
from serial import Serial
import os
with open("startup.txt","a") as fl:
    fl.write("started reading sensors\n")


def setPerm():
    sudoPass='bnlphysics'
    command = 'chmod 777 /dev/serial0'
    os.system('echo %s | sudo -S %s' %(sudoPass,command))

setPerm()
uart=Serial("/dev/ttyS0",115200,timeout=5)
uart.flush()
Star11=sht85(1) #black
Star12=sht85(3) #blue
Star13=sht85(4) #green
Star14=sht85(5) #red

while True:
    try:
        Star11_data=Star11.get_data()
        print("ABCStar 11 data"+str(Star11_data))
        uart.write("ABCStar11,"+str(Star11_data['temperature'])+","+str(Star11_data['humidity'])+"\n")
        time.sleep(0.5)
    except:
        uart.write("ABCStar11 not working\n")

    try:
        Star12_data=Star12.get_data()
        print("ABCStar 12 data"+str(Star12_data))
        uart.write("ABCStar12," +str(Star12_data['temperature'])+","+str(Star12_data['humidity'])+"\n")
        time.sleep(0.5)
    except:
        uart.write("ABCStar12 not working\n")
        

    try:
        Star13_data=Star13.get_data()
        print("ABCStar 13 data"+str(Star13_data))
        uart.write("ABCStar13," +str(Star13_data['temperature'])+","+str(Star13_data['humidity'])+"\n")
        time.sleep(0.5)
    except:
        uart.write("ABCStar13 not working\n")
        
    try:
        Star14_data=Star14.get_data()
        print("ABCStar 14 data"+str(Star14_data))
        uart.write("ABCStar14," +str(Star14_data['temperature'])+","+str(Star14_data['humidity'])+"\n")
        time.sleep(0.5)
    except:
        uart.write("ABCStar14 not working\n")
        

    time.sleep(10)
    print("-----------------------------------------")

