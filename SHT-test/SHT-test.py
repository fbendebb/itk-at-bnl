import smbus
from smbus2 import SMBus
import time
import qwiic
import numpy as np
from matplotlib import pyplot as plt

test = qwiic.QwiicTCA9548A()
test.enable_channels([5])
test.disable_channels([6])
test.list_channels()

def dew_point(temperature, humidity):
    A = 17.27
    B = 237.7
    
    #protection against div by 0
    if humidity == 0:
        return -999
    alpha = ((A * temperature) / (B + temperature)) + np.log(humidity / 100.0)
    dewpoint = (B * alpha) / (A - alpha)
    return dewpoint


def dewPoint(temp, rh):
        #Values and formulas from datasheet
        if temp > 0:
            m = 17.62
            Tn = 243.12
        else:
            m = 22.46
            Tn = 272.62
        
        #protection against div by 0
        if rh == 0:
            rh = -999
            
        return Tn * (np.log(rh/100) + (m * temp)/(Tn + temp))/(m - np.log(rh/100) - (m * temp)/(Tn * temp)) 


class sht85:
    def __init__(self,bus=1,address=0x44):
        self.bus=smbus.SMBus(bus)
        self.address=address
    def get_data(self):
        #Write the read sensor command
        self.bus.write_byte_data(self.address, 0x24, 0x00)
        time.sleep(0.5) #This is so the sensor has tme to preform the mesurement and write its registers before you read it\
        
        # Read data back, 8 bytes, temperature MSB first then lsb, Then skip the checksum bit then humidity MSB the lsb.
        data0 = self.bus.read_i2c_block_data(self.address, 0x00, 8)
        t_val = (data0[0]<<8) + data0[1] #convert the data
        h_val = (data0[3] <<8) + data0[4]     # Convert the data
        T = ((175.72 * t_val) / 65536.0 ) - 45 #do the maths from datasheet
        H = ((100 * h_val) / 65536.0 )
        return {"Temperature": T,"Humidity": H, "Dewpoint": dew_point(T, H), "Dewpoint2": dewPoint(T, H)}
        #self.Upload_To_Influx_hum(current_time, ifuser = "abe", ifpass = "abe_influxdb")
    
myself = sht85()

# output data (eventually will be uploaded to influxDB, but save to output file for capability to plot without that)

outFile = "data.txt"
NdataPoints = 10

# Prepare output data file
with open(outFile, 'w') as f:

    # Write output file header
    f.write("SHT1_temperature \t SHT2_temperature \t SHT1_humidity \t SHT2_humidity \n")

    # Measure each SHT
    for i in range(0, NdataPoints):
        print(f"On data point {i} / {NdataPoints}")

        test.enable_channels([0])
        test.disable_channels([4])

        SHT1_data = myself.get_data()

        print("\n\n\nSensor 1: ")
        print(SHT1_data)

        test.enable_channels([1])
        test.disable_channels([0])

        SHT2_data = myself.get_data()

        print("\nSensor 2: ")
        print(SHT2_data)
        
        #test.enable_channels([2])
        #test.disable_channels([1])
        
        #print("\nSensor 3: ")
        #print(myself.get_data())
        
        #test.enable_channels([3])
        #test.disable_channels([2])
        
        #print("\nSensor 4: ")
        #print(myself.get_data())
        
        #test.enable_channels([4])
        #test.disable_channels([3])
        
        #print("\nSensor 5: ")
        #print(myself.get_data())
        
        SHT1_temperature, SHT1_humidity = SHT1_data["Temperature"], SHT1_data["Humidity"]
        SHT2_temperature, SHT2_humidity = SHT2_data["Temperature"], SHT2_data["Humidity"]

        f.write(f"{SHT1_temperature}\t{SHT2_temperature}\t{SHT1_humidity}\t{SHT2_humidity}\n") # save line to output file

        time.sleep(1)
    
f.close()

# plot the data
with open(outFile) as f:
    lines = f.readlines()
    
    SHT1_temp = [line.split()[0] for line in lines] # SHT1_temp 
    SHT2_temp = [line.split()[1] for line in lines] # SHT2_temp
    
    # remove column labels 
    SHT1_temp.pop(0) 
    SHT2_temp.pop(0) 

    # convert from string to float
    SHT1_temp = [float(i) for i in SHT1_temp]
    SHT2_temp = [float(i) for i in SHT2_temp]

print("All SHT1 temperatures:",SHT1_temp)
print("All SHT2 temperatures:",SHT2_temp)

fig, ax = plt.subplots()
plt.plot(SHT1_temp)
plt.savefig("SHT1_temp.png")
plt.close()

print(f"Output data saved to: {outFile}")
print(f"Output plot saved to SHT1_temp.png")

