#!/usr/bin/env python

# From: https://simonhearne.com/2020/pi-metrics-influx/

import datetime
import random 
from influxdb import InfluxDBClient

# influx configuration - edit these
ifuser = "Abe"
ifpass = "<password_here>"
ifdb   = "home"
ifhost = "127.0.0.1"
ifport = 8086
measurement_name = "RandomNumbers"

# take a timestamp for this measurement
time = datetime.datetime.utcnow()

num = random.random()

# format the data as a single measurement for influx
body = [
    {
        "measurement": measurement_name,
        "time": time,
        "fields": {
            "Random_Number" : num
        }
    }
]

# connect to influx
ifclient = InfluxDBClient(ifhost,ifport,ifuser,ifpass,ifdb)

# write the measurement
ifclient.write_points(body)
