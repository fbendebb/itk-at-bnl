#!/usr/bin/ python3
# From: https://simonhearne.com/2020/pi-metrics-influx/

import datetime
import random 
from influxdb import InfluxDBClient

# influx configuration - edit these
ifuser = "abe"
ifpass = "abe_influxdb"
ifdb   = "rpi_I61"
ifhost = "127.0.0.1"
ifport = 8084
measurement_name = "RandomNumbers"

# take a timestamp for this measurement
time = datetime.datetime.utcnow()

num = random.random()

# format the data as a single measurement for influx
body = [
    {
        "measurement": measurement_name,
        "time": time,
        "fields": {
            "Random_Number" : num
        }
    }
]

print("Connecting to influx now")

# connect to influx
ifclient = InfluxDBClient(ifhost,ifport,ifuser,ifpass,ifdb)

# write the measurement
ifclient.write_points(body)
