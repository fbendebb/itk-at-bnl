"""
Abraham Tishelman-Charny
24 July 2023

The purpose of this script is to set up git branches properly to work in an operational mode, i.e. one that should work for thermal cycling, rather than a development mode.

Usage: python3 SetupOperationsMode.py
"""

import os
import subprocess

commands = [
    ["git", "checkout", "Options_Shunted_Cycling_Custom"],
]

cwd = "/home/qcbox2/Desktop/itsdaq-sw"
for c in commands:
    command = " ".join(c)
    print("$", command)
    if(c[0] == "cd"):
        cwd=c[1]
    else:
        #subprocess.run(f"bash -i -c '{command}'", shell=True)
        subprocess.run(c, cwd=cwd)

