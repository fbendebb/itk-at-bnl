/*
26 July 2023
Abraham Tishelman-Charny

This function notifies people via email if a step is ready for them to perform. This is triggered every time a cell is edited in the spreadsheet.
When the cell is edited, check:
  - If a hybrid is ready for wirebonding 
  - If a module is ready for wirebonding
  - If a module is ready for testing
*/

function sendMailEdit(e){
 
  // Get the edited row and column
  let editedRow = e.range.getRow();
  let editedColumn = e.range.getColumn();

  // Initialize variables
  let columnToCheck = -1; // Define column to check for changes
  let desiredValue = ""; // Define the desired value to trigger the email

  // Spreadsheet info, info of row that was edited
  let sheet = e.source.getActiveSheet();
  let rowData = sheet.getRange(editedRow, 1, 1, sheet.getLastColumn()).getValues()[0];
  let columnData = sheet.getRange(1, editedColumn, sheet.getLastRow(), 1).getValues();
  columnData = columnData.flat(); // Convert 2D array to 1D array
  let sheetName = sheet.getName();

  // Create the link to the edited cell
  let spreadsheetUrl = e.source.getUrl();
  let editedCellLink = spreadsheetUrl + "#gid=" + sheet.getSheetId() + "&range=" + sheet.getRange(editedRow, editedColumn).getA1Notation();

  let columnName = "";

  // Edit was made in Module Assembly sheet. 
  if(sheetName === "Module Assembly"){
    
    // First check if the "Module Wirebonded" column was marked as "To Do Next", AND that the metrology has already been marked as done.
    columnToCheck = 15; // Module Wirebonded - double check name
    desiredValue = "To Do Next";
    columnName = columnData[2];
  
    if(columnName === "Module Wirebonded" && editedColumn === columnToCheck && e.value === desiredValue){ 
      const metrology_status = rowData[13]; // make sure metrology is marked as done
      if(metrology_status === "Done"){
        const email = "cmusso@bnl.gov,wfielitz@bnl.gov,abraham.tishelman.charny@cern.ch";
        const module_local_ID = rowData[4];
        const module_serial_number = rowData[5];

        // Construct the email message
        const subject = "[ITk at BNL] - " + module_local_ID + " (" + module_serial_number + ") is ready for wirebonding";
        const message = "Dear Chris and Will,<br><br>" + 
                        module_local_ID + " (" + module_serial_number + ") is now ready for wirebonding. Please wirebond at your earliest convenience." + 
                        " When you're finished, remember to update the spreadsheet and upload any required files to the database. <br><br>Thank you,<br>BNL_ModuleStatus spreadsheet <br><br>--------------------------<br><br>This is an automatic email. Please do not reply to it - if you do, your message will not be seen." + 
                        '<br><br>This email was trigged by the following cell being edited: <a href="'+editedCellLink+'">Link_To_Spreadsheet</a>';

        // Send the email
        MailApp.sendEmail(email, subject, "", {htmlBody: message});
        return 1;
      }
    }

    // Next, check if the "Module Metrology" column was just marked as "Done", and the Module Wirebonded column was already marked as To Do Next
    columnToCheck = 14; // Module Metrology - double check name (safeguard)
    desiredValue = "Done";
    columnName = columnData[2];

    if(columnName === "Module Metrology" && editedColumn === columnToCheck && e.value === desiredValue){ 
      const wirebonded_status = rowData[14]; // make sure Module Wirebonded was already marked as To Do Next (otherwise might get two emails if it's about to be marked as To Do Next)
      if(wirebonded_status === "To Do Next"){
        const email = "cmusso@bnl.gov,wfielitz@bnl.gov,abraham.tishelman.charny@cern.ch";
        const module_local_ID = rowData[4];
        const module_serial_number = rowData[5];

        // Construct the email message
        const subject = "[ITk at BNL] - " + module_local_ID + " (" + module_serial_number + ") is ready for wirebonding";
        const message = "Dear Chris and Will,<br><br>" + 
                        module_local_ID + " (" + module_serial_number + ") is now ready for wirebonding. Please wirebond at your earliest convenience." + 
                        " When you're finished, remember to update the spreadsheet and upload any required files to the database. <br><br>Thank you,<br>BNL_ModuleStatus spreadsheet <br><br>--------------------------<br><br>This is an automatic email. Please do not reply to it - if you do, your message will not be seen." + 
                        '<br><br>This email was trigged by the following cell being edited: <a href="'+editedCellLink+'">Link_To_Spreadsheet</a>';

        // Send the email
        MailApp.sendEmail(email, subject, "", {htmlBody: message});
        return 1;
      }
    }

    // Next, check if the "High res photo taken?" column was just marked as "To Do Next", and the Module Wirebonded column is already marked as Done
    columnToCheck = 16; // High res photo taken? - double check name (safeguard)
    desiredValue = "To Do Next";
    columnName = columnData[2];

    if(columnName === "High res photo taken?" && editedColumn === columnToCheck && e.value === desiredValue){ 
      const wirebonded_status = rowData[14]; // make sure Module Wirebonded was already marked as Done (otherwise might get two emails if it's about to be marked as Done)
      if(wirebonded_status === "Done"){
        const email = "emily.rose.duden@cern.ch,abraham.tishelman.charny@cern.ch";
        const module_local_ID = rowData[4];
        const module_serial_number = rowData[5];

        // Construct the email message 
        const subject = "[ITk at BNL] - " + module_local_ID + " (" + module_serial_number + ") is ready for testing";
        const message = "<p style='word-wrap: break-word;'></p>" +
                        "Dear Emily and Abe,<br><br>" + 
                        module_local_ID + " (" + module_serial_number + ") is now ready for testing. Please test at your earliest convenience.<br><br>" + 
                        "Remember to perform the following steps:<br><br>" + 
                        "- Take a high resolution photo, upload somewhere, mark this column as Done in the spreadsheet<br>"+
                        "- Take an AMAC IV at room temperature, upload to the database, mark this column appropriately in the spreadsheet\<br>"+
                        "- Take a room temperature electrical test, upload to the database, fille in the HCCStar fuseID, mark this column appropriately in the spreadsheet<br>"+
                        "- Take an AMAC IV at cold temperature, (no need to upload this to the database), mark this column appropriately in the spreadsheet<br>"+
                        "- Thermal cycle this module at your earliest convenience and at an opportune time. Feel free to wait for a batch of four modules to thermal cycle at once<br><br>"+
                        "If any of these steps results in 'Problems', leave a meaningful and detailed description in the Comments column for this module.<br><br>"+
                        "Thank you,<br>BNL_ModuleStatus spreadsheet <br><br>--------------------------<br><br>This is an automatic email. Please do not reply to it - if you do, your reply will not be seen." + 
                        '<br><br>This email was trigged by the following cell being edited: <a href="'+editedCellLink+'">Link_To_Spreadsheet</a>';

        // Send the email
        MailApp.sendEmail(email, subject, "", {htmlBody: message});
        return 1;
      }
    }

    // Next, check if the "Module Wirebonded" column was just marked as "Done", and the "High res photo taken?" column is already marked as "To Do Next"
    columnToCheck = 15; // Module Wirebonded - double check name
    desiredValue = "Done";
    columnName = columnData[2];

    if(columnName === "Module Wirebonded" && editedColumn === columnToCheck && e.value === desiredValue){ 
      const photo_status = rowData[15]; // make sure "High res photo taken?" was already marked as To Do Next (otherwise might get two emails if it's about to be marked as To Do Next)
      if(photo_status === "To Do Next"){
        const email = "emily.rose.duden@cern.ch,abraham.tishelman.charny@cern.ch";
        const module_local_ID = rowData[4];
        const module_serial_number = rowData[5];

        // Construct the email message 
        const subject = "[ITk at BNL] - " + module_local_ID + " (" + module_serial_number + ") is ready for testing";
        const message = "<p style='word-wrap: break-word;'></p>" +
                        "Dear Emily and Abe,<br><br>" + 
                        module_local_ID + " (" + module_serial_number + ") is now ready for testing. Please test at your earliest convenience.<br><br>" + 
                        "Remember to perform the following steps:<br><br>" + 
                        "- Take a high resolution photo, upload somewhere, mark this column as Done in the spreadsheet<br>"+
                        "- Take an AMAC IV at room temperature, upload to the database, mark this column appropriately in the spreadsheet\<br>"+
                        "- Take a room temperature electrical test, upload to the database, fill in the HCCStar fuseID, mark this column appropriately in the spreadsheet<br>"+
                        "- Take an AMAC IV at cold temperature, (no need to upload this to the database), mark this column appropriately in the spreadsheet<br>"+
                        "- Thermal cycle this module at your earliest convenience and at an opportune time. Feel free to wait for a batch of four modules to thermal cycle at once<br><br>"+
                        "If any of these steps results in 'Problems', leave a meaningful and detailed description in the Comments column for this module.<br><br>"+
                        "Thank you,<br>BNL_ModuleStatus spreadsheet <br><br>--------------------------<br><br>This is an automatic email. Please do not reply to it - if you do, your reply will not be seen." + 
                        '<br><br>This email was trigged by the following cell being edited: <a href="'+editedCellLink+'">Link_To_Spreadsheet</a>';

        // Send the email
        MailApp.sendEmail(email, subject, "", {htmlBody: message});
        return 1;
      }
    }
  }

  // Edit was made in Hybrid Assembly sheet. 
  if(sheetName === "Hybrid Assembly"){

    // First check if the "Wirebonded" column was marked as "To Do Next", AND that "On Test Panel" has already been marked as done.
    columnToCheck = 14; // (Hybrid) Wirebonded - double check name
    desiredValue = "To Do Next";
    columnName = columnData[1];
  
    if(columnName === "Wirebonded" && editedColumn === columnToCheck && e.value === desiredValue){ 
      const onpanel_status = rowData[10]; // make sure "On Test Panel" is marked as done
      if(onpanel_status === "Done"){
        const email = "cmusso@bnl.gov,wfielitz@bnl.gov,abraham.tishelman.charny@cern.ch";
        const hybrid_substrate_number = rowData[4];
        const hybrid_serial_number = rowData[5];

        // Construct the email message
        const subject = "[ITk at BNL] - Hybrid " + hybrid_substrate_number + " (" + hybrid_serial_number + ") is ready for wirebonding";
        const message = "Dear Chris and Will,<br><br>" + 
                        "Hybrid " + hybrid_substrate_number + " (" + hybrid_serial_number + ") is now ready for wirebonding. Please wirebond at your earliest convenience." + 
                        " When you're finished, remember to update the spreadsheet and upload any required files to the database. <br><br>Thank you,<br>BNL_ModuleStatus spreadsheet <br><br>--------------------------<br><br>This is an automatic email. Please do not reply to it - if you do, your message will not be seen." + 
                        '<br><br>This email was trigged by the following cell being edited: <a href="'+editedCellLink+'">Link_To_Spreadsheet</a>';

        // Send the email
        MailApp.sendEmail(email, subject, "", {htmlBody: message});
        return 1;
      }
    }

    // Then check if the "On Test Panel" column was marked as "Done", AND that "Wirebonded" column has already been marked as To Do Next.
    columnToCheck = 11; // On Test Panel - double check name
    desiredValue = "Done";
    columnName = columnData[1];
  
    if(columnName === "On Test Panel" && editedColumn === columnToCheck && e.value === desiredValue){ 
      const wirebonded_status = rowData[13]; // make sure "Wirebonded" is marked as To Do Next
      if(wirebonded_status === "To Do Next"){
        const email = "cmusso@bnl.gov,wfielitz@bnl.gov,abraham.tishelman.charny@cern.ch";
        const hybrid_substrate_number = rowData[4];
        const hybrid_serial_number = rowData[5];

        // Construct the email message
        const subject = "[ITk at BNL] - Hybrid " + hybrid_substrate_number + " (" + hybrid_serial_number + ") is ready for wirebonding";
        const message = "Dear Chris and Will,<br><br>" + 
                        "Hybrid " + hybrid_substrate_number + " (" + hybrid_serial_number + ") is now ready for wirebonding. Please wirebond at your earliest convenience." + 
                        " When you're finished, remember to update the spreadsheet and upload any required files to the database. <br><br>Thank you,<br>BNL_ModuleStatus spreadsheet <br><br>--------------------------<br><br>This is an automatic email. Please do not reply to it - if you do, your message will not be seen." + 
                        '<br><br>This email was trigged by the following cell being edited: <a href="'+editedCellLink+'">Link_To_Spreadsheet</a>';

        // Send the email
        MailApp.sendEmail(email, subject, "", {htmlBody: message});
        return 1;
      }
    }
  }

  // Edit was made in Module Reception sheet. 
  if(sheetName === "Module Reception"){

    // Check if the "On Test Panel" column was marked as "Done", AND that "Wirebonded" column has already been marked as To Do Next.
    columnToCheck = 9; // Visual Inspected - double check name
    desiredValue = "To Do Next";
    columnName = columnData[2]; // row 3 for this sheet.
  
    if(columnName === "Visual Inspected" && editedColumn === columnToCheck && e.value === desiredValue){ 
        const email = "emily.rose.duden@cern.ch,abraham.tishelman.charny@cern.ch";
        const module_local_ID = rowData[4];
        const module_serial_number = rowData[5];

        // Construct the email message
        const subject = "[ITk at BNL] - Module " + module_local_ID + " (" + module_serial_number + ") is ready for testing";
        const message = "Dear Emily and Abe,<br><br>" + 
                        module_local_ID + " (" + module_serial_number + ") is now ready for testing. Please test at your earliest convenience.<br><br>" + 
                        "Remember to perform the following steps as part of the reception test:<br><br>" + 
                        "- Take a high resolution photo, upload somewhere, mark this column as Done in the spreadsheet<br>"+
                        "- Take an AMAC IV at room temperature, upload to the DB, mark this column appropriately in the spreadsheet<br>"+
                        "- Take a room temperature electrical test, upload to the DB, fill in the HCCStar fuseID, mark this column appropriately in the spreadsheet<br>"+
                        "- If all tests pass, mark this module as Ready in the Status column<br><br>"+
                        "If any of these steps results in 'Problems', leave a meaningful and detailed description in the Comments column for this module.<br><br>"+
                        "Thank you,<br>BNL_ModuleStatus spreadsheet <br><br>--------------------------<br><br>This is an automatic email. Please do not reply to it - if you do, your reply will not be seen." + 
                        '<br><br>This email was trigged by the following cell being edited: <a href="'+editedCellLink+'">Link_To_Spreadsheet</a>';

        // Send the email
        MailApp.sendEmail(email, subject, "", {htmlBody: message});
        return 1;
    }
  }

}
