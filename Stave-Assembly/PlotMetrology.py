"""
14 August 2023
Abraham Tishelman-Charny

The purpose of this module is to convert take a smartscope output txt file as input, and output:

CSV file with x,y,z values 
3D plot of z values as a function of x and y

Usage: 
Adjust "files" list as necessary
python3 PlotMetrology.py
"""

# imports
import mplhep as hep 
import matplotlib.pyplot as plt
plt.style.use(hep.style.ATLAS) # customization: https://mplhep.readthedocs.io/en/latest/api.html
import pandas as pd
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.cm import ScalarMappable
import numpy as np
from matplotlib.tri import Triangulation
import re
from IPython.display import display
import ipywidgets as widgets
from mpl_toolkits.mplot3d import Axes3D

# save CSV file
def SaveCSV(outFile, output_data):
    with open(outFile, 'w') as file:
        file.write("X,Y,Z\n")
        for data in output_data:
            file.write("%s\n"%(data))

# make 3D plot
def Make3DPlot(data, Label, Z_label = "Z [mm]"):
    print("Making 3D plot")

    # Extract X, Y, and Z values
    X = np.array(data['X'])
    Y = np.array(data['Y'])
    Z = np.array(data['Z'])

    # Create a triangulation of the X, Y points
    triang = Triangulation(X, Y)

    # Create a 3D plot
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    # Plot the 2D heatmap on the non-uniform grid
    heatmap = ax.plot_trisurf(triang, Z, cmap='viridis', edgecolor='none', linewidth=0.2) #, vmax = 10)

    # Add colorbar
    cbar = fig.colorbar(heatmap, ax=ax, label=Z_label, extend = 'both') #vmin = -1, vmax = 10) #, norm = 'log')

    hep.atlas.text("ITk Strips Internal") 

    # Remove z-axis tick labels
    ax.set_zticklabels([])

    title = Label.replace(".txt", "")

    ax.set_xlabel('X [mm]')
    ax.set_ylabel('Y [mm]')
    ax.set_title(title)

    if(".txt" not in Label): 
        outPlotName = "%s.pdf"%(Label)
        print("outPlotName:",outPlotName)
    else: outPlotName = Label.replace(".txt", ".pdf")

    outPlotName = outPlotName.replace("csv/", "plots/")
    outPlotName = "plots/%s"%(outPlotName)

    print("Saving figure at:",outPlotName)
    plt.savefig(outPlotName)
    outPlotName = outPlotName.replace(".pdf", ".png")
    print("Saving figure at:",outPlotName)
    plt.savefig(outPlotName)

    # Create interactive widgets for azimuth and elevation angles
    azim_slider = widgets.FloatSlider(value=45, min=0, max=360, step=1, description='Azimuth:')
    elev_slider = widgets.FloatSlider(value=30, min=-90, max=90, step=1, description='Elevation:')

    # Create a layout for the widgets
    widgets_layout = widgets.Layout(width='50%')

    # Create a widget box
    widgets_box = widgets.VBox([azim_slider, elev_slider], layout=widgets_layout)

    # Display the widget box and the plot together
    plt.show() # if you want to be able to 3D rotate each plot

    plt.close()

def MakeCSVAndPlot(inFile, file_format):
    inFileLabel = inFile.replace("txt/","")
    outFile = inFile.replace(".txt", ".csv")
    outFile = outFile.replace("txt/", "csv/")

    # Read the text file
    with open(inFile, 'r') as file:
        content = file.read()

    # Split the content into blocks using the "Step Name" separator
    blocks = re.split(r'Step Name:', content)[1:]

    # Initialize a list to store the extracted values
    output_data = []

    # save two output csvs in this case
    if(file_format == "ls_ls_sensor"):
        output_data_LS_sensor_N = []
        output_data_LS_N = []

    # Iterate through each block
    for i, block in enumerate(blocks):
        lines = block.split("\n")
        Nlines = len(lines)

        if(Nlines == 11):
            x_line, y_line, z_line = lines[4], lines[5], lines[6]
            x_val = x_line.split()[3]
            y_val = y_line.split()[3]
            z_val = z_line.split()[3]

            label = x_line.split()[1]

            xyz_vals = "%s,%s,%s"%(x_val, y_val, z_val)

            if(file_format == "sensor_N"):
                output_data.append(xyz_vals)
            elif(file_format == "ls_ls_sensor"):
                # see if label has LS_sensor_ or Ls_
                if("Ls_Sensor_" in label):
                    output_data_LS_sensor_N.append(xyz_vals)
                elif("Ls_" in label):
                    output_data_LS_N.append(xyz_vals)

    # save to CSV file
    if(file_format == "sensor_N"): 
        SaveCSV(outFile, output_data)
        data = pd.read_csv(outFile)
        Make3DPlot(data, inFileLabel)

    elif(file_format == "ls_ls_sensor"):
        outFile = outFile.replace(".csv", "_LS_sensor_N.csv")
        inFileLabel = inFileLabel.replace("plots/", "")
        inFileLabel = inFileLabel.replace(".txt", "_LS_sensor_N.txt")

        SaveCSV(outFile, output_data_LS_sensor_N)
        data = pd.read_csv(outFile)
        Make3DPlot(outFile, inFileLabel)

        outFile = outFile.replace("_LS_sensor_N", "_LS_N")
        inFileLabel = inFileLabel.replace("_LS_sensor_N.txt", "_LS_N.txt")
        inFileLabel = inFileLabel.replace("plots/", "")
        SaveCSV(outFile, output_data_LS_N)
        data = pd.read_csv(outFile)
        Make3DPlot(data, inFileLabel)

    # if sensor_N format, also make 2D grid of values
    if(file_format == "sensor_N"):

        # Initialize an empty 11x11 grid
        grid_size = 11
        grid = [[None] * grid_size for _ in range(0,grid_size)]

        # Convert output_data to a list of tuples (X, Y, Z)
        data_tuples = [tuple(map(float, item.split(','))) for item in output_data]

        # Populate the grid with Z values based on X and Y ranges
        x_range = np.linspace(0, 100, grid_size)
        y_range = np.linspace(-100, 0, grid_size)

        for i, x_val in enumerate(x_range):
            for j, y_val in enumerate(y_range):
                for x, y, z in data_tuples:
                    if x_val == round(x/10)*10 and y_val == round(y/10)*10:
                        grid[j][i] = z

        # save to CSV file
        outFileGrid = outFile.replace(".csv", "_2Dgrid.csv")
        with open(outFileGrid, 'w') as grid_file:
            for row in grid:
                grid_file.write("%s\n"%(','.join(map(str, row))))

if(__name__ == "__main__"):
    # File, format
    files = [
        ["txt/BNL-PPB2-MLS-229_BackAfterTC.txt", "sensor_N"],
        ["txt/BNL-PPB2-MLS-229_BackAfterTCWithoutVacuum.txt", "sensor_N"],
        ["txt/BNL-PPB2-MLS-229.txt", "ls_ls_sensor"],
        ["txt/BNL-PPB2-MLS-229_FrontBowAfterTC.txt", "ls_ls_sensor"],
        ["txt/BNL-PPB2-MLS-241_BackAfterTCWithoutVacuum.txt", "sensor_N"],
        ["txt/BNL-PPB2-MLS-241_BackAfterTC.txt", "sensor_N"],
        ["txt/BNL-PPB2-MLS-241.txt", "ls_ls_sensor"],
        ["txt/BNL-PPB2-MLS-241-FrontBowAfterTC.txt", "ls_ls_sensor"],
    ]

    for file_info in files:
        file = file_info[0]
        file_format = file_info[1]
        print("On file:",file)
        MakeCSVAndPlot(file, file_format)