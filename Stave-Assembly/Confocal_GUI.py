#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 27 13:04:52 2023

@author: mayamancini
"""
import matplotlib.pyplot as plt
from scipy.interpolate import CubicSpline
import numpy as np
import csv
import os
from PyQt5.QtWidgets import QMainWindow, QApplication, QPushButton, QLabel, QCheckBox, QComboBox, QFileDialog
from PyQt5 import uic
import sys

#creating/Running GUI 

class UI(QMainWindow):
    def __init__(self):
        super(UI, self).__init__()
        
        
        #Load ui file
        uic.loadUi("Confocal_GUI.ui", self)
        
        #define Widgets
        self.button = self.findChild(QPushButton, "pushButton")
        self.label = self.findChild(QLabel, "label")
        
        self.button2 = self.findChild(QPushButton, "pushButton_2")
        self.label2 = self.findChild(QLabel, "label_2")
        
        self.comboBox = self.findChild(QComboBox, "comboBox")
        self.comboBox.addItem("J")
        self.comboBox.addItem("L")
        
        self.box = self.findChild(QCheckBox, "checkBox")
        self.box2 = self.findChild(QCheckBox, "checkBox_2")
        
        self.button3 = self.findChild(QPushButton, "pushButton_3")
        self.label3 = self.findChild(QLabel, "label_3")

        
        #Click the dropdown menus
        self.button.clicked.connect(self.clicker)
        self.button2.clicked.connect(self.clicker2)
        self.button3.clicked.connect(self.clicker3)

        
        #show the window
        self.show()
    
    def clicker(self):
        #open file dialog 
        fname = QFileDialog.getExistingDirectory(self, "Open Folder", "")
        
        #output folder to screen
        if fname:
            path = str(fname + "/")
            self.label.setText(f'Input Folder: {path}')
            
            self.input_path = path
        
    def clicker2(self):
        #open file dialog 
        fname = QFileDialog.getExistingDirectory(self, "Open Folder", "")
        
        #output folder to screen
        if fname:
            path = str(fname + "/")
            self.label2.setText(f'Output Folder: {path}')
            
            self.output_path = path
        
        
    def clicker3(self):
        
        
        side = self.comboBox.currentText()
        
        
        in_file = self.input_path
        
        out_directory = self.output_path
        
        if self.checkBox.isChecked():
            images = True
        else:
            images = False
            
        if self.checkBox_2.isChecked():
            middle_line = True
        else:
            middle_line = False
        
        
        
        if "J" in side:
            out_file = out_directory + "SideJ_ConfocalResults/"
            if not os.path.exists(out_file):
                os.mkdir(out_file)
        else:
            out_file = out_directory + "SideL_ConfocalResults/"
            if not os.path.exists(out_file):
                os.mkdir(out_file)
        
        
        
        #==============================================================================
        #==============================================================================
        #==============================================================================
        
        
        missing = -999999.000
        
        data_bots = []
        
        xmin = -4000
        xmax = 0

        for i in range(1,45):
            data_bot = np.loadtxt(in_file + f'confocal_scan_{i}.csv', delimiter = ",")
            data_filtered_bot = data_bot[ data_bot[:,-1] != missing]
            data_filtered_bot = data_filtered_bot[ data_filtered_bot[:,-1] <= xmax]
            data_filtered_bot = data_filtered_bot[ data_filtered_bot[:,-1] >= xmin]
            #data_filtered_bot = data_filtered_bot[ np.where(np.abs(data_filtered_bot[1:,-1]-data_filtered_bot[:-1,-1]) < 100)]
        
            data_bots.append(data_filtered_bot)
        
        data_bots = np.concatenate(data_bots)
        # print(data_bots)
        
        
        if images:  
            
            fig = plt.figure(figsize=(20, 10), dpi=300)
            ax = fig.add_subplot(projection='3d')
            ax.set_box_aspect(aspect = (4,3,1))
        
        
        
            x = data_bots[:,0]
            y = data_bots[:,1]
            z = data_bots[:,-1]
        
            ax.scatter( x, y, z, c = z, cmap = 'jet', edgecolor= 'black', marker = '.', linewidths=0.2)
        
        
            ax.set_xlabel('Aerotech x $[mm]$', labelpad=20)
            ax.set_ylabel('Aerotech y $[mm]$', labelpad=5)
            ax.set_zlabel(r'confocal $[\mu m]$', labelpad=10)
        
            ax.view_init(10, -90)
            
            if "J" in side:
                plt.savefig(out_file + "SideJ_MountedStave_3D_Plot_WShieldBox.png")
            else:
                plt.savefig(out_file + "SideL_MountedStave_3D_Plot_WShieldBox.png")
            
            plt.show()
        
        
        #==============================================================================
        #==============================================================================
        #==============================================================================
        
        
        data_bots = []
        
        for i in range(1,45):
            data_bot = np.loadtxt(in_file + f'confocal_scan_{i}.csv', delimiter = ",")
            data_filtered_bot = data_bot[ data_bot[:,-1] != missing]
            data_filtered_bot = data_filtered_bot[ data_filtered_bot[:,-1] <= 2000]
            data_filtered_bot = data_filtered_bot[ data_filtered_bot[:,-1] >= -100]
            #data_filtered_bot = data_filtered_bot[ np.where(np.abs(data_filtered_bot[1:,-1]-data_filtered_bot[:-1,-1]) < 5000)]
        
            data_bots.append(data_filtered_bot)
        
        data_bots = np.concatenate(data_bots)
        # print(data_bots)

        if images: 
            
            fig = plt.figure(figsize=(20, 10), dpi=300)
            #ax = mplot3d.Axes3D(fig)
            ax = fig.add_subplot(projection='3d')
            ax.set_box_aspect(aspect = (4,3,1))
        
        
        
            x = data_bots[:,0]
            y = data_bots[:,1]
            z = data_bots[:,-1]
        
            ax.scatter( x, y, z, c = z, cmap = 'jet', edgecolor= 'black', marker = '.', linewidths=0.2)
        
        
            ax.set_xlabel('Aerotech x $[mm]$', labelpad=20)
            ax.set_ylabel('Aerotech y $[mm]$', labelpad=5)
            ax.set_zlabel(r'confocal $[\mu m]$', labelpad=10)
        
            ax.view_init(10, -90)
            
            if "J" in side:
                plt.savefig(out_file + "SideJ_MountedStave_3D_Plot_WOut_ShieldBox.png")
            else:
                plt.savefig(out_file + "SideL_MountedStave_3D_Plot_WOut_ShieldBox.png")
            
            
            plt.show()
        
        
        #==============================================================================
        #==============================================================================
        #==============================================================================
        
        
        
        data_bots = []
        
        for i in range(1,45):
            data_bot = np.loadtxt(in_file + f'confocal_scan_{i}.csv', delimiter = ",")
            data_filtered_bot = data_bot[ data_bot[:,-1] != missing]
            data_filtered_bot = data_filtered_bot[ data_filtered_bot[:,-1] <= 7000]
            data_filtered_bot = data_filtered_bot[ data_filtered_bot[:,-1] >= -500]
            data_filtered_bot = data_filtered_bot[ np.where(np.abs(data_filtered_bot[1:,-1]-data_filtered_bot[:-1,-1]) < 100)]
            data_bots.append(data_filtered_bot)
        fig, ax = plt.subplots(1, 1, figsize=(30,15))
        # print(data_bots)
        
        if images: 
            
            for i in range(44):
                x = data_bots[i][:,0]
                z = data_bots[i][:,-1]
                ax.plot( x, z) #label = f"Y={data_bots[i][0,1]:0.0f}")
        
            ax.set_xlabel(r"Aerotech X Coordinate $[mm]$", fontsize = 25)
            ax.set_ylabel(r"Confocal measurement $[\mu m]$", fontsize = 25)
            ax.grid(True)
            ax.legend(loc="best")
            ax.set_title(r"Confocal Measurements on Short Strip Stave X Direction At $25mms^{-1}$", fontsize = 25)
            plt.yticks(fontsize=25)
            plt.xticks(fontsize=25)
            plt.tight_layout()
            
            if "J" in side:
                plt.savefig(out_file + "SideJ_BusTape_2D_Plot.png")
            else:
                plt.savefig(out_file + "SideL_BusTape_2D_Plot.png")
            
            plt.show()
        
        
        #==============================================================================
        #==============================================================================
        #==============================================================================
        
        
        data_bot = np.loadtxt(in_file + "confocal_scan_2.csv", delimiter = ",")
        data_filtered_bot = data_bot[ data_bot[:,-1] != missing]
        data_filtered_bot = data_filtered_bot[ data_filtered_bot[:,-1] <= 1000]
        # print(data_filtered_bot)
        data_filtered_bot = data_filtered_bot[ data_filtered_bot[:,-1] >= xmin]
        # print(data_filtered_bot)
        data_filtered_bot = data_filtered_bot[ np.where(np.abs(data_filtered_bot[1:,-1]-data_filtered_bot[:-1,-1]) < 100)]
        data_filtered_bot = data_filtered_bot[ np.where(np.abs(data_filtered_bot[1:,-1]-data_filtered_bot[:-1,-1]) < 100)]
        data_filtered_bot = data_filtered_bot[ np.where(np.abs(data_filtered_bot[1:,-1]-data_filtered_bot[:-1,-1]) < 100)]
        # print(data_filtered_bot)
        
        data_top = np.loadtxt(in_file + "confocal_scan_44.csv", delimiter = ",")
        data_filtered_top = data_top[ data_top[:,-1] != missing]
        # print("data_filter_top", data_filtered_top)
        data_filtered_top = data_filtered_top[ data_filtered_top[:,-1] <= 1000]
        data_filtered_top = data_filtered_top[ data_filtered_top[:,-1] >= xmin]
        # print("data_filter_top", data_filtered_top)
        # print("data_filter_top_0", data_filtered_top[1:,-1])
        # print("data_filter_top_1", data_filtered_top[:-1,-1])
        data_filtered_top = data_filtered_top[ np.where(np.abs(data_filtered_top[1:,-1]-data_filtered_top[:-1,-1]) < 100)]
        data_filtered_top = data_filtered_top[ np.where(np.abs(data_filtered_top[1:,-1]-data_filtered_top[:-1,-1]) < 100)]
        data_filtered_top = data_filtered_top[ np.where(np.abs(data_filtered_top[1:,-1]-data_filtered_top[:-1,-1]) < 100)]
        # print("data_filter_top", data_filtered_top)
        #data_filtered_top[:,-1]*=0.1
        
        
        if images: 
            
            fig, ax = plt.subplots(1, 1, figsize=(20,10))
            # ax.vlines((data_continuous_filtered_2[25,0], data_continuous_filtered_2[79,0]), ymin=-5000, ymax=15000, color="red")
            # ax.plot(data_continuous_filtered_2[:,0], data_continuous_filtered_2[:,-1])
            # ax.plot(data_filtered_5mms[:,0] ,data_filtered_5mms[:,-1],'.')
            ax.plot(data_filtered_bot[:,0] ,data_filtered_bot[:,-1], label="Bottom")
            ax.plot(data_filtered_top[:,0] ,data_filtered_top[:,-1], label="Top")
            ax.set_xlabel(r"Aerotech X Coordinate $[mm]$", fontsize = 25)
            ax.set_ylabel(r"Confocal measurement $[\mu m]$", fontsize = 25)
            ax.grid(True)
            ax.legend(loc="best")
            ax.set_title(r"Confocal Measurements on Short Strip Stave X Direction At $25mms^{-1}$", fontsize = 25)
            plt.yticks(fontsize=25)
            plt.xticks(fontsize=25)
            plt.tight_layout()
            
            if "J" in side:
                plt.savefig(out_file + "SideJ_MountedStave_BusTape_Plot.png")
            else:
                plt.savefig(out_file + "SideL_MountedStave_BusTape_Plot.png")
                
            plt.show()
        
        
        #==============================================================================
        #==============================================================================
        #==============================================================================
        
        
        
        bot_diff = data_filtered_bot[1:,-1]-data_filtered_bot[:-1,-1]
        
        if images: 
            fig, ax = plt.subplots(1, 1, figsize=(20,10))
            ax.plot(data_filtered_bot[:-1,0] ,np.abs(bot_diff[:]))
            ax.set_xlabel(r"Aerotech X Coordinate $[mm]$")
            ax.set_ylabel(r"Confocal measurement $[\mu m]$")
            ax.grid(True)
            # ax.legend(loc="best")
            ax.set_title(r"Confocal Measurements on Short Strip Stave X Direction At $25mms^{-1}$")
            
            plt.tight_layout()
            plt.show()
        
        
        
        #==============================================================================
        #==============================================================================
        #==============================================================================
        
        # print("data_filter_bot",data_filtered_bot)
        # print("data_filter_bot[:,0]",data_filtered_bot[:,0])
        # print("data_filter_bot[:,-1]",data_filtered_bot[:,-1])
        bottom_fit = np.polyfit(data_filtered_bot[:,0],data_filtered_bot[:,-1],20)
        bot_1d = np.poly1d(bottom_fit)
        x = np.arange(data_filtered_bot[-1,0],data_filtered_bot[0,0],0.1)
        
        # print("data_filter_bot",data_filtered_top)
        # print("data_filter_bot[:,0]",data_filtered_top[:,0])
        # print("data_filter_bot[:,-1]",data_filtered_top[:,-1])
        top_fit = np.polyfit(data_filtered_top[:,0],data_filtered_top[:,-1], 20)
        # print("top_fit", top_fit)
        top_1d = np.poly1d(top_fit)
        
        if images: 
            fig, ax = plt.subplots(1, 1, figsize=(20,10))
            ax.plot(data_filtered_bot[:,0] ,data_filtered_bot[:,-1], label="Bottom Bus tape")
            ax.plot(data_filtered_top[:,0] ,data_filtered_top[:,-1], label="Top Bus tape")
            ax.plot(x, bot_1d(x), label="Bottom Fit, deg=20")
            ax.plot(x, top_1d(x), label="Top Fit, deg=20")
            ax.set_xlabel(r"Aerotech X Coordinate $[mm]$", fontsize = 25)
            ax.set_ylabel(r"Confocal measurement $[\mu m]$", fontsize = 25)
            ax.grid(True)
            ax.legend(loc="best")
            ax.set_title(r"Confocal Measurements on Short Strip Stave Bus Tape X Direction At $25mms^{-1}$", fontsize = 25)
            plt.yticks(fontsize=25)
            plt.xticks(fontsize=25)
            plt.tight_layout()
            
            if "J" in side:
                plt.savefig(out_file + "SideJ_MountedStave_FittedBusTape_Plot.png")
            else:
                plt.savefig(out_file + "SideL_MountedStave_FittedBusTape_Plot.png")
            
            plt.show()
        
        
        #==============================================================================
        #==============================================================================
        #==============================================================================
        
        
        
        sensors = [in_file + f'confocal_scan_{i}.csv' for i in range(5,42)]
        if images:
            fig, ax = plt.subplots(1, 1, figsize=(20,10))
            
            ax.plot(data_filtered_bot[:,0] ,data_filtered_bot[:,-1], label="Bot BT")
            ax.plot(data_filtered_top[:,0] ,data_filtered_top[:,-1], label="Top BT")
            
        for sensor in sensors:
            data = np.loadtxt(sensor, delimiter = ",")
            data_filtered = data[ data[:,-1] != missing]
            data_filtered = data_filtered[ data_filtered[:,-1] >= xmin]
            data_filtered = data_filtered[ data_filtered[:,-1] <= xmax]
            data_filtered = data_filtered[ np.where(np.abs(data_filtered[1:,-1]-data_filtered[:-1,-1]) < 100)]
            
            if images:
                ax.plot(data_filtered[:,0] ,data_filtered[:,-1], label=f"Y={data_filtered[0,1]:0.1f}")
        
        if images: 
            ax.set_xlabel(r"Aerotech X Coordinate $[mm]$", fontsize = 25)
            ax.set_ylabel(r"Confocal measurement $[\mu m]$", fontsize = 25)
            ax.grid(True)
            ax.set_title(r"Confocal Measurements on Short Strip Stave X Direction At $25mms^{-1}$", fontsize = 25)
            #ax.legend(loc="best")
            plt.yticks(fontsize=25)
            plt.xticks(fontsize=25)
            plt.tight_layout()
        
        
            if "J" in side:
                plt.savefig(out_file + "SideJ_MountedStave_NoPB_Plot.png")
            else:
                plt.savefig(out_file + "SideL_MountedStave_NoPB_Plot.png")
            
            plt.show()
        
        
        
        #==============================================================================
        #==============================================================================
        #==============================================================================
        
        
        if images:
            fig, ax = plt.subplots(1, 1, figsize=(20,10))
            
            
            ax.plot(data_filtered_bot[:,0] ,data_filtered_bot[:,-1], label="Bot BT")
            ax.plot(data_filtered_top[:,0] ,data_filtered_top[:,-1], label="Top BT")
        
        data1 = np.loadtxt(sensors[0], delimiter = ",")
        data_filtered1 = data1[ data1[:,-1] != missing]
        data_filtered1 = data_filtered1[ data_filtered1[:,-1] >= xmin]
        data_filtered1 = data_filtered1[ data_filtered1[:,-1] <= xmax]
        
        if images:
            ax.plot(data_filtered1[:,0] ,data_filtered1[:,-1], label=f"Y={data_filtered1[0,1]:0.1f}")
        
        data2 = np.loadtxt(sensors[-1], delimiter = ",")
        data_filtered2 = data2[ data2[:,-1] != missing]
        data_filtered2 = data_filtered2[ data_filtered2[:,-1] >= xmin]
        data_filtered2 = data_filtered2[ data_filtered2[:,-1] <= xmax]
        
        
        
        if images: 
            ax.plot(data_filtered2[:,0] ,data_filtered2[:,-1], label=f"Y={data_filtered2[0,1]:0.1f}")
        
            ax.set_xlabel(r"Aerotech X Coordinate $[mm]$", fontsize = 25)
            ax.set_ylabel(r"Confocal measurement $[\mu m]$", fontsize = 25)
            ax.grid(True)
            ax.set_title(r"Confocal Measurements on Short Strip Stave X Direction At $25mms^{-1}$", fontsize = 25)
            ax.legend(loc="best")
            plt.yticks(fontsize=25)
            plt.xticks(fontsize=25)
            plt.tight_layout()
            
            if "J" in side:
                plt.savefig(out_file + "SideJ_MountedStave_TB_NonRot_Plot.png")
            else:
                plt.savefig(out_file + "SideL_MountedStave_TB_NonRot_Plot.png")
            
            plt.show()
        
        
        
        #==============================================================================
        #==============================================================================
        #==============================================================================
        
        
        bot_listX = data_filtered_bot[:,0]
        bot_listY = data_filtered_bot[:,-1]
        
        top_listX = data_filtered_top[:,0]
        top_listY = data_filtered_top[:,-1]
        
        cub_spline_bot = CubicSpline(bot_listX[::-1],bot_listY[::-1])
        cub_spline_top = CubicSpline(top_listX[::-1],top_listY[::-1])
        from scipy.stats import linregress
        
        def lin_diff(x, y, z):
            z1 = cub_spline_bot(x)
            z2 = cub_spline_top(x)
            res = linregress((data_filtered_bot[0,1], data_filtered_top[0,1]), (z1, z2))
            return z - res.slope*y - res.intercept
        def lin_diff2(array):
            z1 = cub_spline_bot(array[0])
            z2 = cub_spline_top(array[0])
            res = linregress((data_filtered_bot[0,1], data_filtered_top[0,1]), (z1, z2))
            return array[0], array[1], array[-1] - res.slope*array[1] - res.intercept
        
        d=np.apply_along_axis(lin_diff2,axis=1, arr=data_filtered1)
        d1=np.apply_along_axis(lin_diff2,axis=1, arr=data_filtered2)
        
        
        #==============================================================================
        #==============================================================================
        #==============================================================================
        
        
        d_filtered = d[d[:,-1]<1500]
        d1_filtered = d1[d1[:,-1]<1500]
        
        if images: 
            fig, ax = plt.subplots(1, 1, figsize=(20,10))
        
            ax.plot(d_filtered[:,0] ,d_filtered[:,-1], label=f"Y={data_filtered1[0,1]:0.0f}")
            ax.plot(d1_filtered[:,0] , d1_filtered[:,-1], label=f"Y={data_filtered2[0,1]:0.0f}")
        
            ax.set_xlabel(r"Aerotech X Coordinate $[mm]$", fontsize=25)
            ax.set_ylabel(r"Confocal measurement $[\mu m]$", fontsize=25)
            ax.grid(True)
            ax.set_title(r"Confocal Measurements on Short Strip Stave X Direction At $25mms^{-1}$", fontsize=25)
            ax.legend(loc="best")
            plt.yticks(fontsize=25)
            plt.xticks(fontsize=25)
            plt.tight_layout()
            
            if "J" in side:
                plt.savefig(out_file + "SideJ_MountedStave_TB_Rot_Plot.png")
            else:
                plt.savefig(out_file + "SideL_MountedStave_TB_Rot_Plot.png")
            
            plt.show()
        
        
        #==============================================================================
        #==============================================================================
        #==============================================================================
        
        if images:
            fig, ax = plt.subplots(1, 1, figsize=(20,10))
        
        
        nearest = []
        data_combined = []
        for sensor in sensors:
            data = np.loadtxt(sensor, delimiter = ",")
            data_filtered = data[ data[:,-1] != missing]
            data_filtered = data_filtered[ data_filtered[:,-1] >= xmin]
            data_filtered = data_filtered[ data_filtered[:,-1] <= xmax]
        
            data_rot = np.apply_along_axis(lin_diff2,axis=1, arr=data_filtered)
            data_combined.append(data_rot)
            near = (np.abs(data_rot[:,0] - 60).argmin())
            nearest.append(data_rot[near,-1])
            if images:
                ax.plot(data_filtered[:,0] ,data_rot[:,-1], label=f"Y={data_filtered[0,1]:0.0f}")
        
        
        
        if images: 
            ax.set_xlabel(r"Aerotech X Coordinate $[mm]$", fontsize=25)
            ax.set_ylabel(r"Confocal measurement $[\mu m]$", fontsize=25)
            ax.grid(True)
            ax.set_title(r"Confocal Measurements on Short Strip Stave X Direction At $25mms^{-1}$. Height bounds: [700, 3000]$\mu m$", fontsize=25)
            # ax.legend(loc="best")
            plt.yticks(fontsize=25)
            plt.xticks(fontsize=25)
            plt.tight_layout()
            
            if "J" in side:
                plt.savefig(out_file + "SideJ_MountedStave_All_Rot_Plot.png")
            else:
                plt.savefig(out_file + "SideL_MountedStave_All_Rot_Plot.png")
            
            plt.show()
        
        
        #==============================================================================
        #==============================================================================
        #==============================================================================
        
        
        
        if images:
            plt.figure()
            plt.plot(d_filtered[325:360,0],d_filtered[325:360,-1])
            
            plt.show()
        #print(d_filtered[325:360,0])
        #near_data = np.array(nearest)
        #print(np.mean(near_data))
        #print(np.std(near_data))
        
        
        
        #==============================================================================
        #==============================================================================
        #==============================================================================
        
        
        if images:
            fig, ax = plt.subplots(1, 1, figsize=(20,10))
        
        def lin_diff3(array):
            z1 = bot_1d(array[0])
            z2 = top_1d(array[0])
            res = linregress((data_filtered_bot[0,1], data_filtered_top[0,1]), (z1, z2))
            return array[0], array[1], array[-1] - res.slope*array[1] - res.intercept
        
        nearest = []
        data_combined = []
        for sensor in sensors:
            data = np.loadtxt(sensor, delimiter = ",")
            data_filtered = data[ data[:,-1] != missing]
            # print(data_filtered)
            data_filtered = data_filtered[ data_filtered[:,-1] >= xmin]
            # print(data_filtered)
            data_filtered = data_filtered[ data_filtered[:,-1] <= xmax]
            # print(data_filtered)
            data_filtered = data_filtered[ np.where(np.abs(data_filtered[1:,-1]-data_filtered[:-1,-1]) < 100)]
            # print(data_filtered)
        
            data_rot = np.apply_along_axis(lin_diff3,axis=1, arr=data_filtered)
            data_rot = data_rot[data_rot[:,-1] >= 0]
            data_combined.append(data_rot)
            near = (np.abs(data_rot[:,0] - 60).argmin())
            nearest.append(data_rot[near,-1])
            if images:
                ax.plot(data_rot[:,0] ,data_rot[:,-1], label=f"Y={data_filtered[0,1]:0.0f}")
        
        uploader_data = []
        
        for i in range(len(data_combined)):
            for j in range(len(data_combined[i])):
                uploader_data.append([data_combined[i][j][0], data_combined[i][j][1], data_combined[i][j][-1]])
        
        
        if images: 
            ax.set_xlabel(r"Aerotech X Coordinate $[mm]$", fontsize=25)
            ax.set_ylabel(r"Confocal measurement $[\mu m]$", fontsize=25)
            ax.grid(True)
            ax.set_title(r"Confocal Measurements on Short Strip Stave X Direction At $25mms^{-1}$", fontsize=25)
            #ax.legend(loc="best")
            plt.yticks(fontsize=25)
            plt.xticks(fontsize=25)
            plt.tight_layout()
            
            if "J" in side:
                plt.savefig(out_file + "SideJ_MountedStave_All_Flat_Plot_2.0.png")
            else:
                plt.savefig(out_file + "SideL_MountedStave_All_Flat_Plot_2.0.png")
            
            
            plt.show()
        
        
        #==============================================================================
        #==============================================================================
        #==============================================================================
        
        if middle_line:
            
            if "J" in side:
                with open(out_file + "Side_J_middle_line_2.0.csv", 'w') as file:
                        for j in range(len(data_combined[24])):
                            writer = csv.writer(file)
                            writer.writerow(data_combined[24][j])
            else:
                with open(out_file + "Side_L_middle_line_2.0.csv", 'w') as file:
                        for j in range(len(data_combined[24])):
                            writer = csv.writer(file)
                            writer.writerow(data_combined[24][j])
                            
        self.label3.setText("Status: DONE!")
            

#initialize the app
app = QApplication(sys.argv)
UIWindow = UI()
app.exec_()

# =============================================================================
# 
# 
# side = "J"
# 
# #in_file = "/Users/mayamancini/Documents/Metrology/PPB2B/06-08-23 Side L Confocal Scan/"
# in_file = "/Users/mayamancini/Documents/Metrology/PPB2B/05-31-23 Side J Mount/Confocal Scan/"
# 
# out_directory = "/Users/mayamancini/Documents/Metrology/PPB2B/TEST/"
# 
# images = True
# 
# middle_line = False
# 
# if "J" in side:
#     out_file = out_directory + "SideJ_ConfocalResults/"
#     if not os.path.exists(out_file):
#         os.mkdir(out_file)
# else:
#     out_file = out_directory + "SideL_ConfocalResults/"
#     if not os.path.exists(out_file):
#         os.mkdir(out_file)
# 
# 
# 
# #==============================================================================
# #==============================================================================
# #==============================================================================
# 
# 
# missing = -999999.000
# 
# data_bots = []
# 
# for i in range(1,45):
#     data_bot = np.loadtxt(in_file + f'confocal_scan_{i}.csv', delimiter = ",")
#     data_filtered_bot = data_bot[ data_bot[:,-1] != missing]
#     data_filtered_bot = data_filtered_bot[ data_filtered_bot[:,-1] <= 7000]
#     data_filtered_bot = data_filtered_bot[ data_filtered_bot[:,-1] >= -500]
#     #data_filtered_bot = data_filtered_bot[ np.where(np.abs(data_filtered_bot[1:,-1]-data_filtered_bot[:-1,-1]) < 100)]
# 
#     data_bots.append(data_filtered_bot)
# 
# data_bots = np.concatenate(data_bots)
# 
# 
# if images:  
#     
#     fig = plt.figure(figsize=(20, 10), dpi=300)
#     ax = fig.add_subplot(projection='3d')
#     ax.set_box_aspect(aspect = (4,3,1))
# 
# 
# 
#     x = data_bots[:,0]
#     y = data_bots[:,1]
#     z = data_bots[:,-1]
# 
#     ax.scatter( x, y, z, c = z, cmap = 'jet', edgecolor= 'black', marker = '.', linewidths=0.2)
# 
# 
#     ax.set_xlabel('Aerotech x $[mm]$', labelpad=20)
#     ax.set_ylabel('Aerotech y $[mm]$', labelpad=5)
#     ax.set_zlabel(r'confocal $[\mu m]$', labelpad=10)
# 
#     ax.view_init(10, -90)
#     
#     if "J" in side:
#         plt.savefig(out_file + "SideJ_MountedStave_3D_Plot_WShieldBox.png")
#     else:
#         plt.savefig(out_file + "SideL_MountedStave_3D_Plot_WShieldBox.png")
#     
#     plt.show()
# 
# 
# #==============================================================================
# #==============================================================================
# #==============================================================================
# 
# 
# data_bots = []
# 
# for i in range(1,45):
#     data_bot = np.loadtxt(in_file + f'confocal_scan_{i}.csv', delimiter = ",")
#     data_filtered_bot = data_bot[ data_bot[:,-1] != missing]
#     data_filtered_bot = data_filtered_bot[ data_filtered_bot[:,-1] <= 2000]
#     data_filtered_bot = data_filtered_bot[ data_filtered_bot[:,-1] >= -100]
#     #data_filtered_bot = data_filtered_bot[ np.where(np.abs(data_filtered_bot[1:,-1]-data_filtered_bot[:-1,-1]) < 5000)]
# 
#     data_bots.append(data_filtered_bot)
# 
# data_bots = np.concatenate(data_bots)
# 
# if images: 
#     
#     fig = plt.figure(figsize=(20, 10), dpi=300)
#     #ax = mplot3d.Axes3D(fig)
#     ax = fig.add_subplot(projection='3d')
#     ax.set_box_aspect(aspect = (4,3,1))
# 
# 
# 
#     x = data_bots[:,0]
#     y = data_bots[:,1]
#     z = data_bots[:,-1]
# 
#     ax.scatter( x, y, z, c = z, cmap = 'jet', edgecolor= 'black', marker = '.', linewidths=0.2)
# 
# 
#     ax.set_xlabel('Aerotech x $[mm]$', labelpad=20)
#     ax.set_ylabel('Aerotech y $[mm]$', labelpad=5)
#     ax.set_zlabel(r'confocal $[\mu m]$', labelpad=10)
# 
#     ax.view_init(10, -90)
#     
#     if "J" in side:
#         plt.savefig(out_file + "SideJ_MountedStave_3D_Plot_WOut_ShieldBox.png")
#     else:
#         plt.savefig(out_file + "SideL_MountedStave_3D_Plot_WOut_ShieldBox.png")
#     
#     
#     plt.show()
# 
# 
# #==============================================================================
# #==============================================================================
# #==============================================================================
# 
# 
# 
# data_bots = []
# 
# for i in range(1,45):
#     data_bot = np.loadtxt(in_file + f'confocal_scan_{i}.csv', delimiter = ",")
#     data_filtered_bot = data_bot[ data_bot[:,-1] != missing]
#     data_filtered_bot = data_filtered_bot[ data_filtered_bot[:,-1] <= 7000]
#     data_filtered_bot = data_filtered_bot[ data_filtered_bot[:,-1] >= -500]
#     data_filtered_bot = data_filtered_bot[ np.where(np.abs(data_filtered_bot[1:,-1]-data_filtered_bot[:-1,-1]) < 100)]
#     data_bots.append(data_filtered_bot)
# fig, ax = plt.subplots(1, 1, figsize=(30,15))
# 
# 
# if images: 
#     
#     for i in range(44):
#         x = data_bots[i][:,0]
#         z = data_bots[i][:,-1]
#         ax.plot( x, z, label = f"Y={data_bots[i][0,1]:0.0f}")
# 
#     ax.set_xlabel(r"Aerotech X Coordinate $[mm]$", fontsize = 25)
#     ax.set_ylabel(r"Confocal measurement $[\mu m]$", fontsize = 25)
#     ax.grid(True)
#     ax.legend(loc="best")
#     ax.set_title(r"Confocal Measurements on Short Strip Stave X Direction At $25mms^{-1}$", fontsize = 25)
#     plt.yticks(fontsize=25)
#     plt.xticks(fontsize=25)
#     plt.tight_layout()
#     
#     if "J" in side:
#         plt.savefig(out_file + "SideJ_BusTape_2D_Plot.png")
#     else:
#         plt.savefig(out_file + "SideL_BusTape_2D_Plot.png")
#     
#     plt.show()
# 
# 
# #==============================================================================
# #==============================================================================
# #==============================================================================
# 
# 
# data_bot = np.loadtxt(in_file + "confocal_scan_2.csv", delimiter = ",")
# data_filtered_bot = data_bot[ data_bot[:,-1] != missing]
# data_filtered_bot = data_filtered_bot[ data_filtered_bot[:,-1] <= 1000]
# data_filtered_bot = data_filtered_bot[ data_filtered_bot[:,-1] >= -2000]
# data_filtered_bot = data_filtered_bot[ np.where(np.abs(data_filtered_bot[1:,-1]-data_filtered_bot[:-1,-1]) < 100)]
# data_filtered_bot = data_filtered_bot[ np.where(np.abs(data_filtered_bot[1:,-1]-data_filtered_bot[:-1,-1]) < 100)]
# data_filtered_bot = data_filtered_bot[ np.where(np.abs(data_filtered_bot[1:,-1]-data_filtered_bot[:-1,-1]) < 100)]
# 
# 
# data_top = np.loadtxt(in_file + "confocal_scan_44.csv", delimiter = ",")
# data_filtered_top = data_top[ data_top[:,-1] != missing]
# data_filtered_top = data_filtered_top[ data_filtered_top[:,-1] <= 1000]
# data_filtered_top = data_filtered_top[ data_filtered_top[:,-1] >= -2000]
# data_filtered_top = data_filtered_top[ np.where(np.abs(data_filtered_top[1:,-1]-data_filtered_top[:-1,-1]) < 100)]
# data_filtered_top = data_filtered_top[ np.where(np.abs(data_filtered_top[1:,-1]-data_filtered_top[:-1,-1]) < 100)]
# data_filtered_top = data_filtered_top[ np.where(np.abs(data_filtered_top[1:,-1]-data_filtered_top[:-1,-1]) < 100)]
# #data_filtered_top[:,-1]*=0.1
# 
# 
# if images: 
#     
#     fig, ax = plt.subplots(1, 1, figsize=(20,10))
#     # ax.vlines((data_continuous_filtered_2[25,0], data_continuous_filtered_2[79,0]), ymin=-5000, ymax=15000, color="red")
#     # ax.plot(data_continuous_filtered_2[:,0], data_continuous_filtered_2[:,-1])
#     # ax.plot(data_filtered_5mms[:,0] ,data_filtered_5mms[:,-1],'.')
#     ax.plot(data_filtered_bot[:,0] ,data_filtered_bot[:,-1], label="Bottom")
#     ax.plot(data_filtered_top[:,0] ,data_filtered_top[:,-1], label="Top")
#     ax.set_xlabel(r"Aerotech X Coordinate $[mm]$", fontsize = 25)
#     ax.set_ylabel(r"Confocal measurement $[\mu m]$", fontsize = 25)
#     ax.grid(True)
#     ax.legend(loc="best")
#     ax.set_title(r"Confocal Measurements on Short Strip Stave X Direction At $25mms^{-1}$", fontsize = 25)
#     plt.yticks(fontsize=25)
#     plt.xticks(fontsize=25)
#     plt.tight_layout()
#     
#     if "J" in side:
#         plt.savefig(out_file + "SideJ_MountedStave_BusTape_Plot.png")
#     else:
#         plt.savefig(out_file + "SideL_MountedStave_BusTape_Plot.png")
#         
#     plt.show()
# 
# 
# #==============================================================================
# #==============================================================================
# #==============================================================================
# 
# 
# 
# bot_diff = data_filtered_bot[1:,-1]-data_filtered_bot[:-1,-1]
# 
# if images: 
#     fig, ax = plt.subplots(1, 1, figsize=(20,10))
#     ax.plot(data_filtered_bot[:-1,0] ,np.abs(bot_diff[:]))
#     ax.set_xlabel(r"Aerotech X Coordinate $[mm]$")
#     ax.set_ylabel(r"Confocal measurement $[\mu m]$")
#     ax.grid(True)
#     # ax.legend(loc="best")
#     ax.set_title(r"Confocal Measurements on Short Strip Stave X Direction At $25mms^{-1}$")
#     
#     plt.tight_layout()
#     plt.show()
# 
# 
# 
# #==============================================================================
# #==============================================================================
# #==============================================================================
# 
# 
# bottom_fit = np.polyfit(data_filtered_bot[:,0],data_filtered_bot[:,-1],20)
# bot_1d = np.poly1d(bottom_fit)
# x = np.arange(data_filtered_bot[-1,0],data_filtered_bot[0,0],0.1)
# 
# top_fit = np.polyfit(data_filtered_top[:,0],data_filtered_top[:,-1], 20)
# top_1d = np.poly1d(top_fit)
# 
# if images: 
#     fig, ax = plt.subplots(1, 1, figsize=(20,10))
#     ax.plot(data_filtered_bot[:,0] ,data_filtered_bot[:,-1], label="Bottom Bus tape")
#     ax.plot(data_filtered_top[:,0] ,data_filtered_top[:,-1], label="Top Bus tape")
#     ax.plot(x, bot_1d(x), label="Bottom Fit, deg=20")
#     ax.plot(x, top_1d(x), label="Top Fit, deg=20")
#     ax.set_xlabel(r"Aerotech X Coordinate $[mm]$", fontsize = 25)
#     ax.set_ylabel(r"Confocal measurement $[\mu m]$", fontsize = 25)
#     ax.grid(True)
#     ax.legend(loc="best")
#     ax.set_title(r"Confocal Measurements on Short Strip Stave Bus Tape X Direction At $25mms^{-1}$", fontsize = 25)
#     plt.yticks(fontsize=25)
#     plt.xticks(fontsize=25)
#     plt.tight_layout()
#     
#     if "J" in side:
#         plt.savefig(out_file + "SideJ_MountedStave_FittedBusTape_Plot.png")
#     else:
#         plt.savefig(out_file + "SideL_MountedStave_FittedBusTape_Plot.png")
#     
#     plt.show()
# 
# 
# #==============================================================================
# #==============================================================================
# #==============================================================================
# 
# 
# 
# sensors = [in_file + f'confocal_scan_{i}.csv' for i in range(5,42)]
# if images:
#     fig, ax = plt.subplots(1, 1, figsize=(20,10))
#     
#     ax.plot(data_filtered_bot[:,0] ,data_filtered_bot[:,-1], label="Bot BT")
#     ax.plot(data_filtered_top[:,0] ,data_filtered_top[:,-1], label="Top BT")
#     
# for sensor in sensors:
#     data = np.loadtxt(sensor, delimiter = ",")
#     data_filtered = data[ data[:,-1] != missing]
#     data_filtered = data_filtered[ data_filtered[:,-1] >= 0]
#     data_filtered = data_filtered[ data_filtered[:,-1] <= 7000]
#     data_filtered = data_filtered[ np.where(np.abs(data_filtered[1:,-1]-data_filtered[:-1,-1]) < 100)]
#     
#     if images:
#         ax.plot(data_filtered[:,0] ,data_filtered[:,-1], label=f"Y={data_filtered[0,1]:0.1f}")
# 
# if images: 
#     ax.set_xlabel(r"Aerotech X Coordinate $[mm]$", fontsize = 25)
#     ax.set_ylabel(r"Confocal measurement $[\mu m]$", fontsize = 25)
#     ax.grid(True)
#     ax.set_title(r"Confocal Measurements on Short Strip Stave X Direction At $25mms^{-1}$", fontsize = 25)
#     #ax.legend(loc="best")
#     plt.yticks(fontsize=25)
#     plt.xticks(fontsize=25)
#     plt.tight_layout()
# 
# 
#     if "J" in side:
#         plt.savefig(out_file + "SideJ_MountedStave_NoPB_Plot.png")
#     else:
#         plt.savefig(out_file + "SideL_MountedStave_NoPB_Plot.png")
#     
#     plt.show()
# 
# 
# 
# #==============================================================================
# #==============================================================================
# #==============================================================================
# 
# 
# if images:
#     fig, ax = plt.subplots(1, 1, figsize=(20,10))
#     
#     
#     ax.plot(data_filtered_bot[:,0] ,data_filtered_bot[:,-1], label="Bot BT")
#     ax.plot(data_filtered_top[:,0] ,data_filtered_top[:,-1], label="Top BT")
# 
# data1 = np.loadtxt(sensors[0], delimiter = ",")
# data_filtered1 = data1[ data1[:,-1] != missing]
# data_filtered1 = data_filtered1[ data_filtered1[:,-1] >= 0]
# data_filtered1 = data_filtered1[ data_filtered1[:,-1] <= 7000]
# 
# if images:
#     ax.plot(data_filtered1[:,0] ,data_filtered1[:,-1], label=f"Y={data_filtered1[0,1]:0.1f}")
# 
# data2 = np.loadtxt(sensors[-1], delimiter = ",")
# data_filtered2 = data2[ data2[:,-1] != missing]
# data_filtered2 = data_filtered2[ data_filtered2[:,-1] >= 0]
# data_filtered2 = data_filtered2[ data_filtered2[:,-1] <= 7000]
# 
# 
# 
# if images: 
#     ax.plot(data_filtered2[:,0] ,data_filtered2[:,-1], label=f"Y={data_filtered2[0,1]:0.1f}")
# 
#     ax.set_xlabel(r"Aerotech X Coordinate $[mm]$", fontsize = 25)
#     ax.set_ylabel(r"Confocal measurement $[\mu m]$", fontsize = 25)
#     ax.grid(True)
#     ax.set_title(r"Confocal Measurements on Short Strip Stave X Direction At $25mms^{-1}$", fontsize = 25)
#     ax.legend(loc="best")
#     plt.yticks(fontsize=25)
#     plt.xticks(fontsize=25)
#     plt.tight_layout()
#     
#     if "J" in side:
#         plt.savefig(out_file + "SideJ_MountedStave_TB_NonRot_Plot.png")
#     else:
#         plt.savefig(out_file + "SideL_MountedStave_TB_NonRot_Plot.png")
#     
#     plt.show()
# 
# 
# 
# #==============================================================================
# #==============================================================================
# #==============================================================================
# 
# 
# bot_listX = data_filtered_bot[:,0]
# bot_listY = data_filtered_bot[:,-1]
# 
# top_listX = data_filtered_top[:,0]
# top_listY = data_filtered_top[:,-1]
# 
# cub_spline_bot = CubicSpline(bot_listX[::-1],bot_listY[::-1])
# cub_spline_top = CubicSpline(top_listX[::-1],top_listY[::-1])
# from scipy.stats import linregress
# 
# def lin_diff(x, y, z):
#     z1 = cub_spline_bot(x)
#     z2 = cub_spline_top(x)
#     res = linregress((data_filtered_bot[0,1], data_filtered_top[0,1]), (z1, z2))
#     return z - res.slope*y - res.intercept
# def lin_diff2(array):
#     z1 = cub_spline_bot(array[0])
#     z2 = cub_spline_top(array[0])
#     res = linregress((data_filtered_bot[0,1], data_filtered_top[0,1]), (z1, z2))
#     return array[0], array[1], array[-1] - res.slope*array[1] - res.intercept
# 
# d=np.apply_along_axis(lin_diff2,axis=1, arr=data_filtered1)
# d1=np.apply_along_axis(lin_diff2,axis=1, arr=data_filtered2)
# 
# 
# #==============================================================================
# #==============================================================================
# #==============================================================================
# 
# 
# d_filtered = d[d[:,-1]<1500]
# d1_filtered = d1[d1[:,-1]<1500]
# 
# if images: 
#     fig, ax = plt.subplots(1, 1, figsize=(20,10))
# 
#     ax.plot(d_filtered[:,0] ,d_filtered[:,-1], label=f"Y={data_filtered1[0,1]:0.0f}")
#     ax.plot(d1_filtered[:,0] , d1_filtered[:,-1], label=f"Y={data_filtered2[0,1]:0.0f}")
# 
#     ax.set_xlabel(r"Aerotech X Coordinate $[mm]$", fontsize=25)
#     ax.set_ylabel(r"Confocal measurement $[\mu m]$", fontsize=25)
#     ax.grid(True)
#     ax.set_title(r"Confocal Measurements on Short Strip Stave X Direction At $25mms^{-1}$", fontsize=25)
#     ax.legend(loc="best")
#     plt.yticks(fontsize=25)
#     plt.xticks(fontsize=25)
#     plt.tight_layout()
#     
#     if "J" in side:
#         plt.savefig(out_file + "SideJ_MountedStave_TB_Rot_Plot.png")
#     else:
#         plt.savefig(out_file + "SideL_MountedStave_TB_Rot_Plot.png")
#     
#     plt.show()
# 
# 
# #==============================================================================
# #==============================================================================
# #==============================================================================
# 
# if images:
#     fig, ax = plt.subplots(1, 1, figsize=(20,10))
# 
# 
# nearest = []
# data_combined = []
# for sensor in sensors:
#     data = np.loadtxt(sensor, delimiter = ",")
#     data_filtered = data[ data[:,-1] != missing]
#     data_filtered = data_filtered[ data_filtered[:,-1] >= 0]
#     data_filtered = data_filtered[ data_filtered[:,-1] <= 7000]
# 
#     data_rot = np.apply_along_axis(lin_diff2,axis=1, arr=data_filtered)
#     data_combined.append(data_rot)
#     near = (np.abs(data_rot[:,0] - 60).argmin())
#     nearest.append(data_rot[near,-1])
#     if images:
#         ax.plot(data_filtered[:,0] ,data_rot[:,-1], label=f"Y={data_filtered[0,1]:0.0f}")
# 
# 
# 
# if images: 
#     ax.set_xlabel(r"Aerotech X Coordinate $[mm]$", fontsize=25)
#     ax.set_ylabel(r"Confocal measurement $[\mu m]$", fontsize=25)
#     ax.grid(True)
#     ax.set_title(r"Confocal Measurements on Short Strip Stave X Direction At $25mms^{-1}$. Height bounds: [700, 3000]$\mu m$", fontsize=25)
#     # ax.legend(loc="best")
#     plt.yticks(fontsize=25)
#     plt.xticks(fontsize=25)
#     plt.tight_layout()
#     
#     if "J" in side:
#         plt.savefig(out_file + "SideJ_MountedStave_All_Rot_Plot.png")
#     else:
#         plt.savefig(out_file + "SideL_MountedStave_All_Rot_Plot.png")
#     
#     plt.show()
# 
# 
# #==============================================================================
# #==============================================================================
# #==============================================================================
# 
# 
# 
# if images:
#     plt.figure()
#     plt.plot(d_filtered[325:360,0],d_filtered[325:360,-1])
#     
#     plt.show()
# #print(d_filtered[325:360,0])
# near_data = np.array(nearest)
# #print(np.mean(near_data))
# #print(np.std(near_data))
# 
# 
# 
# #==============================================================================
# #==============================================================================
# #==============================================================================
# 
# 
# if images:
#     fig, ax = plt.subplots(1, 1, figsize=(20,10))
# 
# def lin_diff3(array):
#     z1 = bot_1d(array[0])
#     z2 = top_1d(array[0])
#     res = linregress((data_filtered_bot[0,1], data_filtered_top[0,1]), (z1, z2))
#     return array[0], array[1], array[-1] - res.slope*array[1] - res.intercept
# 
# nearest = []
# data_combined = []
# for sensor in sensors:
#     data = np.loadtxt(sensor, delimiter = ",")
#     data_filtered = data[ data[:,-1] != missing]
#     data_filtered = data_filtered[ data_filtered[:,-1] >= 0]
#     data_filtered = data_filtered[ data_filtered[:,-1] <= 7000]
#     data_filtered = data_filtered[ np.where(np.abs(data_filtered[1:,-1]-data_filtered[:-1,-1]) < 100)]
# 
#     data_rot = np.apply_along_axis(lin_diff3,axis=1, arr=data_filtered)
#     data_rot = data_rot[data_rot[:,-1] >= 0]
#     data_combined.append(data_rot)
#     near = (np.abs(data_rot[:,0] - 60).argmin())
#     nearest.append(data_rot[near,-1])
#     if images:
#         ax.plot(data_rot[:,0] ,data_rot[:,-1], label=f"Y={data_filtered[0,1]:0.0f}")
# 
# uploader_data = []
# 
# for i in range(len(data_combined)):
#     for j in range(len(data_combined[i])):
#         uploader_data.append([data_combined[i][j][0], data_combined[i][j][1], data_combined[i][j][-1]])
# 
# 
# if images: 
#     ax.set_xlabel(r"Aerotech X Coordinate $[mm]$", fontsize=25)
#     ax.set_ylabel(r"Confocal measurement $[\mu m]$", fontsize=25)
#     ax.grid(True)
#     ax.set_title(r"Confocal Measurements on Short Strip Stave X Direction At $25mms^{-1}$", fontsize=25)
#     #ax.legend(loc="best")
#     plt.yticks(fontsize=25)
#     plt.xticks(fontsize=25)
#     plt.tight_layout()
#     
#     if "J" in side:
#         plt.savefig(out_file + "SideJ_MountedStave_All_Flat_Plot_2.0.png")
#     else:
#         plt.savefig(out_file + "SideL_MountedStave_All_Flat_Plot_2.0.png")
#     
#     
#     plt.show()
# 
# 
# #==============================================================================
# #==============================================================================
# #==============================================================================
# 
# if middle_line:
#     with open(out_file + "Side_L_middle_line_2.0.csv", 'w') as file:
#             for j in range(len(data_combined[24])):
#                 writer = csv.writer(file)
#                 writer.writerow(data_combined[24][j])
# =============================================================================
