"""
28 August 2023 
Abraham Tishelman-Charny

The purpose of this plotter is to display the 3D X,Y,Z of a module from a stave confocal measurement.

Example usage: 
- Alter input file name in this file, alter selections as desired, then run:
python3 PlotModuleShape.py 
"""

# Imports
import pandas as pd 
import numpy as np
from PlotMetrology import Make3DPlot, SaveCSV

f = "csv/Side_L_SingleDocReadout.csv" # input file 
Label = "Module_0" # label for naming outputs

# get the data
data = pd.read_csv(f)

# make selections by eye
mask = np.logical_or(data['X'] > 670, data['X'] < 710)
mask = np.logical_and(mask, data['X'] > 630)
mask = np.logical_and(mask, data['X'] < 720)
mask = np.logical_and(mask, data['Z'] < 540)
data = data[mask]

# make the plot
Make3DPlot(data, Label)

# save an output csv file which contains the data after selections are applied
out_file = "csv/%s.csv"%(Label)
X, Y, Z = np.array(data['X']), np.array(data['Y']), np.array(data['Z'])
data = ["%s,%s,%s"%(X[i], Y[i], Z[i]) for i in range(0, len(Z))]
SaveCSV(out_file, data)