import ConfigParser as configparser

import LV
import HV
import Chiller
import Interlock
from Sensors import *

import DCSTools as fm
import os

path=os.getcwd()
print(path)
def DCSConfig():
    parser = configparser.ConfigParser()
    parser.read(path+"/DCS//DCSConfig.ini")
    sections=parser.sections()
    dcs_dict={}
    
    for section in sections:
        print("parsing section "+str(section))
        if('HighVoltage' in section):
            HV_type=parser.get('HighVoltage','Type')
            HV_location=parser.get('HighVoltage','Location')
            dcs_dict['HV']=fm.init_class("HV",HV_type,HV_location)
        if('LowVoltage' in section):            
            LV_type=parser.get('LowVoltage','Type')
            LV_location=parser.get('LowVoltage','Location')
            print("setting low voltage to dictionary")
            dcs_dict['LV']=fm.init_class("LV",LV_type,LV_location)
        if('Chiller' in section):
            Chiller_type=parser.get('Chiller','Type')
            Chiller_location=parser.get('Chiller','Location')
            dcs_dict['Chiller']=fm.init_class("Chiller",Chiller_type,Chiller_location)
            
        if('Interlock' in section):
            interlock_location=parser.get('Interlock','location')
            interlock_baud=parser.get('Interlock','baud')
            interlock=Interlock.Cambridge(interlock_location,interlock_baud)    
    return dcs_dict 

def MasterConfig():
    parser = configparser.ConfigParser()
    parser.read(path+"/DCS/DCSConfig.ini")
    print(path+"/DCSConfig.ini")
    print(parser.sections())
    master_server=str(parser.get('DAQServer','location'))
    grafana_server=str(parser.get('DAQServer','grafana'))
    return(master_server,grafana_server)

def IVCurveConfig():
    parser=configparser.ConfigParser()
    parser.read(path+"/DCS/DCSConfig.ini")
    step=int(str(parser.get('IVCurve','step')))
    start=int(str(parser.get('IVCurve','start')))
    end=int(str(parser.get('IVCurve','stop')))
    return (step,end,start)

def SensorConfig():
    parser = configparser.ConfigParser()
    parser.read(path+"/DCS/SensorConfig.ini")
    sensor_package=fm.package_contents("Sensors")
    sections=parser.sections()
    sensors={}
    for section in sections:
        for sensor_name in sensor_package:
            if sensor_name in section:
                args=parser._sections[section]
                print(section)
                print(args)
                sensor_type=fm.init_class_with_dict("Sensors",sensor_name,args)
                sensors[section]=sensor_type
           
    return sensors

dcs_dict=DCSConfig()
#print(dcs_dict)
SensorDict=SensorConfig()
master,grafana=MasterConfig()

