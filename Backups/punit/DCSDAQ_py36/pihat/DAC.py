import smbus
import time

class dac:
    def __init__(self,vref=1.5,bus=1,address=0x0c):
        self.bus=smbus.SMBus(bus)
        self.address=address
        self.vref=vref

	# Channel Mapping / Commands
        # DACA -> J8 pin 1  -> 0x18
        # DACB -> J8 pin 2  -> 0x19
        # BOTH ->           -> 0x1F

        self.cmd=[0x18,0x19,0x1f]

	dac_bits=[]
	dac_bits.append(0)

	if vref == 5.0:
		print("Initialising DAC for internal Vref ",vref,"V")
		dac_bits.append(1)
	else:
		print("Initialising DAC for external Vref ",vref,"V")
		dac_bits.append(0)

	# ref figure 66, page 27 of AD5627R datasheet
       	self.bus.write_i2c_block_data(self.address,0x38,dac_bits)

    def write_raw(self,channel,dac_value):
	dac_bits=[]
	
	dac_bits.append( ( int(dac_value) & 0x00ff) >> 0)
        dac_bits.append( ( int(dac_value) & 0xff00) >> 8)

	# ref figure 61, page 24 of AD5627R datasheet
       	self.bus.write_i2c_block_data(self.address,self.cmd[channel],dac_bits)
        return

    def write_voltage(self,channel,dac_voltage):
	#  Vout = Vref x bits/4095

	dac_value = 4095 * dac_voltage / self.vref

	print("Channel ",channel," set to ",int(dac_value)," for ",dac_voltage,"V")

        self.write_raw(channel,dac_value)
	return
