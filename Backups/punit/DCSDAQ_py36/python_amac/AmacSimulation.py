#simulate values in  amac registers
import numpy
temperature =10 
left_current= .1203 
right_current = .850 


def temp_registers(temperature):
    adc_value=int((temperature*1000+25)/12.5)
    value1=adc_value& 0xff
    value2=adc_value& 0x300
    value2 =value2 >> 8
    
    return (value1,value2)

def right_current_registers(right_current):
    adc_value=int(right_current*33400664.452/1E6+106)
    value1=adc_value & 0xf
    value1=value1 << 4

    value2=adc_value & 0x3f0
    value2 =value2 >> 4
    return (value1,value2)
    
def left_current_registers(left_current):
    adc_value=int((left_current*33400664.452/1E6)+106)
    print("adc value",adc_value)
    value1=adc_value & 0xf
    value1=value1 << 4

    value2=adc_value & 0x3f0
    value2 =value2 >>4
    return (value1,value2)

class simulated_i2c:
    def __init__(self):
        left_current= .1203 
        right_current = .850 
        register36,register37=right_current_registers(right_current)
        register27,register28=left_current_registers(left_current)

        self.amac_registers=numpy.zeros(40,dtype=numpy.uint8)
        self.amac_registers[27]=register27
        print("actual register 27 value",register27)
        print("actual register  28 value",register28)
        self.amac_registers[28]=register28
        self.amac_registers[36]=register36
        self.amac_registers[37]=register37
        
    def read(self,register):
        return self.amac_registers[register]
        
    def write(self,register,data):
        self.amac_registers[register]=data

if __name__=="__main__":
    register36,register37=right_current_registers(right_current)
    register27,register28=left_current_registers(left_current)

    amac_registers=numpy.zeros(40,dtype=numpy.uint8)
    amac_registers[27]=register27
    amac_registers[28]=register28
    amac_registers[36]=register36
    amac_registers[37]=register37
    print(amac_registers)
