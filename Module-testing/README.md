# BNL-Module-Testing

The purpose of this directory is to keep track of files used for ITk strip module testing at BNL.

For starting the coldbox GUI, ssh into the raspberry pi, navigate to the coldbox gui directory and run the following commands:

```
pipenv shell
source setenv.sh
./run.sh
```

NOTE: If gui will not open, make sure pi eth0 IP is properly set with the command (for the coldbox closest to the testing clean room entrance):

```
sudo ifconfig eth0 169.254.115.144
```

A link that was useful for a beginner to figure out how to SSH into the pi: [https://itsfoss.com/ssh-into-raspberry/](https://itsfoss.com/ssh-into-raspberry/)

## Module testing notes 

- Need to get config file based on Hybrid serial number using getAsicConfig.py script (upload to GitLab repo). This script queries the ITk database to get the config file for a given Hybdrid.

/home/qcbox/Desktop/stars/latest-2022/itsdaq-sw/DATA/config/
/home/qcbox/Desktop/stars/latest-2022/itsdaq-sw/DATA/config/st_system_config.dat # config file for the coldbox

LV at 11V

To run ITSDAQ:
source RUNITSDAQ.sh 

https://docs.google.com/document/d/1rz9wb2HsCYoP0CRJNnf46j8Z5hdrdXrfZz31Rlb0AaQ/edit?usp=sharing

.L AMAC_common.cpp 
.L AMACStar.cpp

bondID is always 0 

example:

AMACStar_initialiseAMACs({0},{0})
AMACStar_configureAMACs(1)
.q # Q: Is there another command that can be run?
restarting ITSDAQ increases current because the components are being configured 

usually switch on HV here

at T = 20C, ran Full test 

FullTest includes:
- Pedestal scan 
- Noise trim # looking for noisy channels 
- Strobe delay # something with timing?
- 3 point gain # test charge 
- Any others?  

FullTest finished when see something like HandleMenu completed for id 48 in 522.2 seconds 

Results folder: /home/qcbox/Desktop/stars/latest-2022/itsdaq-sw/DATA/ps/ and /results/ 

## Raspberry Pi Notes 

Note: One interesting thing about the raspberry pi at coldbox station 2 has an incorrect time when checking for US eastern. It is off not just by one hour, but by about one hour and 10-ish minutes. This can present problems when uploading data to grafana as it may end up minutes out of date. To set the date on a raspberry pi by hand:

```
sudo date -s '2022-11-21 11:34:33'
```

The above will set the system date to 21 November 2022, 11:34am and 33 seconds. 

## Setup ITSDAQ software 

git clone https://gitlab.cern.ch/atlas-itk-strips-daq/itsdaq-sw.git
cd 
itsdaq-sw/
ls
mkdir DATA
cd DATA/
mkdir ps etc log logs results config data
cd ..
python waf configure
python waf build
python3 waf build
python3 waf configure
python3 waf build
python waf install

## Functional tests (run in ITSDAQ)
pedestalScan(6,15,100,50)
ABCStarNoiseTrimPlot(e->runnum, e->scannum, 6,15)
ABCStarStrobeDelay(0,0.57,256,32,1,50)
ABCStarThreePointGain(1.0,256,1020,1,50) # ThreePointGain 

## ITSDAQ-SW 
have some functions defined in macros 
https://gitlab.cern.ch/atlas-itk-strips-daq/itsdaq-sw/-/blob/master/macros/pedestal.cpp

## Frequently Asked Questions 
- What is a three point gain? 
- What is a strobe delay?
- What is a noise trim plot?
- What does "change the shunt" mean? 
- Why do we need to restart ITSDAQ after the initial initialization of the AMAC? Can that be placed into a function without having to restart? 
- What are all of the PPB phases and subphases? How many staves per phase / subphase?
- If the BurstData and ScanData windows close while ITSDAQ is running, do you need to restart ITSDAQ?
- What is a strobe delay?
- From are above list are we missing anything that is included in the FullTest 
- In examples RC curves, what is the quantity in the top plot? And how does it affect the bottom plot? 
- Does the ITk production database contain both pixel and strips information?
- Which two components does the HV tab connect?  

## Misc notes

32 bits 0x0000000
binary: 0000 0000 0000 0000 0000 0000 0000 0000 0000

and then AMACStar_readNTC(-1,A) where A is what you want to read. 0 for X hybrid, 1 for Y hybrid and 2 for PB

// Initialize and configure with DCDCs off.
AMACStar_initialiseAMACs({0,0,0,0},{0,1,2,3} // Do we need to initialize again even if we've already initialized earlier? 
AMACStar_configureAMACs(0) // DCDCs off.

// Calibrate
loadmacro("AMACStar_0514") // Why does this have to be done again? 
AMACStar_0514_calScan(1)
AMACStar_internalCalibrations()

// Read PTAT.  Should agree with NTCpb.
AMACStar_readPTAT(-1)

// Enable DCDCs
AMACStar_DCDC(-1,1)