# Module noise analysis

The purpose of this directory is to analyze noise from ITk strips modules. 

## Unshunted / shunted comparison

To produce ratio plots comparing noise results between shunted and unshunted cold tests, the rough instructions are as follows:

* Edit `Make_TC_Plot.py`, specifically the `cycle_dict_all` dictionary to contain the Run and Scan numbers for a given module's results, including the scan numbers for the unshunted and shunted tests 
* Make sure the output txt files are contained in the proper location, somewhere accesible by the plotter. E.g. `/eos/user/a/atishelm/www/ITk/PPB2/TC/All/{local_ID}_RC_{RunNumber}_{UnshuntedScanNum}.txt` as used in the example.
* Run the plotter with `python3 Make_TC_Plot.py`. If it works properly, this should output ratio plots and histograms to your specified output directory.

