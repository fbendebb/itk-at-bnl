"""
28 December 2022
Abraham Tishelman-Charny

The purpose of this module is to define parameters for Module-Analysis.py
"""

# parameters 
hist_color = 'C0'

# if you want to compare modules all in the same direc
modules = [
    #"SN20USBHX2001170"
    #"SCIPP-PPB_LS-012_box2",
    #"SCIPP-PPB_LS-012_box3",
    # "BNL-PPB2-MLS-112",
    # "BNL-PPB2-MLS-113",
    # "BNL-PPB2-MLS-114",

    # Death by thermal cycle
    #"BNL-PPB2-MLS-210"
    #"BNL-PPB2-MLS-211"
    #"SCIPP-PPB_LS-016"
    #"SCIPP-PPB_LS-019"
]
quantities = [
    #"gain", 
    #"vt50", 
    "innse" # innse = input noise 
]

# quantities = [
#     "Current", "Voltage" 
# ]

quantity_dict = {
    "gain" : "Gain",
    "vt50" : "vt50", 
    "innse" : "Input_noise",
    "Current" : "Current",
    "Voltage" : "Voltage"
}

# format (for ITSDAQ output): "%s/%s_%s_%s_%s.txt"%(inDir, module, RESULT_TYPE, RUN_NUMBER, SCAN_NUMBER)
# SN20USBHX2001147_RC_1135_966.txt

# 94, 25 was first cold test of TC to death
# 138, 166 was final

RUN_NUMBER = "94"
SCAN_NUMBER = "25"
RESULT_TYPE = "RC"

#RESULT_TYPE = "IandV"
#inDir = "/eos/user/a/atishelm/www/ITk/Thermal-Cycling/ThreePointGain/FullTest/results/"
#ol = "/eos/user/a/atishelm/www/ITk/Thermal-Cycling/ThreePointGain/FullTest/" # SN20USBHX2001147_RC_1135_966.txt
