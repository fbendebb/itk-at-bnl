"""
3 July 2023 
Abraham Tishelman-Charny

The purpose of this module is to create a shunted/unshunted ratio plot for each cycle in a module's TC.
"""

import os

# just need:
# module, Run numbers, scan numbers

local_ID = "BNL-PPB2-MLS-232"
flattened_local_ID = local_ID.replace("-", "_")

# modules which were cycled together will share the run and scan numbers
cycle_dict_all = {

    # "1" : ["229", "6", "22"], # Cycle number : [Run number, unshunted scan number, shunted scan number] 
    # "2" : ["229", "54", "70"], 
    # "3" : ["229", "102", "118"], 
    # "4" : ["229", "150", "166"], 
    # "5" : ["229", "198", "214"], 

    # "6" : ["232", "6", "22"], # Cycle number : [Run number, unshunted scan number, shunted scan number] 
    # "7" : ["232", "54", "70"], 
    # "8" : ["232", "102", "118"], 
    # "9" : ["232", "150", "166"], 
    # "10" : ["232", "198", "214"], 

    "BNL-PPB2-MLS-229" : {
        "1" : ["96", "6", "22"], # Cycle number : [Run number, unshunted scan number, shunted scan number] 
        "2" : ["96", "54", "70"], 
        "3" : ["96", "102", "118"], 
        "4" : ["96", "150", "166"], 
        "5" : ["96", "198", "214"], 
    },

    "BNL-PPB2-MLS-230" : {
        "1" : ["96", "6", "22"], # Cycle number : [Run number, unshunted scan number, shunted scan number] 
        "2" : ["96", "54", "70"], 
        "3" : ["96", "102", "118"], 
        "4" : ["96", "150", "166"], 
        "5" : ["96", "198", "214"], 
    },

    "BNL-PPB2-MLS-231" : {
        "1" : ["96", "6", "22"], # Cycle number : [Run number, unshunted scan number, shunted scan number] 
        "2" : ["96", "54", "70"], 
        "3" : ["96", "102", "118"], 
        "4" : ["96", "150", "166"], 
        "5" : ["96", "198", "214"], 
    },

    "BNL-PPB2-MLS-232" : {
        "1" : ["96", "6", "22"], # Cycle number : [Run number, unshunted scan number, shunted scan number] 
        "2" : ["96", "54", "70"], 
        "3" : ["96", "102", "118"], 
        "4" : ["96", "150", "166"], 
        "5" : ["96", "198", "214"], 
    },
}

cycle_dict = cycle_dict_all[local_ID]

for cycle_i in cycle_dict.keys():

    cycle_info = cycle_dict[cycle_i]
    RunNumber = cycle_info[0]
    UnshuntedScanNum = cycle_info[1]
    ShuntedScanNum = cycle_info[2]

    cmd = 'python3 Module-Analysis.py --plotRatio --ratioFiles \
            "/eos/user/a/atishelm/www/ITk/PPB2/TC/All/{local_ID}_RC_{RunNumber}_{UnshuntedScanNum}.txt,/eos/user/a/atishelm/www/ITk/PPB2/TC/All/{local_ID}_RC_{RunNumber}_{ShuntedScanNum}.txt" \
            --perSide --ratioFilesLabels "{flattened_local_ID}_WithoutShunt,{flattened_local_ID}_WithShunt,{flattened_local_ID}_Comparison_Cycle_{cycle_i}" --dataFromITSDAQ \
            --ol /eos/user/a/atishelm/www/ITk/PPB2/TC/ShuntingComparison/ --makeSeriesPlot --makeHist'.format(
                RunNumber=RunNumber,
                UnshuntedScanNum=UnshuntedScanNum,
                ShuntedScanNum=ShuntedScanNum,
                cycle_i=cycle_i,
                local_ID=local_ID,
                flattened_local_ID=flattened_local_ID
            )
    print("$",cmd)
    os.system(cmd)