10a11,12
> //   The ITK HV supply is also supported (2022)  
> //
15,16c17
< #include "../stlib/st_error.cpp"
< //#include "drivers\Ke24xx.h"
---
> #include "../stlib/st_error.h"
25d25
< #include "../worker/worker.cpp"
28d27
< #include <typeinfo>
41a41,50
> std::string TkHV::deviceTypeName(DeviceType dt) {
>   // using TkHV::DeviceType;
>   switch(dt) {
>   case DeviceType::SCPI_COMPLIANT: return "SCPI";
>   case DeviceType::LEGACY: return "LEGACY";
>   case DeviceType::ITKHV: return "ITKHV";
>   default: return "UNKNOWN";
>   }
> }
> 
182,200d190
<       
<     //Need to read output twice for ISEG SHR serial connections
<     if (!sendCommand("*IDN?\n", where)) return;
<     status = viRead(data, 1024, &job); 
<     status = viRead(data, 1024, &job);
<     
<     if (CheckError("TkHV::TkHV viRead *IDN?", status)) return;
<     //ISEG Information. Remove this if not using the ISEG SHR
<     iModel = 1234;
<     strcpy(sModel, "iseg");
<     iMax = 6000; // uA
<     vMax = 1000; // V
<     nChannels = atoi(label);
<       
<     std::cout << "These are the number of nChannels: " << nChannels << std::endl;
<       
<     break;
<       
<     //if (!sendCommands({"*RST\n", "*IDN?\n"}, where)) return;
202c192,194
<     //status = viRead(data, 1024, &job);
---
>     if (!sendCommands({"*CLS\n", "*IDN?\n"}, where)) return;
> 
>     status = viRead(data, 1024, &job);
225a218,256
>   case DeviceType::ITKHV: // ITKHV supply 
>       viSetAttribute(VI_ATTR_ASRL_RTS_STATE, 1);
>       viSetAttribute(VI_ATTR_ASRL_DTR_STATE, 1);
>       viSetAttribute(VI_ATTR_ASRL_BAUD, 115200);
>       // viSetAttribute(VI_ATTR_ASRL_FLOW_CNTRL, VI_ASRL_FLOW_NONE);
>       // viSetAttribute(VI_ATTR_TERMCHAR_EN, 1);
>       iModel = ITKHVModel;
>       strcpy(sModel, "ITKHV");
>       iMax = 19999; // uA (actual current sent to ITKHV multiplied by 1000 to account for ITKHV firmware)
>       vMax = 707; // V
>       FlushDeviceBuffer();
> 
>       if(true){ // I want to define some variables inside the switch statement
> 
>         // Toggle the expert mode
>         sprintf(stringinput, "X\n");
>         status = viWrite(reinterpret_cast<ViBuf>(stringinput), strlen(stringinput), &rcount);
> 
>         // It may be that expert mode was already on, and by toggling it is now off. 
>         // If that is the case we need to turn it ON
>         ViStatus read_status = viRead(data, 1024, &job);
>         std::cout << reinterpret_cast<char*>(data) << std::endl;
>         std::cout << "ITKHV read status: " << read_status << std::endl;
> 
>         // "data" should be either:
>         // " expert mode = OFF\n" or " expert mode =  ON\n"
>         if(strncmp(reinterpret_cast<char*>(data), " expert mode = OFF", 18) == 0){
>           std::cout << "ITKHV expert mode is OFF. Toggling ON" << std::endl;
>           status = viWrite(reinterpret_cast<ViBuf>(stringinput), strlen(stringinput), &rcount);
> 
>           // check the status again to make sure
>           read_status = viRead(data, 1024, &job);
>           std::cout << reinterpret_cast<char*>(data) << std::endl;
>           std::cout << "ITKHV read status: " << read_status << std::endl;
>         }
> 
>       }
>       break;
> 
257,276d287
<   case DeviceType::ITKHV:
<       viSetAttribute(VI_ATTR_ASRL_RTS_STATE, 1);
<       viSetAttribute(VI_ATTR_ASRL_DTR_STATE, 1);
<       viSetAttribute(VI_ATTR_ASRL_BAUD, 115200);
<       // viSetAttribute(VI_ATTR_ASRL_FLOW_CNTRL, VI_ASRL_FLOW_NONE);
<       // viSetAttribute(VI_ATTR_TERMCHAR_EN, 1);
<       iModel = ITKHVModel;
<       strcpy(sModel, "ITKHV");
<       iMax = 19999; // uA
<       vMax = 707; // V
<       FlushDeviceBuffer();
<       // Enable expert mode
<       sprintf(stringinput, "X\n");
<       do {
<         status = viWrite(reinterpret_cast<ViBuf>(stringinput), strlen(stringinput), &rcount);
<         if (CheckError("TkHV: viWrite X\\n", status)
<          || CheckError("TkHV: viRead", viRead(data, 1024, &job))) return;
<       }
<       while (strncmp(reinterpret_cast<char*>(data), " expert mode =  ON", 18) != 0);
<       break;
281a293,298
>   if(strcmp(sModel, "UNKNOWN") == 0) {
>     // Not valid
>     std::cout << "TkHV::TkHV not found at resource "<< m_resource << "\n";
>     return;
>   }
> 
319c336
< TGraph* TkHV::GetIVGraph(int channel){
---
> TGraph* TkHV::GetIVGraph(){
321,322c338
<     std::cout << "xPoints: " << xPoints[channel] << " yPoints: " << yPoints[channel] << " channel: " << channel << std::endl;
<     ivGraph = new TGraph(nPoints,xPoints[channel],yPoints[channel]);
---
>     ivGraph = new TGraph(nPoints,xPoints,yPoints);
326,327d341
<   std::cout << "xPoints: " << xPoints[channel] << " yPoints: " << yPoints[channel] << " channel: " << channel << std::endl;
<   ivGraph = new TGraph(nPoints,xPoints[channel],yPoints[channel]);
346a361
> 
348,349d362
<   //std::cout << "this is the command: " << command << std::endl;
<   //std::cout << "This is where it is: " << where << std::endl;
354d366
< 
409,429d420
<         case 1234:
<           //Set all channel voltages to zero
< 	  for(int channel = 0; channel < nChannels; channel ++) {
< 	    sprintf(stringinput,":VOLT 0,(@%d)\r\n",channel);
< 	    std::cout << "setting all channels to zero: " << stringinput << std::endl;
< 	    if (!sendCommand(stringinput, where)) return false;
< 	    status = viRead(data, 1024, &job);
< 	    status = viRead(data, 1024, &job);
< 	    
< 	    if(CheckError("TkHV::Mon viRead (:MEAS:VOLT)", status)) return false;
<           
< 	    //Set polarity to negative
< 	    sprintf(stringinput,":CONF:OUTPUT:POL n,(@%d)\r\n",channel);
< 	    if (!sendCommand(stringinput, where)) return false;
< 	    status = viRead(data, 1024, &job);
< 	    status = viRead(data, 1024, &job);
< 	    if(CheckError("TkHV::Mon viRead (:CONF:POL)", status)) return false;
< 	  }
<           
<           break;
<           
716,717d706
<       
<       
721,731c710,711
<         for(int channel = 0; channel < nChannels; channel++){
<           std::cout << "isegVMon: " << isegVMon[channel] << " isegIMon: " << isegIMon[channel] << std::endl;
<           
<           xPoints[channel][nPoints] = isegVMon[channel];
<           yPoints[channel][nPoints] = isegIMon[channel];
<           
<           std::cout << "isegVMon xpoints: " << isegVMon[channel] << " isegIMon ypoints: " << isegIMon[channel] << std::endl;
<           
< //          xPoints[nPoints]=vMon;
< //          yPoints[nPoints]=iMon;
<         }
---
>         xPoints[nPoints]=vMon;
>         yPoints[nPoints]=iMon;
742c722,723
<       iLimitTemp = std::max(CalcPBCompliance(vEnd,iLimit,nPB),CalcPBCompliance(vStop,iLimit,nPB));
---
>       iLimitTemp = std::max(CalcPBCompliance(vEnd,iLimit,nPB),
>                             CalcPBCompliance(vStop,iLimit,nPB));
754,760c735,738
<   for(int channel = 0; channel < nChannels; channel++){
<     std::cout << "channel: " << channel << " xpoints: " << xPoints[channel] << " ypoints: " << yPoints[channel] << std::endl;
<     ivGraph = new TGraph(nPoints,xPoints[channel],yPoints[channel]);
<     sprintf(stringinput, "IV Data from %s at resource %s", sModel, m_resource);
<     ivGraph->SetTitle(stringinput);
<     ivGraph->Print();
<   }
---
>   ivGraph = new TGraph(nPoints,xPoints,yPoints);
>   sprintf(stringinput, "IV Data from %s at resource %s", sModel, m_resource);
>   ivGraph->SetTitle(stringinput);
>   ivGraph->Print();
827c805
<       fprintf(outfile, "%3.1f\t%1.2f\n",xPoints[0][i],yPoints[0][i]);
---
>       fprintf(outfile, "%3.1f\t%1.2f\n",xPoints[i],yPoints[i]);
900,936d877
<     case 1234:
<       
<       for(int channel = 0; channel < nChannels; channel++){
<         
<         sprintf(stringinput,":MEAS:VOLT? (@%d)\r\n",channel);
<       
<         if (!sendCommand(stringinput, where)) return false;
<         status = viRead(data, 1024, &job);
< 	status = viRead(data, 1024, &job);
<         
< 	
<         if(CheckError("TkHV::Mon viRead (:MEAS:VOLT)", status)) return false;
<         
<         token = (char*)data;
<         token[strlen(token) - 1] = '\0';
<         isegVMon[channel] = atof((char*) token);
<         
< //        vMon = atof((char*) token);
<         
<         sprintf(stringinput,":MEAS:CURR? (@%d)\r\n",channel);
<         if (!sendCommand(stringinput, where)) return false;
<         status = viRead(data, 1024, &job);
<         status = viRead(data, 1024, &job);
<         
<         if(CheckError("TkHV::Mon viRead (:MEAS:CURR)", status)) return false;
<         
<         token = (char*)data;
<         token[strlen(token) - 1] = '\0';
<         isegIMon[channel] = atof((char*) token)*1000000;
< //        iMon = atof((char*) token)*1000000;
<         
<         std::cout << "ISEG channel " << channel << " has voltage: " << isegVMon[channel] << " and current: " << isegIMon[channel] << std::endl;
<         
<       }
<       
<       break;
<       
983c924
<       break;
---
>       break;      
987c928,929
<       return readNum("v\n", vMon, -1) && readNum("i\n", iMon, 1000000);
---
>       // Update 29/06/2022, current now read in uA we think (scale in readNum set to 1) 
>       return readNum("v\n", vMon, -1) && readNum("i\n", iMon, 1);
1004,1006d945
<       case 1234:
<         //std::cout << "No filter set for iseg" << std::endl;
<         break;
1110a1050,1054
> // Get (software) safety limit
> Float_t TkHV::GetLimit(){
>   return vSafety;
> }
> 
1115,1117d1058
<     case 1234:
<       //std::cout << "No set terminal set for iseg" << std::endl;
<       break;
1144c1085,1090
<   *Ustatus = 0;
---
>   if(HitCompliance()) {
>     *Ustatus = 1;
>   } else {
>     *Ustatus = 0;
>   }
> 
1155,1156d1100
<   ViStatus status = VI_SUCCESS;
<   
1174,1187d1117
<       
<     case 1234:
<       
<       for(int channel = 0; channel < nChannels; channel++){
<         
<         sprintf(stringinput,":VOLT OFF,(@%d)\r\n",channel);
<         
<         if(!sendCommand(stringinput, "TkHV::Off")) return false;
<         status = viRead(data, 1024, &job);
<         status = viRead(data, 1024, &job);
<         
<         state = DeviceState::OFF;
<       }
<       break;
1220,1223c1150,1152
<       //std::cout << "This is the unvoided stringinput (off): " << stringinput << std::endl;
<       //if (!sendCommand(stringinput, "TkHV::Off")) {
<       //  return false;
<       //}
---
>       if (!sendCommand(stringinput, "TkHV::Off")) {
>         return false;
>       }
1236,1237d1164
<   
<   ViStatus status = VI_SUCCESS;
1240,1251c1167
<     case 1234:
<       
<       for(int channel = 0; channel < nChannels; channel++){
<         sprintf(stringinput,":VOLT ON,(@%d)\r\n",channel);
<         if (!sendCommand(stringinput, "TkHV::On")) return false;
<         
< 	status = viRead(data, 1024, &job);
<         status = viRead(data, 1024, &job);
<         
<         state = DeviceState::ON;
<       }
<       break;
---
>     case 236:
1294,1296c1210
< //       std::cout << "in the default" << std::endl;
<       sprintf(stringinput,"NOTPROGRAMMED");
<       //std::cout << "here is the default stringinput: " << stringinput << std::endl;
---
>       sprintf(stringinput,"VOID");
1300,1310c1214,1223
<    if(0!=strncmp(stringinput,"NOTPROGRAMMED",13)){
<        //std::cout << "this is the unvoided stringinput (on): " << stringinput << std::endl;
<        //if (!sendCommand(stringinput, "TkHV::On_VOID")) {
<        //    return false;
<        //}
<        state = DeviceState::ON;
<  
<        return true;
<    }else{
<        std::cout << "TkHV::On not yet implemented for " << sModel << std::endl;
<    }
---
>   if(0!=strncmp(stringinput,"VOID",4)){
>       if (!sendCommand(stringinput, "TkHV::On")) {
>           return false;
>       }
>       state = DeviceState::ON;
> 
>       return true;
>   }else{
>       std::cout << "TkHV::On not yet implemented for " << sModel << std::endl;
>   }
1329c1242,1248
<   std::cout << "  Set current " << iSet << "uA" << std::endl;
---
>   if(iModel == ITKHVModel){
>   // Note, for ITKHV, need to divide return values by 1000 
>     std::cout << "  Set current " << iSet / 1000 << "uA" << std::endl;
>   }
>   else{
>     std::cout << "  Set current " << iSet << "uA" << std::endl;
>   }
1414c1333
<   hd = work_scheduleJob([&](void*){this->IVScanImpl(vStop, iLimit, rampRate); }, NULL);
---
>   hd = work_scheduleJob([&](void*){this->RampImpl(vStop, iLimit, rampRate); }, NULL);
1420c1339,1347
<      * REAL_RAMP_RATES corresponds to the switch in the code for handling other supplies. */
---
>      * REAL_RAMP_RATES corresponds to the switch in the code for handling other supplies.
>      *
>      * Relevant commands for ITKHV are:
>      *  U write the ramp Up rate in Volts per second
>      *  D write the ramp Down rate in Volts per second
>      *
>      * */
> 
> 
1434,1436d1360
<     //stefania
<     std::cout << "what are we doing in this lifeeeeeee " << vStop << std::endl;
<     //gSystem->Sleep(1000);
1609c1533
< Bool_t TkHV::ReadLabel(char* label){
---
> Bool_t TkHV::ReadLabel(char* label) const {
1615a1540,1552
> std::string TkHV::GetModel() const {
>   return sModel;
> }
> 
> std::string TkHV::GetResourceString() const {
>   return m_resource;
> }
> 
> std::string TkHV::GetSerialNumber() const {
>   // Not read
>   return "";
> }
> 
1635,1672c1572
<     case 1234:
<       
<       for(int channel = 0; channel < nChannels; channel++){
<         
<         sprintf(stringinput,":READ:VOLT:ON? (@%d)\r\n",channel);
<         if (!sendCommand(stringinput, where)) return false;
<         status = viRead(data, 1024, &job);
< 	status = viRead(data, 1024, &job);
<         
<         token = (char*)data;
<         //Change this to off and add an on conditional
<         if(atoi(token) == 0) {
<           state = DeviceState::OFF;
<         }
<         if(atoi(token) == 1) {
<           state = DeviceState::ON;
<         }
<         
<         sprintf(stringinput,":MEAS:VOLT? (@%d)\r\n",channel);
<         if (!sendCommand(stringinput, where)) return false;
<         status = viRead(data, 1024, &job);
< 	status = viRead(data, 1024, &job);
<         
<         token = (char*)data;
<         token[strlen(token) - 1] = '\0';
<         vSet = atof(token);
<         
<         sprintf(stringinput,":MEAS:CURR? (@%d)\r\n",channel);
<         if (!sendCommand(stringinput, where)) return false;
<         status = viRead(data, 1024, &job);
<         status = viRead(data, 1024, &job);
<         
<         token = (char*)data;
<         token[strlen(token) - 1] = '\0';
<         iSet = atof(token)*1000000; //Convert to uA
<       }
<       
<       break;
---
>     case 236:
1915,1917d1814
<     case 1234:
<       //std::cout << "No compliance implemented for iseg" << std::endl;
<       break;
1982,1983c1879,1882
<     if(current_uA<100){
<       std::cout << "TkHV::Set request rejected: " << current_uA << " below minimum level of 100 uA" << std::endl;
---
>     int itkhv_current_minimum_uA = 100;
>     if(current_uA < itkhv_current_minimum_uA){
>       std::cout << "TkHV::Set request rejected: " << current_uA << " below minimum level of " << itkhv_current_minimum_uA << " uA" << std::endl;
>       std::cout << "TkHV::Set try setting the current limit to " << itkhv_current_minimum_uA << ", the ITK HV supply is not accurate below this value" << std::endl;
2008,2009d1906
<   
<   //std::cout << "This is the model: " << iModel << std::endl;
2012d1908
<   ViStatus status = VI_SUCCESS;
2015,2017d1910
<     
<     
<     
2019a1913
>     case 236:
2062,2092d1955
<     case 1234:
<       
<       //Setting voltage
<       for(int channel = 0; channel < nChannels; channel++){
<         sprintf(stringinput,":VOLT %.1f,(@%d)\r\n",voltage,channel);
<         if (!sendCommand(stringinput, where)) return false;
<         
<         status = viRead(data, 1024, &job);
< 	status = viRead(data, 1024, &job);
< 	
< 	sprintf(stringinput,":CURR:BOUNDS %fE-6,(@%d)\r\n",current_uA,channel);
< 	std::cout<<stringinput<<std::endl;
<    	if (!sendCommand(stringinput, where)) return false;
<    	status = viRead(data, 1024, &job);
<    	status = viRead(data, 1024, &job);
< 	sprintf(stringinput,":READ:CURR:BOUNDS? (@%d)\r\n",channel);
< 	if (!sendCommand(stringinput,where)) return false;
< 	status = viRead(data,1024, &job);
<    	status = viRead(data, 1024, &job);
< 	Char_t *token;
< 	token = (char*)data;
< 	std::cout<<token<<std::endl;
< 
<         vSet = voltage;
<         iSet = current_uA;
<       }
<       
<       usleep(200000);
<       
<       break;
<       
2150a2014,2020
>       /* Set the output voltage for the ITKHV supply.
>        * Relevant commands:
>        * S:  Writes a new value to VSET. This value cannot be greater than VMAX. If the output is enabled the output volts will immediately ramp to the new value.
>        *
>        * Formatting of commands is e.g: "S200\n" ... will ramp to 200V 
>        */
> 
2158a2029,2030
>       current_uA *= 1000; // This is important: Units conversion needed for the ITKHV firmware
> 
2170c2042
<         while (Mon(), std::abs(vMon) > 1) gSystem->Sleep(1000);
---
>         while (Mon(), std::abs(vMon) > 1) gSystem->Sleep(100);
2189,2190c2061
<   ViStatus status = VI_SUCCESS;
<   
---
> 
2192,2196c2063
<     case 1234:
<       //std::cout << "no delay set for iseg" << std::endl;
<       break;
<       
<     case 2410:
---
>   case 2410:
2219,2233d2085
< Bool_t TkHV::ThreadComplete(){
<   int status = work_isJobComplete(hd);
<   switch (status){
<     case JOB_REGISTERED:
<     case JOB_RUNNING:
<       return false;
<       break;
<     case JOB_COMPLETED:
<     default:
<       return true;
<       break;
<   }
< 
< }
< 
2250,2252d2101
<     case 1234:
<       //std::cout << "No need to clear errors for iseg" << std::endl;
<       break;
2337,2347c2186
<   //if (!sendCommand("\n", "TkHV: FlushDeviceBuffer")) return;
<   for(int i = 0; i < 3; i++) {
<     //if (!sendCommand("\n", "TkHV: FlushDeviceBuffer")) return;
<     // Keep reading lines from the buffer until it is empty
<     ViStatus status = viSetAttribute(VI_ATTR_TMO_VALUE, timeout);
<     if (CheckError( "FlushDeviceBuffer: viSetAttribute VI_ATTR_TMO_VALUE", status)) return;
<     do {
<       status = viRead(data, 1024, &job);
<       std::cout << "flush data: " << data << std::endl;
<       if ((status < VI_SUCCESS) && (status != VI_ERROR_TMO)) return;
<     } while (status >= VI_SUCCESS);
---
>   if (!sendCommand("\n", "TkHV: FlushDeviceBuffer")) return;
2349,2350c2188,2196
<     viSetAttribute(VI_ATTR_TMO_VALUE, DEFAULT_TIMEOUT);
<   }
---
>   // Keep reading lines from the buffer until it is empty
>   ViStatus status = viSetAttribute(VI_ATTR_TMO_VALUE, timeout);
>   if (CheckError( "FlushDeviceBuffer: viSetAttribute VI_ATTR_TMO_VALUE", status)) return;
>   do {
>     status = viRead(data, 1024, &job);
>     if ((status < VI_SUCCESS) && (status != VI_ERROR_TMO)) return;
>   } while (status >= VI_SUCCESS);
> 
>   viSetAttribute(VI_ATTR_TMO_VALUE, DEFAULT_TIMEOUT);
2373,2374c2219,2220
< int TkHV::GetChannels() {
<   return nChannels;
---
> void TkHV::SetDebug(bool d) {
>   TSerialProxy::setDebug(d);
