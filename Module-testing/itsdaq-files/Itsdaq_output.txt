[qcbox@localhost itsdaq-sw]$ source RUNITSDAQ.sh
Running ITS DAQ
Using SCTDAQ_ROOT=/home/qcbox/Desktop/itsdaq-sw-reception/itsdaq-sw (from script directory)
Using root 6 setup
Now running Stavelet macro in ROOT ()

  **********************************
  *                                *
  *    ITSDAQ ROOT (6) Session     *
  *                                *
  **********************************

If ITSDAQ does not start automatically, type
.X Stavelet.cpp to start the application using the new GUI.


Pre-loading macros
Loading ToolBox
Loading TestRegistry
Loading BurstSetup
...done
Attempting to load libstdll.so...
 ... loaded stdll
Attempting to load libkhvdll.so (may not be required, ignore error)...
 ... loaded khvdll
Attempting to load libttidll.so (may not be required, ignore error)...
 ... loaded ttidll
Attempting to load libtvgendll.so (may not be required, ignore error)...
 ... loaded tvgendll
root [0] 
Processing Stavelet.cpp...
Begin Stavelet.cpp
>>> Stavelet using ROOT6 <<<
Loading LV supplies (-1)
Extend var directory with /
Check json file: /home/qcbox/Desktop/itsdaq-sw-reception/itsdaq-sw/DAT/config/power_supplies.json
No JSON file
Loading HV supplies with hvtype: -1
hvtype: -1
Check json file: /home/qcbox/Desktop/itsdaq-sw-reception/itsdaq-sw/DAT/config/power_supplies.json
Loading HV supplies from JSON file
Create TkHV from json:
 /dev/ttyACM0 for 1, deviceType = 0 (SCPI)
TkHV opening resource /dev/ttyACM0
TkHV::TkHV Found iseg at resource /dev/ttyACM0
TkHV::Init Device is OFF, proceeding with initialisation
setting all channels to zero: :VOLT 0,(@0)

TkHV::Mon for iseg at resource /dev/ttyACM0 label 1
Cannot Monitor values if source output is off. Returning zeros.
TkHV::PrintStatus for iseg at resource /dev/ttyACM0
  Label 1
  Safety Limit (software) +/-550V
  Max voltage 1000V
  Max current 6000uA
  Set voltage -0.186V
  Set current -0.00259uA
  Output state OFF
INFO: HV supply is valid, there are now 1 HV supplies

Creating TST object 'e'
You are logged in as qcbox
SCTDB_USER is defined as bnl

TST::UpdateRunInfo()

 Update run number from 55 to 56
 Last info file open for writing ...done run number update


TST::PrintStatus()

 GIT url      https://gitlab.cern.ch/atlas-itk-strips-daq/itsdaq-sw.git/-/tree/759cce2cfc8b6e7ec3459f21c1b996fa1c7272a3
 GIT commit date 2023-01-25 15:18:34 +0000 (Wed, 25 Jan 2023) (locally modified)
 Build info   2023-02-04 00:11:05 +0000 (Sat, 04 Feb 2023)
 GIT describe itsdaq_28_6_2022-479-g759cce2c
 Dedication   New dedication as we're going for PPA
              - with support for the Strips IFB DS2482 one wire interfaces
 Run number   56
 Scan number  0
 Burst number 0
 config_frequency     0
 soft_reset_frequency 1000
 bc_reset_frequency   0
 burst.abc_busy_limit 5
 burst.abort_on_l0id_mismatch 0
 burst.check_l0id_offset 0
 burst.continue_event_count_between_bursts 0
 burst.do_check_l0id 0
 burst.drop_extra_events 0
 burst.dump_raw_data 0
 burst.external_trigger_source 255
 burst.fill_delta_t 1
 burst.do fill evtree 0
 burst.fill_tree_counter 0
 burst.fill_timebin_hit_data 0
 burst.hcc_star_select_decoder 0
 burst.one_bank 0
 burst.pack_hitmap_bits 0
 burst.pull_event_by_l0 0
 burst.read_tdc_data 0
 burst.reset_on_l0id_mismatch 0
 burst.save_event_hit_data 0
 burst.save_raw_event_data 0
 burst.save_trigger_data 0
 burst.send_raw_event_data 0
 burst.send signals 0
 burst.send_signal_event_offset 0
 burst.signal_filler_events 0
 burst.trigger_single_packet 0
 burst.ttc_busy_limit 5
 burst.wait_external_trigger_timeout 5
 do_action_DCS  1
 do_autostop    1
 do_cal_loop    1
 do_fits        1
 do_read_post_scan_config   1
 do_mask_test   0
 do_preburst    0
 do_vary_ntrigs 0
 do_adc_read 0
 do_auto_recover 0
 do_threaded_burst 1
 do_mask_by_cal 0
 burst_type     1
 burst_trtype   3
 burst_ntrigs   100
 burst_maxtrigs 1000000
 burst_target   10
 burst_max_timeouts 6
 L1A_per_loop   1
 throw_away     0
 rate_limit_delay  0
 Debug level 0
 num_modules  -26215
 num_showmods 10
 num_panelmods 2
 abort 0
6 debug options are set
 skip_extra_sleep
 report_reverse_sysmap
 report_configure_variable
 report_execute_enables
 report_execute_variable
 report_get_counters

Unknown chipset in st_coerce_variable!
st_configure_variable: module -1 typ 20 value 5.00

Starting up SCT system
################################################
#                                              #
#   ITSDAQ Startup - call   0                  #
#                                              #
################################################

################################################
#                                              #
#   Configuring SYSTEM                         #
#                                              #
################################################

st_read_sysmap: DAQ config: 'udp' '192.168.222.16' '60002,60002'
st_read_sysmap: DAQ config: 'udp' '192.168.222.16' '60003,60003'
st_read_sysmap: New sysmap 1 modules
sysmap read OK

ITSDAQ System Mapping:
DAQ configuration:
  backend: udp
  config1: 192.168.222.16
  config2: 60002,60002
DAQ configuration:
  backend: udp
  config1: 192.168.222.16
  config2: 60003,60003

DETECTOR  Command config             Data config     MODULE
id pr ac  id ck0 ck1 com0 com1 com2  id s0 s1 d0 d1  Name (type)

 0  1  1   0   0   0    0    0    0   0  0 -1 50 50  BNL-PPB2-MLS-111 (Star_Test)

Total of 1 entries in the sysmap

DAQ/DCS hardware (static configuration):
  nHSIO = 1, nSCTHVs = 0, nOneWire = 1
################################################
#                                              #
#   Load MODULE Configuration                  #
#                                              #
################################################

st_configure_modules: load configuration for MODULE 0
st_read_module: opened file /home/qcbox/Desktop/itsdaq-sw-reception/itsdaq-sw/DAT/config/BNL-PPB2-MLS-111.det
st_read_module: just chipset
st_read_module: found HCC config row 1
st_read_module: found HCC config row 2
st_read_module: found HCC config row 3
st_read_module: found HCC config row 4
st_read_module: found HCC config row 5
st_read_trims: failed to open file either /home/qcbox/Desktop/itsdaq-sw-reception/itsdaq-sw/DAT/config/BNL-PPB2-MLS-111.trim
st_read_trims: failed to open file or BNL-PPB2-MLS-111.trim
st_read_mask: failed to open file /home/qcbox/Desktop/itsdaq-sw-reception/itsdaq-sw/DAT/config/BNL-PPB2-MLS-111.mask
st_read_rc: failed to open file /home/qcbox/Desktop/itsdaq-sw-reception/itsdaq-sw/DAT/config/BNL-PPB2-MLS-111.rcdat
st_mask_cache: masks saved to cache
################################################
#                                              #
#   Initialising READOUT                       #
#                                              #
################################################
#   Initialising HSIO...
st_hsio_init: Using daq_id 0 from 2
Init HSIO interface
Split ports: 60002 and 60002
Creating UDP socket to 192.168.222.16 ports: 60002 to 60002
Address:
 IP address: 192.168.222.16
Have udp socket handle: 5
UDP receive buffer size set to 0 bytes
Have bound socket handle: 5
Made HSIO socket: UDP: 192.168.222.16 60002,60002
Have new HSIO interface
Reading status registers ...
  Don't call receive ack due to recursion (no info read)
 ... using 64 registers to construct FWInfo
BUILD_INFO is: 0a25
Initial firmware register configuration
 0000 0000 0000 0000
 0000 001f 0000 003f
 004f 0032 0000 00ee
 0000 0000 0600 ffff
 010f 2010 0000 0000
 0000 0000 2222 0080
 000a 0001 0010 0010
 1000 0000 0000 0000
 200b 0000 0000 0000
 0000 0000 0000 0000
 0000 0000 0000 0000
 0000 0000 0000 0000
 0000 0000 0000 0000
 0000 0000 0000 0000
 0000 0000 0000 0000
 0000 0000 0000 0000
Initial firmware status
 b1ff a510 b5fb 001f
 0000 0000 0104 06cc
 cfb1 3f30 8aa8 e73d
 0000 0000 0040 0002
 ffff 00ff 0000 7f00
 0000 0030 5be8 6282
 001f 0000 0000 ffec
 0000 0000 0000 0000
 2b03 0000 0011 cb41
 0a25 4000 8500 000f
 041d 0000 0000 0000
 7474 4d61 0000 0000
 0000 0000 0000 0000
 8008 1414 0000 0000
 0000 0000 0000 000c
 fffb ffff fff7 ffff
Establishing stream map...
 ... allowed streams mask:
 5555 5555 5555 0000 0000 0000 0000 1555 0000
No streams to send command to
#   ... HSIO initialised
################################################
#                                              #
#   Initialising SYSTEM                        #
#                                              #
################################################

Set DAQ speed for module 0 to 640
st_execute_enables: 1 modules are present in the sysmap
st_reverse_sysmap:
  HSIO 0 Stream 0 -> Module 0 Link 0
################################################
#                                              #
#   Initialising DCS                           #
#                                              #
################################################

HARD RESET to all modules
'Hard reset' Star chips, sending ABC register reset
################################################
#                                              #
#   Initialising Modules                       #
#                                              #
################################################

EXECUTE CONFIG CONTROL BLOCKS to all modules
st_execute_configs: mod 0 sel 0 using SLOG id 0 ch 0 
st_execute_configs: mod 0 sel 0 using SLOG id 0 ch 0 
st_execute_configs: mod 0 sel 0 using SLOG id 0 ch 0 
st_exceptions: attempting to open file /home/qcbox/Desktop/itsdaq-sw-reception/itsdaq-sw/DAT/config/exceptions.txt
st_exceptions: failed to open file /home/qcbox/Desktop/itsdaq-sw-reception/itsdaq-sw/DAT/config/exceptions.txt
st_exceptions: 0 valid exceptions found
################################################
#                                              #
#   End of System Startup : ready to go !      #
#                                              #
################################################


TST::Startup - Number of modules present: 1
...done Startup
Loading ABCStar test macros...!
Creating FMC0514 object for ABCStar communication as g
  FMC0514
  Revision1
  Serial40
  Patch2
  Passed testYES
  Date13/10/20
  Tested bycas
FMC0514::ad5629PrintStatus
  VREF  1.500V
  VCAL_1-1.000V
  VCAL_3-1.000V
  VCAL_2-1.000V
  VCAL_4-1.000V
  VCAL_0-1.000V
  VCAL_5-1.000V
  V_CM  -1.000V
  V_DM  -1.000V
FMC0514::max11615PrintStatus
  VOFOUT50.203V
  VOFOUT20.268V
  VOFOUT30.264V
  VOFOUT40.317V
  VOFOUT00.001V
  VREF   1.504V
  VOFOUT10.370V
  V3V3   3.267V
(FMC0514 *) 0xb749cf0
st_configure_variable: module -1 typ 10016 value 271.00
st_configure_variable: module -1 typ 3089 value 0.00
st_execute_variable: Send configuration for scan variable: HCCStar variable (32)
Requesting FUSE ID loaded to registers
 0: (15:auto) 0 1 2 3 4 5 6 7 8 9
 ! Some (1) HCCs are waiting for fuse ID to be read (auto)
Start GUI windows...
Setting up display for: Star HCCV1
Set up GUI with configuration for: ABCStar
Creating 1 tabs for module data in BurstData
Pre-loading analysis macros...
 ... done
Using 'stavelet' GUI layout
TkHV::Mon for iseg at resource /dev/ttyACM0 label 1
Cannot Monitor values if source output is off. Returning zeros.
HandleMenu completed for id 201 in 3.19481e-05 seconds
Setting up display for: Star HCCV1
Set up GUI with configuration for: ABCStar
Creating 1 tabs for module data in ScanData
Pre-loading analysis macros...
 ... done
Using 'stavelet' GUI layout
Plot autorange selected
Module 0 Column 0 hit total 0
Module 0 Column 1 hit total 0
Module 0 Column 2 hit total 0
Module 0 Column 3 hit total 0
TkHV::Mon for iseg at resource /dev/ttyACM0 label 1
Cannot Monitor values if source output is off. Returning zeros.
HandleMenu completed for id 201 in 1.90735e-05 seconds
 ... done initial GUI windows
ST() ...ready for action!
Run number is 56.
ST [1] AMACStar_initialiseAMACs({0},{0})
ST [2] AMACStar_configureAMACs(1)
Packet: dfe0ffffffe020   CRC: 20
Packet: dfe0ffffffe020   CRC: 20
ST [3] AMACStar_configureAMACs(0)
Packet: dfe0ffffffe020   CRC: 20
Packet: dfe0ffffffe020   CRC: 20
ST [4] AMACStar_configureAMACs(1)
Packet: dfe0ffffffe020   CRC: 20
Packet: dfe0ffffffe020   CRC: 20
ST [5] HandleMenu completed for id 117 in 0.00325704 seconds
Warning! Incorrect component format entered! Enter a format of 20USxxxxxxxxxx
Warning! Temperature is outside of range of 21 +/- 2
Warning! Humidity is above 10%
Warnings found! Press OK to proceed anyways.
Warning! Incorrect component format entered! Enter a format of 20USxxxxxxxxxx
Warning! Temperature is outside of range of 21 +/- 2
Warning! Humidity is above 10%
Packet: dfe0ffffffe020   CRC: 20
Packet: dfe0ffffffe020   CRC: 20
048
9.8901159
19.780269
29.670379
39.926790
49.8168101
59.707111
69.9634122
79.8535132
89.7436143
100154
109.89164
119.78174
129.67184
139.927196
149.817206
159.707217
169.963228
179.853238
189.744249
200260
209.89270
219.78280
229.67291
239.927302
249.817312
259.707323
269.963334
279.853344
289.744355
300365
309.89376
319.78386
329.67397
339.927408
349.817418
359.707429
369.963440
379.853450
389.744460
400471
409.89481
419.78492
429.67502
439.927513
449.817524
459.707534
469.963545
479.853555
489.744566
500577
509.89587
519.78598
529.67609
539.927619
549.817630
559.707641
569.963650
579.853661
589.744672
600683
609.89693
619.78704
629.67714
639.927726
649.817736
659.707746
669.963759
679.854770
689.744780
699.634790
709.89801
719.78811
729.67821
739.927832
749.817843
759.707854
769.963864
779.853874
789.744885
800896
809.89906
819.78916
829.67927
839.927937
849.817947
859.707958
869.963969
879.854979
889.744990
899.6341000
909.891010
919.781021
929.671023
939.9271023
949.8171023
959.7071023
969.9631023
979.8531023
989.7441023
10001023

****************************************
Minimizer is Minuit / Migrad
Chi2                      =      5.13585
NDf                       =           41
Edm                       =  1.03019e-17
NCalls                    =           42
p0                        =     0.944923   +/-   0.000410978 
p1                        =     -45.2462   +/-   0.123377    
AM0= 43
AM1= 45
AM2= 43
AM3= 37
AM4= 46
AM5= 51
AM6= 49
AM7= 135
AM8= 44
AM9= 40
AM10= 37
AM11= 46
AM12= 40
AM13= 36
AM14= 45
AM15= 37
NTC0 Vref=242.845108
NTC1 Vref=247.569733
NTC2 Vref=250.404495
Scheduled job 1
TkHV::RampImpl ramping to 0V, rampRate 3
:CURR:BOUNDS 20.000000E-6,(@0)


0.000000E-3A

:CURR:BOUNDS 20.000000E-6,(@0)




ISEG channel 0 has voltage: 0 and current: 0
Requested 0V Achieved 0V current 0uA
TkHV::Mon for iseg at resource /dev/ttyACM0 label 1
Cannot Monitor values if source output is off. Returning zeros.
Scheduled job 2
TkHV::RampImpl ramping to -10V, rampRate 3
:CURR:BOUNDS 21.000000E-6,(@0)


:MEAS:CURR? (@0)

:CURR:BOUNDS 21.000000E-6,(@0)


:CURR:BOUNDS 21.000000E-6,(@0)

ISEG channel 0 has voltage: 0 and current: 0
Requested -2.5V Achieved 0V current 0uA
:CURR:BOUNDS 21.000000E-6,(@0)


:MEAS:CURR? (@0)

ISEG channel 0 has voltage: 0 and current: 0
Requested -5V Achieved 0V current 0uA
:CURR:BOUNDS 21.000000E-6,(@0)


:READ:CURR:BOUNDS? (@0)

ISEG channel 0 has voltage: 0 and current: 0
Requested -7.5V Achieved 0V current 0uA
:CURR:BOUNDS 21.000000E-6,(@0)


:VOLT 0.0,(@0)

ISEG channel 0 has voltage: 0 and current: 0
Requested -10V Achieved 0V current 0uA
ISEG channel 0 has voltage: 0 and current: 0
Scheduled job 3
TkHV::RampImpl ramping to -20V, rampRate 3
:CURR:BOUNDS 22.000000E-6,(@0)


:VOLT ON,(@0)

:CURR:BOUNDS 22.000000E-6,(@0)


:VOLT ON,(@0)

ISEG channel 0 has voltage: 0 and current: 0
Requested -2.5V Achieved 0V current 0uA
:CURR:BOUNDS 22.000000E-6,(@0)


:MEAS:VOLT? (@0)

ISEG channel 0 has voltage: 0 and current: 0
Requested -5V Achieved 0V current 0uA
:CURR:BOUNDS 22.000000E-6,(@0)


:MEAS:VOLT? (@0)

ISEG channel 0 has voltage: 0 and current: 0
Requested -7.5V Achieved 0V current 0uA
:CURR:BOUNDS 22.000000E-6,(@0)


0

ISEG channel 0 has voltage: 0 and current: 0
Requested -10V Achieved 0V current 0uA
:CURR:BOUNDS 22.000000E-6,(@0)


:VOLT -5.0,(@0)

ISEG channel 0 has voltage: 0 and current: 0
Requested -12.5V Achieved 0V current 0uA
:CURR:BOUNDS 22.000000E-6,(@0)


-0.000181E3V

ISEG channel 0 has voltage: 0 and current: 0
Requested -15V Achieved 0V current 0uA
:CURR:BOUNDS 22.000000E-6,(@0)


:CURR:BOUNDS 21.000000E-6,(@0)

ISEG channel 0 has voltage: 0 and current: 0
Requested -17.5V Achieved 0V current 0uA
:CURR:BOUNDS 22.000000E-6,(@0)


:CURR:BOUNDS 21.000000E-6,(@0)

ISEG channel 0 has voltage: 8.12e-09 and current: 0
Requested -20V Achieved 0V current 0uA
ISEG channel 0 has voltage: 0 and current: 0
Scheduled job 4
TkHV::RampImpl ramping to -30V, rampRate 3
:CURR:BOUNDS 23.000000E-6,(@0)


:VOLT -10.0,(@0)

