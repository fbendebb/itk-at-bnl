// 26 January 2023 
// Abraham Tishelman-Charny
//
// The purpose of this macro is to setup modules for testing.
// Example usage within ROOT session:
// $ loadmacro("SetupModules")
// $ SetupModules(1) // Setup one module, assuming it's in the leftmost chuck
// $ SetupModules(2) // Setup two modules, assuming they start from the left and go up.
// $ SetupModules(4) // Setup four modules assuming they start from the left
// $ SetupModules(1,"0") // Setup a module in the first chuck from the left
// $ SetupModules(1,"1") // Setup a module in the second chuck from the left
// $ SetupModules(1,"2") // Setup a module in the third chuck from the left
// $ SetupModules(1,"3") // Setup a module in the fourth chuck from the left

void SetupModules(int N_modules, string position = "all"){
	loadmacro("AMAC_common");
	loadmacro("AMACStar");
	loadmacro("AMACStar_0514");
	if(position == "all"){
		if(N_modules==1) AMACStar_initialiseAMACs({0},{0});
		else if(N_modules==2) AMACStar_initialiseAMACs({0,0},{0,1});
		else if(N_modules==3) AMACStar_initialiseAMACs({0,0,0},{0,1,2});
		else if(N_modules==4) AMACStar_initialiseAMACs({0,0,0,0},{0,1,2,3});
	}
	else if(position == "0") AMACStar_initialiseAMACs({0},{0});
	else if(position == "1") AMACStar_initialiseAMACs({0},{1});
	else if(position == "2") AMACStar_initialiseAMACs({0},{2});
	else if(position == "3") AMACStar_initialiseAMACs({0},{3});
	AMACStar_configureAMACs(1);
	AMACStar_configureAMACs(0);
	AMACStar_0514_calScan(1);
	AMACStar_internalCalibrations();
	AMACStar_configureAMACs(1);
	std::cout << "Calibration complete." << std::endl;
}
