# Cold Noise Tests Procedure

The purpose of these instructions is to share and describe the ITSDAQ commands to run in order to perform cold noise tests on ITk strip modules.

## Instructions

These are the current tests being run at different chiller temperatures at BNL.

### Setup 

Run ITSDAQ: `source RUNITSDAQ.sh` 

Initialize the AMACs ***once per ITSDAQ session:***:

```
source RUNITSDAQ.sh 
loadmacro("AMAC_common")
loadmacro("AMACStar")
AMACStar_initialiseAMACs({0,0,0},{0,2,3}) # depends on number of modules/chucks used.
```

### 20 degrees C
- DCDC ON: `AMACStar_configureAMACs(1)`
- Ensure ABCStars are fully configured and have LV fully ON: 
  - In ITSDAQ GUI: `Capture` --> `Scan HPR output phase` (if you see red text and errors, just let it finish. It may not be a problem.)
  - In ITSDAQ GUI: `Action` --> `ExecuteConfigs` 
- HV ON 
- Run FullTest
- HV OFF 
- DCDC OFF `AMACStar_configureAMACs(0)`
- Calibrate the AMAC so that you can calibrate the temperature measurements it will take at this chiller/chuck temperature:
- ***Once per ITSDAQ session:*** ` loadmacro("AMACStar_0514")`

```
AMACStar_0514_calScan(1)
AMACStar_internalCalibrations()
```

- If desired: Take PTATZERO: `AMACStar_readPTATzero(-1,100, 1)`
- DCDC on: `AMACStar_configureAMACs(1)`
- Ensure ABCStars are fully configured and have LV fully ON: 
  - In ITSDAQ GUI: `Capture` --> `Scan HPR output phase` (if you see red text and errors, just let it finish. It may not be a problem.)
  - In ITSDAQ GUI: `Action` --> `ExecuteConfigs` 
- HV ON 

### 40 degrees C
Pedestal scan, noise trim, strobe delay, three point gain:

```
pedestalScan(6,15,100,50)
ABCStarNoiseTrimPlot(e->runnum, e->scannum, 6,15)
ABCStarStrobeDelay(0,0.57,256,32,1,50)
ABCStarThreePointGain(1.0,256,1020,1,50) 
```

- Look at Response Curve plots to see how things look, make sure plots aren't extremely crazy / missing.

### -20, -40, -60 degrees C (and similar temperatures)
Pedestal scan, noise trim, strobe delay, three point gain:

```
pedestalScan(6,15,100,50)
ABCStarNoiseTrimPlot(e->runnum, e->scannum, 6,15)
ABCStarStrobeDelay(0,0.57,256,32,1,50)
ABCStarThreePointGain(1.0,256,1020,1,50) 
```

- Look at Response Curve plots to see how things look, make sure plots aren't extremely crazy / missing.

Use shunt to increase strip current, in order to run a current through long strips similar to that from short strips, as short strips were the first type of module where cold noise was seen:

```
AMACStar_writeReg(0,49, 0x010d)
AMACStar_writeReg(0,48, 0x80f40000) # ~ 450mA example for LS Hybrid X
```

- Three point gain: `ABCStarThreePointGain(1.0,256,1020,1,50)`
- Look at Response Curve plots to see how things look, make sure plots aren't extremely crazy / missing.
- HV off 
- Disable shunt: `AMACStar_writeReg(0,49, 0x000d)`
- DCDC off: `AMACStar_configureAMACs(0)`

- Calibrate the AMAC so that you can calibrate the temperature measurements it will take at this chiller/chuck temperature:

```
AMACStar_0514_calScan(1)
AMACStar_internalCalibrations()
```

- DCDC on: `AMACStar_configureAMACs(1)`
- Ensure ABCStars are fully configured and have LV fully ON: 
  - In ITSDAQ GUI: `Capture` --> `Scan HPR output phase` (if you see red text and errors, just let it finish. It may not be a problem.)
  - In ITSDAQ GUI: `Action` --> `ExecuteConfigs` 
- HV on 

Take NTC and PTAT measurements:

```
AMACStar_readNTC(-1,2) # Powerboard NTC temp read by AMAC
AMACStar_readNTC(-1,0) # Hybrid X NTC temp read by AMAC
```

- Read Powerboard PTAT temperature: `AMACStar_readPTAT(-1)`

Use shunt to increase strip current, in order to run a current through long strips similar to that from short strips, as short strips were the first type of module where cold noise was seen:

```
AMACStar_writeReg(0,49, 0x010d)
AMACStar_writeReg(0,48, 0x80f40000) # ~ 450mA example for LS Hybrid X
```

- Repeat PTAT measurement: `AMACStar_readPTAT(-1)`
- Disable use of shunt: `AMACStar_writeReg(0,49, 0x000d)`
